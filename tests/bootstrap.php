<?php

$autoLoader = str_replace(
    '/bin/phpunit',
    '/autoload.php',
    realpath($_SERVER['SCRIPT_FILENAME'])
);

require_once $autoLoader;

