<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Tests\LegacyMapper;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use stdClass;
use Symfony\Component\DependencyInjection\ServiceLocator;
use MiniatureHappiness\LegacyBundle\Exceptions\MemoryException;
use MiniatureHappiness\LegacyBundle\LegacyMapper\ServiceMapper;

class ServiceMapperTest extends TestCase
{
    use ProphecyTrait;

    private ObjectProphecy|ServiceLocator|null $locator;
    private ?ServiceMapper $uut;

    public function setUp(): void
    {
        $this->locator = $this->prophesize(ServiceLocator::class);

        $this->uut = new ServiceMapper(
            $this->locator->reveal()
        );
    }

    public function tearDown(): void
    {
        $this->locator = null;
        $this->uut = null;
    }

    /**
     * @test
     */
    public function should_have_id(): void
    {
        $id = 'youconix.test';
        $this->locator->has($id)->willReturn(true);

        $result = $this->uut->has($id);

        self::assertTrue($result);
    }

    /**
     * @test
     */
    public function should_not_have_id(): void
    {
        $id = 'youconix.test';
        $this->locator->has($id)->willReturn(false);

        $result = $this->uut->has($id);

        self::assertFalse($result);
    }

    /**
     * @test
     */
    public function should_load_service(): void
    {
        $id = 'youconix.test';
        $class = new stdClass();
        $this->locator->has($id)->willReturn(true);
        $this->locator->get($id)->willReturn($class);

        $result = $this->uut->get($id);

        self::assertSame($class, $result);
    }

    /**
     * @test
     */
    public function should_not_load_service(): void
    {
        $this->expectException(MemoryException::class);

        $id = 'youconix.test';
        $this->locator->has($id)->willReturn(false);

        $this->uut->get($id);
    }
}