<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Tests\Listeners;

use MiniatureHappiness\LegacyBundle\MemoryLoader;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelInterface;
use MiniatureHappiness\LegacyBundle\LegacyMapper\LegacyMapRetriever;
use MiniatureHappiness\LegacyBundle\LegacyMapper\LegacyMap;
use MiniatureHappiness\LegacyBundle\LegacyMapper\ServiceMapper;
use MiniatureHappiness\LegacyBundle\Listeners\BridgeHookListener;

class BridgeHookTest extends TestCase
{
    use ProphecyTrait;

    private LegacyMapRetriever|ObjectProphecy|null $retriever;
    private ?ServiceMapper $locator;
    private MemoryLoader|ObjectProphecy|null $memory;
    private ?BridgeHookListener $uut;
    private string $cacheDir = 'cache/dev';

    public function setUp(): void
    {
        $this->retriever = $this->prophesize(LegacyMapRetriever::class);
        $this->locator = $this->prophesize(ServiceMapper::class)->reveal();
        $this->memory = $this->prophesize(MemoryLoader::class)->reveal();

        $this->uut = new BridgeHookListener(
            $this->retriever->reveal(),
            $this->locator,
            $this->memory,
            $this->cacheDir
        );
    }

    public function tearDown(): void
    {
        $this->retriever = null;
        $this->locator = null;
        $this->memory = null;
        $this->uut = null;
    }

    /**
     * @test
     */
    public function should_init_mapper_on_main_request(): void
    {
        $event = $this->createEvent(true);

        /** @var LegacyMap|ObjectProphecy $map */
        $map = $this->prophesize(LegacyMap::class);
        $map->setServiceLocator($this->locator)->shouldBeCalledOnce();
        $map->loadMap($this->cacheDir)->shouldBeCalledOnce();

        $this->retriever->get()->willReturn($map->reveal());

        $this->uut->onKernelRequest($event);
    }

    /**
     * @test
     */
    public function should_not_init_mapper_on_sub_request(): void
    {
        $event = $this->createEvent(false);

        $this->retriever->get()->shouldNotBeCalled();

        $this->uut->onKernelRequest($event);
    }

    private function createEvent(bool $isMainEvent): RequestEvent
    {
        return new RequestEvent(
            $this->prophesize(KernelInterface::class)->reveal(),
            $this->prophesize(Request::class)->reveal(),
            $isMainEvent ? KernelInterface::MAIN_REQUEST : KernelInterface::SUB_REQUEST
        );
    }
}