<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Tests\Services;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\LoggerInterface;
use MiniatureHappiness\CoreBundle\Exceptions\IOException;
use MiniatureHappiness\LegacyBundle\Services\File;

class FileTest extends TestCase
{
    use ProphecyTrait;

    private ?File $uut;
    private string $temp;
    private string $file = 'example.txt';
    private $rights = 0600;
    private string $content1 = 'content1';
    private string $content2 = 'content2';

    public function setUp(): void
    {
        $this->temp = sys_get_temp_dir() . DIRECTORY_SEPARATOR;

        $logger = $this->prophesize(LoggerInterface::class)->reveal();

        $this->uut = new File();
        $this->uut->setDeprecationLogger($logger);
    }

    public function tearDown(): void
    {
        $this->uut = null;

        if (file_exists($this->temp . $this->file)) {
            unlink($this->temp . $this->file);
            clearstatcache();
        }
    }

    /**
     * @test
     */
    public function should_create_a_new_file(): void
    {
        $fileName = $this->temp . $this->file;

        $this->uut->newFile($fileName, $this->rights);

        self::assertFileExists($fileName);
    }

    /**
     * @test
     */
    public function should_not_read_file(): void
    {
        $this->expectException(IOException::class);

        $this->uut->readFile($this->temp . $this->file);
    }

    /**
     * @test
     */
    public function should_write_file(): void
    {
        $fileName = $this->temp . $this->file;

        $this->createFile($fileName, $this->content1, $this->rights);

        self::assertSame($this->content1, $this->uut->readFile($fileName));
    }

    /**
     * @test
     */
    public function should_write_content_to_end_of_the_file(): void
    {
        $fileName = $this->temp . $this->file;

        $this->uut->writeFile($fileName, $this->content1, $this->rights);
        $this->uut->writeLastFile($fileName, $this->content2, $this->rights);

        $expected = $this->content1 . $this->content2;

        self::assertSame($expected, $this->uut->readFile($fileName));
    }

    /**
     * @test
     */
    public function should_write_content_at_begin_of_the_file(): void
    {
        $fileName = $this->temp . $this->file;

        $this->uut->writeFile($fileName, $this->content1, $this->rights);
        $this->uut->writeFirstFile($fileName, $this->content2, $this->rights);

        $expected = $this->content2 . $this->content1;

        self::assertSame($expected, $this->uut->readFile($fileName));
    }

    /**
     * @test
     */
    public function should_rename_file(): void
    {
        $fileName = $this->temp . $this->file;

        $this->uut->writeFile($fileName, '');

        $this->uut->renameFile($fileName, $fileName . '_2');

        self::assertFileExists($fileName . '_2', 'Renaming ' . $fileName . ' to ' . $fileName . '_2 failed');

        unlink($fileName . '_2');
    }

    /**
     * @test
     */
    public function should_copy_file(): void
    {
        $this->createFile($this->temp . $this->file, 'lalalala', $this->rights);

        $this->uut->newDirectory($this->temp . 'test');
        $this->uut->copyFile($this->temp . $this->file, $this->temp . 'test');

        self::assertFileExists($this->temp . 'test/' . $this->file);

        unlink($this->temp . 'test/' . $this->file);
        rmdir($this->temp . 'test/');
    }

    /**
     * Moves the given file to the given directory
     *
     * @test
     */
    public function should_fail_to_move_file(): void
    {
        $this->expectException(IOException::class);

        $fileName = $this->temp . $this->file;

        $this->createFile($fileName, 'lalalala', $this->rights);

        $this->uut->moveFile($fileName, '/');
    }

    /**
     * Deletes the given file
     *
     * @test
     */
    public function should_delete_file(): void
    {
        $fileName = $this->temp . $this->file;

        $this->createFile($fileName, 'lalalala', $this->rights);

        $this->uut->deleteFile($fileName);

        self::assertFileDoesNotExist($fileName);
    }

    /**
     * @test
     */
    public function should_read_directory(): void
    {
        $this->uut->newDirectory($this->temp . 'read_test');
        $this->uut->writeFile($this->temp . 'read_test/' . $this->file, '');
        $this->uut->newDirectory($this->temp . 'read_test/test');
        $this->uut->newDirectory($this->temp . 'read_test/test/subdir');
        $this->uut->writeFile($this->temp . 'read_test/test/file2.txt', '');
        $this->uut->writeFile($this->temp . 'read_test/test/file3.txt', '');
        $this->uut->writeFile($this->temp . 'read_test/test/subdir/file4.txt', '');

        $expected = [
            $this->temp . 'read_test/' . $this->file,
            $this->temp . 'read_test/test/file2.txt',
            $this->temp . 'read_test/test/file3.txt',
            $this->temp . 'read_test/test/subdir/file4.txt',
        ];

        $result = $this->uut->readDirectory($this->temp . 'read_test', true);

        self::assertSame($expected, $result);

        $this->uut->deleteDirectory($this->temp . 'read_test');
    }

    /**
     * @test
     */
    public function should_create_directory(): void
    {
        $this->uut->newDirectory($this->temp . 'test');

        self::assertDirectoryExists($this->temp . 'test');

        rmdir($this->temp . 'test');
    }

    /**
     * @test
     */
    public function should_rename_directory(): void
    {
        $this->uut->newDirectory($this->temp . 'test');
        $this->uut->renameDirectory($this->temp . 'test', 'new_test');

        self::assertDirectoryExists($this->temp . 'new_test');

        rmdir($this->temp . 'new_test');
    }

    /**
     * @test
     */
    public function should_move_directory(): void
    {
        $this->uut->newDirectory($this->temp . 'test');
        $this->uut->newDirectory($this->temp . 'target');

        $this->uut->moveDirectory($this->temp . 'test', $this->temp . 'target');

        self::assertDirectoryExists($this->temp . 'target/test');

        $this->uut->deleteDirectory($this->temp . 'target');
    }

    /**
     * @test
     */
    public function should_copy_directory(): void
    {
        $this->uut->newDirectory($this->temp . 'write_test');
        $this->uut->writeFile($this->temp . 'write_test/' . $this->file, '');
        $this->uut->writeFile($this->temp . 'write_test/file2.txt', '');
        $this->uut->writeFile($this->temp . 'write_test/file3.txt', '');
        $this->uut->newDirectory($this->temp . 'target');

        $this->uut->copyDirectory($this->temp . 'write_test', $this->temp . 'target/write_test');

        self::assertFileExists($this->temp . 'write_test/file2.txt');
        self::assertDirectoryExists($this->temp . 'target/write_test');
        self::assertFileExists($this->temp . 'target/write_test/file2.txt');
        self::assertFileExists($this->temp . 'target/write_test/file3.txt');

        $this->uut->deleteDirectory($this->temp . 'write_test');
        $this->uut->deleteDirectory($this->temp . 'target');
    }

    /**
     * @test
     */
    public function should_delete_directory(): void
    {
        $this->uut->newDirectory($this->temp . 'write_test');
        $this->uut->newDirectory($this->temp . 'target');
        $this->uut->newDirectory($this->temp . 'write_test/target');

        $this->uut->deleteDirectory($this->temp . 'target');
        $this->uut->deleteDirectory($this->temp . 'write_test');

        self::assertDirectoryDoesNotExist($this->temp . 'write_test/target');
        self::assertDirectoryDoesNotExist($this->temp . 'write_test');
        self::assertDirectoryDoesNotExist($this->temp . 'target');
    }

    /**
     * @test
     */
    public function file_should_exist(): void
    {
        $fileName = $this->temp . $this->file;

        $this->createFile($fileName, 'lalalala', $this->rights);

        self::assertTrue($this->uut->exists($fileName));
    }

    /**
     * @test
     */
    public function file_should_not_exist(): void
    {
        $fileName = $this->temp . $this->file;

        self::assertFalse($this->uut->exists($fileName));
    }

    /**
     * @test
     */
    public function should_set_permissions(): void
    {
        $fileName = $this->temp . $this->file;

        $this->createFile($fileName, 'lalalala', $this->rights);
        $this->uut->rights($fileName, 0644);

        $result = substr(sprintf('%o', fileperms($fileName)), -4);

        self::assertSame('0644', $result);
    }

    private function createFile(string $filename, string $content, $rights): void
    {
        $this->uut->writeFile($filename, $content, $rights);
    }
}