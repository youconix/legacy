<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Tests\Services;

use MiniatureHappiness\LegacyBundle\MemoryLoader;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use MiniatureHappiness\LegacyBundle\Services\Template;
use MiniatureHappiness\LegacyBundle\Exceptions\TemplateException;
use MiniatureHappiness\LegacyBundle\Services\Templating\TemplateParser;
use MiniatureHappiness\LegacyBundle\Services\Templating\TemplateSettings;

class TemplateTest extends TestCase
{
    use ProphecyTrait;

    private TemplateParser|ObjectProphecy|null $templateParser;
    private TemplateSettings|ObjectProphecy|null $templateSettings;
    private MemoryLoader|ObjectProphecy|null $memoryWrapper;
    private ?Template $uut;
    private string $cacheDirectory = '/var/cache';
    private string $rootDirectory;
    private string $stylesFile = '/styles/default/style.yml';

    public function setUp(): void
    {
        $this->rootDirectory = __DIR__ . '/../data/template';

        $this->templateParser = $this->prophesize(TemplateParser::class);

        $this->templateSettings = $this->prophesize(TemplateSettings::class);
        $this->templateSettings->getTemplateDirectory()->willReturn('default');
        $this->templateSettings->getLayout()->willReturn('layouts/default.tpl');
        $this->templateSettings->getStylesDirs()->willReturn(['default']);

        $this->memoryWrapper = $this->prophesize(MemoryLoader::class);
        $this->memoryWrapper->getPage()->willReturn('/index');

        $this->uut = new Template(
            $this->templateParser->reveal(),
            $this->templateSettings->reveal(),
            $this->memoryWrapper->reveal(),
            $this->cacheDirectory,
            $this->rootDirectory
        );
    }

    public function tearDown(): void
    {
        $this->templateParser = null;
        $this->templateSettings = null;
        $this->memoryWrapper = null;
        $this->uut = null;
    }

    /**
     * @test
     */
    public function should_load_view_without_extension(): void
    {
        $this->templateParser
            ->addContent('VIEW', '/index/landing.tpl')
            ->shouldBeCalledOnce();

        $this->uut->loadView('landing');
    }

    /**
     * @test
     */
    public function should_load_view_without_setting_for_default(): void
    {
        $this->templateParser
            ->addContent('VIEW', '/entry/index.tpl')
            ->shouldNotBeCalled();

        $this->uut->loadView('/entry/index.tpl');
    }

    /**
     * @test
     */
    public function should_not_load_unknown_view(): void
    {
        $this->expectException(TemplateException::class);

        $this->uut->loadView('unknown.tpl');
    }

    /**
     * @test
     */
    public function should_set_javascript_link(): void
    {
        $link = '<script src="test.js"></script>';

        $this->templateParser
            ->addJavascriptLink($link)
            ->shouldBeCalledOnce();

        $this->uut->headerLink($link);
    }

    /**
     * @test
     */
    public function should_set_javascript(): void
    {
        $javascript = '<script> <!-- function test(){ alert("test"); } //--> </script>';

        $this->templateParser
            ->addJavascript($javascript)
            ->shouldBeCalledOnce();

        $this->uut->headerLink($javascript);
    }

    /**
     * @test
     */
    public function should_set_css_link(): void
    {
        $link = '<link type="stylesheet" href="css/style.css">';

        $this->templateParser
            ->addCssLink($link)
            ->shouldBeCalledOnce();

        $this->uut->headerLink($link);
    }

    /**
     * @test
     */
    public function should_set_CSS(): void
    {
        $css = '<style> body { color:#111; }</style>';

        $this->templateParser
            ->addCss($css)
            ->shouldBeCalledOnce();

        $this->uut->headerLink($css);
    }

    /**
     * @test
     */
    public function should_set_metaLink(): void
    {
        $metaTag = '<meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">';

        $this->templateParser
            ->addMetaTag($metaTag)
            ->shouldBeCalledOnce();

        $this->uut->headerLink($metaTag);
    }

    /**
     * @test
     */
    public function should_write_template(): void
    {
        $expected = '<body>Rendered page</body>';

        $this->templateParser
            ->build('layouts/default.tpl', 'default')
            ->willReturn($expected)
            ->shouldBeCalledOnce();

        $result = $this->uut->buildPage();

        self::assertSame($expected, $result);
    }
}