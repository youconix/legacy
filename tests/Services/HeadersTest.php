<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Tests\Services;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use MiniatureHappiness\LegacyBundle\Services\Headers;

class HeadersTest extends TestCase
{
    use ProphecyTrait;

    private RouterInterface|ObjectProphecy|null $router;
    private HeaderBag|ObjectProphecy|null $headerBag;
    private ?Headers $uut;

    public function setUp(): void
    {
        $this->router = $this->prophesize(RouterInterface::class);
        $requestStack = $this->prophesize(RequestStack::class);
        $request = $this->prophesize(Request::class);

        $this->headerBag = $this->prophesize(HeaderBag::class);
        $request->headers = $this->headerBag->reveal();

        $requestStack->getMainRequest()->willReturn($request->reveal());

        $logger = $this->prophesize(LoggerInterface::class)->reveal();

        $this->uut = new Headers(
            $this->router->reveal(),
            $requestStack->reveal()
        );
        $this->uut->setDeprecationLogger($logger);
    }

    public function tearDown(): void
    {
        $this->router = null;
        $this->headerBag = null;
        $this->uut = null;
    }

    /**
     * @test
     */
    public function should_clear_the_headers(): void
    {
        $this->headerBag
            ->keys()
            ->willReturn([
                'Content-Type',
                'Content-Size',
            ])
            ->shouldBeCalledOnce();

        $this->headerBag
            ->remove('Content-Type')
            ->shouldBeCalledOnce();
        $this->headerBag
            ->remove('Content-Size')
            ->shouldBeCalledOnce();
        $this->headerBag
            ->set('Content-Type', 'text/html')
            ->shouldBeCalledOnce();

        $this->uut->clear();
    }

    /**
     * @test
     */
    public function should_set_contentType(): void
    {
        $contentType = 'application/pdf';

        $this->headerBag
            ->set('Content-Type', $contentType)
            ->shouldBeCalledOnce();

        $this->uut->contentType($contentType);
    }

    /**
     * @test
     */
    public function should_set_javascript_contentType(): void
    {
        $contentType = 'application/javascript';

        $this->headerBag
            ->set('Content-Type', $contentType)
            ->shouldBeCalledOnce();

        $this->uut->setJavascript();
    }

    /**
     * @test
     */
    public function should_set_css_contentType(): void
    {
        $contentType = 'text/css';

        $this->headerBag
            ->set('Content-Type', $contentType)
            ->shouldBeCalledOnce();

        $this->uut->setCSS();
    }

    /**
     * @test
     */
    public function should_set_XML_contentType(): void
    {
        $contentType = 'application/xml';

        $this->headerBag
            ->set('Content-Type', $contentType)
            ->shouldBeCalledOnce();

        $this->uut->setXML();
    }

    /**
     * @test
     */
    public function should_set_modified_header(): void
    {
        $modified = time();
        $expected_value = sprintf('%s GMT', gmdate('D, d M Y H:i:s', $modified));

        $this->headerBag
            ->set('Last-Modified', $expected_value)
            ->shouldBeCalledOnce();

        $this->uut->modified($modified);
    }

    /**
     * @test
     */
    public function should_set_cache_header(): void
    {
        $cache = 20;
        $expected_value = sprintf('%s GMT', gmdate('D, d M Y H:i:s', (time() + $cache)));

        $this->headerBag
            ->set('Expires', $expected_value)
            ->shouldBeCalledOnce();

        $this->uut->cache($cache);
    }

    /**
     * @test
     */
    public function should_set_no_cache_headers(): void
    {
        $this->headerBag
            ->set('Expires', 'Thu, 01-Jan-70 00:00:01 GMT')
            ->shouldBeCalledOnce();
        $this->headerBag
            ->set('Last-Modified', gmdate('D, d M Y H:i:s') . ' GMT')
            ->shouldBeCalledOnce();
        $this->headerBag
            ->set('Cache-Control', 'no-store, no-cache, must-revalidate')
            ->shouldBeCalledOnce();
        $this->headerBag
            ->set('Cache-Control', 'post-check=0, pre-check=0')
            ->shouldBeCalledOnce();
        $this->headerBag
            ->set('Pragma', 'no-cache')
            ->shouldBeCalledOnce();

        $this->uut->cache(-1);
    }

    /**
     * @test
     */
    public function should_set_contentLength_header(): void
    {
        $length = 2342345234;

        $this->headerBag
            ->set('Content-Length', (string)$length)
            ->shouldBeCalledOnce();

        $this->uut->contentLength($length);
    }

    /**
     * @test
     */
    public function should_set_the_given_header(): void
    {
        $key = 'Mime-Type';
        $content = 'application/wrong';

        $this->headerBag
            ->set($key, $content)
            ->shouldBeCalledOnce();

        $this->uut->setHeader($key, $content);
    }
}