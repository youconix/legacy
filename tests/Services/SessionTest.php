<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Tests\Services;

use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use MiniatureHappiness\CoreBundle\Auth\LoginManager;
use MiniatureHappiness\CoreBundle\Exceptions\AuthenticationException;
use MiniatureHappiness\CoreBundle\Exceptions\IOException;
use MiniatureHappiness\LegacyBundle\Services\Session;

class SessionTest extends TestCase
{
    use ProphecyTrait;

    private SessionInterface|ObjectProphecy|null $session;
    private LoginManager|ObjectProphecy|null $loginManager;
    private TokenStorageInterface|ObjectProphecy|null $tokenStorage;

    private ?Session $uut;
    private string $name;
    private string $data;

    public function setUp(): void
    {
        $this->session = $this->prophesize(SessionInterface::class);
        $this->loginManager = $this->prophesize(LoginManager::class);
        $this->tokenStorage = $this->prophesize(TokenStorageInterface::class);

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getSession()->willReturn($this->session->reveal());
        $logger = $this->prophesize(LoggerInterface::class)->reveal();

        $this->uut = new Session(
            $requestStack->reveal(),
            $this->loginManager->reveal(),
            $this->tokenStorage->reveal()
        );
        $this->uut->setDeprecationLogger($logger);

        $this->name = 'testSession';
        $this->data = 'lalalala';
    }

    public function tearDown(): void
    {
        $this->session = null;
        $this->loginManager = null;
        $this->tokenStorage = null;
        $this->uut = null;
    }

    /**
     * @test
     */
    public function should_delete_session(): void
    {
        $this->session
            ->has($this->name)
            ->willReturn(true)
            ->shouldBeCalledOnce();

        $this->session
            ->remove($this->name)
            ->shouldBeCalledOnce();

        $this->uut->delete($this->name);
    }

    /**
     * @test
     */
    public function should_set_session(): void
    {
        $this->session
            ->set($this->name, $this->data)
            ->shouldBeCalledOnce();

        $this->uut->set($this->name, $this->data);
    }

    /**
     * @test
     */
    public function should_not_retrieve_session(): void
    {
        $this->expectException(IOException::class);

        $this->session
            ->has($this->name)
            ->willReturn(false)
            ->shouldBeCalledOnce();

        $this->uut->get($this->name);
    }

    /**
     * @test
     */
    public function should_retrieve_session(): void
    {
        $this->session
            ->has($this->name)
            ->willReturn(true)
            ->shouldBeCalledOnce();
        $this->session
            ->get($this->name)
            ->willReturn($this->data)
            ->shouldBeCalledOnce();

        $result = $this->uut->get($this->name);

        self::assertSame($this->data, $result);
    }

    /**
     * @test
     */
    public function session_should_exist(): void
    {
        $this->session
            ->has($this->name)
            ->willReturn(true)
            ->shouldBeCalledOnce();

        $result = $this->uut->exists($this->name);

        self::assertTrue($result);
    }

    /**
     * @test
     */
    public function should_destroy_all_sessions(): void
    {
        $this->session
            ->clear()
            ->shouldBeCalledOnce();

        $this->uut->destroy();
    }

    /**
     * @test
     */
    public function should_set_login_session(): void
    {
        $user = $this->prophesize(UserInterface::class)->reveal();
        $userId = 231;
        $lastLogin = 1223412213;

        $this->loginManager
            ->getUserById($userId)
            ->willReturn($user)
            ->shouldBeCalledOnce();

        $this->loginManager
            ->login(
                $user,
                $lastLogin,
                true
            )
            ->will(function () {
                return $this;
            })
            ->shouldBeCalledOnce();

        $this->uut->setLogin($userId, $lastLogin, true);
    }

    /**
     * @test
     */
    public function should_destroy_login_session(): void
    {
        $this->loginManager
            ->logout()
            ->shouldBeCalledOnce();

        $this->uut->destroyLogin();
    }

    /**
     * @test
     */
    public function should_check_login_session(): void
    {
        $user = $this->prophesize(UserInterface::class)->reveal();

        $this->loginManager
            ->checkAutoLogin()
            ->willReturn($user)
            ->shouldBeCalledOnce();

        $this->loginManager
            ->validateFingerprint($user)
            ->shouldBeCalledOnce();

        $this->uut->checkLogin();
    }

    /**
     * @test
     */
    public function should_reject_check_login_session(): void
    {
        $this->expectException(AuthenticationException::class);

        $this->loginManager
            ->checkAutoLogin()
            ->willReturn(null)
            ->shouldBeCalledOnce();

        $this->uut->checkLogin();
    }

    /**
     * @test
     */
    public function should_check_2fa(): void
    {
        $user = $this->prophesize(UserInterface::class)->reveal();

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($user);

        $this->tokenStorage
            ->getToken()
            ->willReturn($token->reveal());

        $this->loginManager
            ->check2fa($user, true)
            ->shouldBeCalledOnce();

        $this->uut->check2fa();
    }

    /**
     * @test
     */
    public function should_set_2fa(): void
    {
        $this->loginManager
            ->set2fa()
            ->shouldBeCalledOnce();

        $this->uut->set2fa();
    }

    /**
     * @test
     */
    public function should_set_auto_login(): void
    {
        $this->loginManager
            ->setAutoLogin()
            ->shouldBeCalledOnce();

        $this->uut->setAutoLogin();
    }

    /**
     * @test
     */
    public function should_check_auto_login(): void
    {
        $user = $this->prophesize(UserInterface::class)->reveal();

        $this->loginManager
            ->checkAutoLogin()
            ->willReturn($user);

        $result = $this->uut->checkAutoLogin();

        self::assertTrue($result);
    }

    /**
     * @test
     */
    public function should_remove_auto_login(): void
    {
        $this->loginManager
            ->unsetAutoLogin()
            ->shouldBeCalledOnce();

        $this->uut->unsetAutoLogin();
    }

    /**
     * @test
     */
    public function should_return_fingerprint(): void
    {
        $fingerprint = 'jfdshgy12gb3k1`21cdfsdfsdf';

        $this->loginManager
            ->getFingerprint(false)
            ->willReturn($fingerprint);

        $result = $this->uut->getFingerprint();

        self::assertSame($fingerprint, $result);
    }
}