<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Tests\Services;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use MiniatureHappiness\LegacyBundle\Services\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class CookieTest extends TestCase
{
    use ProphecyTrait;

    private ParameterBag|ObjectProphecy|null $cookies;
    private LoggerInterface|ObjectProphecy|null $logger;
    private ?Cookie $uut;
    private string $name;
    private string $data;
    private string $domain;

    public function setUp(): void
    {
        $this->cookies = $this->prophesize(ParameterBag::class);
        $this->logger = $this->prophesize(LoggerInterface::class);

        /** @var Request|ObjectProphecy $request */
        $request = $this->prophesize(Request::class);
        $request->cookies = $this->cookies->reveal();

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack
            ->getMainRequest()
            ->willReturn($request->reveal());

        $this->uut = new Cookie($requestStack->reveal());
        $this->uut->setDeprecationLogger($this->logger->reveal());

        $this->name = 'testCookie';
        $this->data = 'lalalala';
        $this->domain = '/';
    }

    public function tearDown(): void
    {
        $this->cookies = null;
        $this->logger = null;
        $this->uut = null;
    }

    /**
     * @test
     */
    public function should_delete_cookie(): void
    {
        $this->cookies
            ->has($this->name)
            ->willReturn(true)
            ->shouldBeCalledOnce();

        $this->cookies
            ->remove($this->name)
            ->shouldBeCalledOnce();

        $this->uut->delete($this->name, $this->domain);
    }

    /**
     * @test
     */
    public function should_set_cookie(): void
    {
        $this->cookies
            ->set($this->name, $this->data)
            ->shouldBeCalledOnce();

        $this->uut->set($this->name, $this->data, $this->domain);
    }

    /**
     * @test
     */
    public function should_return_cookie(): void
    {
        $this->cookies
            ->has($this->name)
            ->willReturn(true)
            ->shouldBeCalledOnce();

        $this->cookies
            ->get($this->name)
            ->willReturn($this->data)
            ->shouldBeCalledOnce();

        $result = $this->uut->get($this->name);

        $this->assertEquals($this->data, $result);
    }

    /**
     * @test
     */
    public function should_check_if_a_cookie_exists(): void
    {
        $this->cookies
            ->has($this->name)
            ->willReturn(false);

        $this->cookies
            ->set($this->name, $this->data)
            ->will(
                function (array $args) {
                    $this->has($args[0])
                        ->willReturn(true);
                }
            );

        $this->assertFalse($this->uut->exists($this->name));

        $this->uut->set($this->name, $this->data, $this->domain);

        $this->assertTrue($this->uut->exists($this->name));
    }
}