<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Tests\Services;

use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Log\Logger;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;
use MiniatureHappiness\CoreBundle\Exceptions\XMLException;
use MiniatureHappiness\LegacyBundle\Services\Language;

class LanguageTest extends TestCase
{
    use ProphecyTrait;

    private TranslatorInterface|ObjectProphecy|null $translator;
    private ?Language $uut;
    private string $locale = 'en_UK';
    private ?string $encoding;

    public function setUp(): void
    {
        $this->translator = $this->prophesize(TranslatorInterface::class);

        $requestStack = $this->prophesize(RequestStack::class);

        $request = $this->prophesize(Request::class);
        $request->getLocale()->willReturn($this->locale);
        $requestStack->getMainRequest()->willReturn($request->reveal());

        $logger = $this->prophesize(Logger::class)->reveal();

        $this->uut = new Language(
            $requestStack->reveal(),
            $this->translator->reveal()
        );
        $this->uut->setDeprecationLogger($logger);

        $this->encoding = ini_get('default_charset');
    }

    public function tearDown(): void
    {
        $this->translator = null;
        $this->uut = null;
        $this->encoding = null;
    }

    /**
     * @test
     */
    public function should_return_the_set_language(): void
    {
        $this->assertSame($this->locale, $this->uut->getLanguage());
    }

    /**
     * @test
     */
    public function should_return_the_set_encoding(): void
    {
        $this->assertSame($this->encoding, $this->uut->getEncoding());
    }

    /**
     * @test
     */
    public function should_reject_invalid_language_key(): void
    {
        $this->expectException(XMLException::class);

        $id = 'youconix.core.admin.index.title';
        $this->translator
            ->trans($id, [], 'youconix_core')
            ->willReturn($id)
            ->shouldBeCalledOnce();

        $this->uut->get('admin/index/title');
    }

    /**
     * @test
     */
    public function should_resolve_valid_language_key(): void
    {
        $id = 'youconix.core.admin.index.title';
        $expected = 'Admin title';
        $this->translator
            ->trans($id, [], 'youconix_core')
            ->willReturn($expected)
            ->shouldBeCalledOnce();

        $result = $this->uut->get('admin/index/title');

        self::assertSame($expected, $result);
    }

    /**
     * @test
     */
    public function should_insert_the_text_variables(): void
    {
        $text = 'It is a %description% day. Lets go %activity%!';
        $expected = 'It is a beautiful day. Lets go walk!';

        $result = $this->uut->insert(
            $text,
            [
                'description',
                'activity'
            ],
            [
                'beautiful',
                'walk'
            ]
        );

        $this->assertSame($expected, $result);
    }
}