<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Tests\Services;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Log\LoggerInterface;
use MiniatureHappiness\LegacyBundle\Services\Hashing;
use MiniatureHappiness\LegacyBundle\Services\Random;

class HashingTest extends TestCase
{
    use ProphecyTrait;

    private Random|ObjectProphecy|null $random;
    private ?Hashing $uut;

    private string $text = 'test text';
    private string $hashNormal = '$2y$10$YWRocWhpMWcyM3dubGRzI.H9.VfRz3FlRkk5lMByYspSs2fFp.jsm';

    public function setUp(): void
    {
        $this->random = $this->prophesize(Random::class);
        $logger = $this->prophesize(LoggerInterface::class)->reveal();

        $this->uut = new Hashing($this->random->reveal());
        $this->uut->setDeprecationLogger($logger);
    }

    public function tearDown(): void
    {
        $this->random = null;
        $this->uut = null;
    }

    /**
     * @test
     */
    public function should_hash_the_value(): void
    {
        $result = $this->uut->hash($this->text);

        self::assertStringStartsWith('$2y$10$', $result);
        self::assertTrue(password_verify($this->text, $result));
    }

    /**
     * @test
     */
    public function should_verify_the_value(): void
    {
        $this->assertTrue($this->uut->verify($this->text, $this->hashNormal));
    }
}