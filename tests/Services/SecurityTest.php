<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Tests\Services;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\LoggerInterface;
use MiniatureHappiness\LegacyBundle\Services\Security;
use MiniatureHappiness\LegacyBundle\Services\Validation;

class SecurityTest extends TestCase
{
    use ProphecyTrait;

    private ?Security $uut;

    public function setUp(): void
    {
        $logger = $this->prophesize(LoggerInterface::class)->reveal();

        $this->uut = new Security();
        $this->uut->setDeprecationLogger($logger);
    }

    public function tearDown(): void
    {
        $this->uut = null;
    }

    /**
     * @test
     */
    public function should_secure_boolean_values(): void
    {
        $this->assertFalse($this->uut->secureBoolean('0'));
        $this->assertTrue($this->uut->secureBoolean(1));
        $this->assertFalse($this->uut->secureBoolean('-'));
    }

    /**
     * @test
     */
    public function should_secure_int_values(): void
    {
        $this->assertSame(0, $this->uut->secureInt('lalala'));
        $this->assertSame(0, $this->uut->secureInt(-10, true));
        $this->assertSame(10, $this->uut->secureInt(10));
    }

    /**
     * @test
     */
    public function should_secure_float_values(): void
    {
        $this->assertSame(0.00, $this->uut->secureFloat('lalala'));
        $this->assertSame(0.00, $this->uut->secureFloat(-10.1, true));
        $this->assertSame(10.1, $this->uut->secureFloat(10.1));
    }

    /**
     * @test
     */
    public function should_secure_string_values(): void
    {
        $this->assertSame(
            'gffdgq sdgfvserty3r dfgber5t6423wefv asfsdfsd',
            $this->uut->secureString('gffdgq sdgfvserty3r dfgber5t6423wefv asfsdfsd')
        );
        $this->assertNotSame(
            'gffdgq <script>sdgfvserty3r dfgber5t6423wefv asfsdfsd</script>',
            $this->uut->secureString('gffdgq <script>sdgfvserty3r dfgber5t6423wefv asfsdfsd</script>')
        );
        $this->assertNotSame(
            'gffdgq <span onclick="alert(\'test\')">sdgfvserty3r dfgber5t6423wefv asfsdfsd</script>',
            $this->uut->secureString(
                'gffdgq <span onclick="alert(\'test\')">sdgfvserty3r dfgber5t6423wefv asfsdfsd</script>'
            )
        );
        $this->assertNotSame(
            'gffdgq sdgfvserty3r & dfgber5t6423wefv & asfsdfsd',
            $this->uut->secureString('gffdgq sdgfvserty3r & dfgber5t6423wefv & asfsdfsd')
        );
    }

    /**
     * @test
     */
    public function should_validate_email(): void
    {
        $email = 'test@example.com';

        $this->assertTrue($this->uut->checkEmail($email));
    }

    /**
     * @test
     */
    public function should_not_validate_email(): void
    {
        $email = 'test@invalid';

        $this->assertFalse($this->uut->checkEmail($email));
    }

    /**
     * @test
     */
    public function should_validate_URI(): void
    {
        $uri = 'http://example.com/test';

        $this->assertTrue($this->uut->checkURI($uri));
    }

    /**
     * @test
     */
    public function should_not_validate_URI(): void
    {
        $uri = 'http://example';

        $this->assertFalse($this->uut->checkURI($uri));
    }

    /**
     * @test
     */
    public function should_validate_postal_NL(): void
    {
        $postal = '1235 AA';

        $this->assertTrue($this->uut->checkPostalNL($postal));
    }

    /**
     * @test
     */
    public function should_not_validate_postal_NL(): void
    {
        $postal = '1235 A';

        $this->assertFalse($this->uut->checkPostalNL($postal));
    }

    /**
     * @test
     */
    public function should_validate_postal_BE(): void
    {
        $postal = '1324';

        $this->assertTrue($this->uut->checkPostalBE($postal));
    }

    /**
     * @test
     */
    public function should_not_validate_postal_BE(): void
    {
        $postal = '13241AA';

        $this->assertFalse($this->uut->checkPostalBE($postal));
    }
}
