<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

/**
 * @deprecated
 */
class TemplateException extends GeneralException {
}
