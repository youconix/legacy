<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

use Psr\Container\NotFoundExceptionInterface;

/**
 * @deprecated
 */
class MemoryException extends GeneralException implements NotFoundExceptionInterface
{
}
