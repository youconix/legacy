<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

use LogicException;
use MiniatureHappiness\CoreBundle\Interfaces\MiniatureHappinessExceptionInterface;

/**
 * @deprecated
 */
class DateException extends LogicException implements MiniatureHappinessExceptionInterface
{
}