<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

use LogicException;
use MiniatureHappiness\CoreBundle\Interfaces\MiniatureHappinessExceptionInterface;

/**
 * @deprecated
 */
class ConfigException extends LogicException implements MiniatureHappinessExceptionInterface
{

}