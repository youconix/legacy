<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

use Exception;

/**
 * @deprecated
 */
class StackException extends Exception
{

}