<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

/** 
 * @deprecated
 */
class NullPointerException extends GeneralException{
}
