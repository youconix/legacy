<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

use LogicException;
use MiniatureHappiness\CoreBundle\Interfaces\MiniatureHappinessExceptionInterface;

/**
 * Type Exception class
 *
 * @author    Rachelle Scheijen
 * @since     1.0
 * @deprecated
 */
class TypeException extends LogicException implements MiniatureHappinessExceptionInterface
{
}
