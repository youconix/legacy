<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

use Exception;
use MiniatureHappiness\CoreBundle\Interfaces\MiniatureHappinessExceptionInterface;

/**
 * @deprecated
 */
abstract class GeneralException extends Exception implements MiniatureHappinessExceptionInterface
{
    public function __toString(): string
    {
        return sprintf(
            "Thrown %s with message '%s' in %s(%s)\n%s",
            get_class($this),
            $this->message,
            $this->file,
            $this->line,
            $this->getTraceAsString()
        );
    }
}
