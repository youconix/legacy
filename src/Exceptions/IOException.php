<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

use MiniatureHappiness\CoreBundle\Exceptions\IOException as BaseIOException;

/**
 * @deprecated
 */
class IOException extends BaseIOException
{
}