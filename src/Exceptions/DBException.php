<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

use LogicException;
use MiniatureHappiness\CoreBundle\Interfaces\MiniatureHappinessExceptionInterface;

/**
 * Database Exception class
 *
 * @author    Rachelle Scheijen
 * @since     1.0
 * @deprecated
 */
class DBException extends LogicException implements MiniatureHappinessExceptionInterface
{
}