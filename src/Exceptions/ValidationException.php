<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

use LogicException;
use MiniatureHappiness\CoreBundle\Interfaces\MiniatureHappinessExceptionInterface;

/**
 * @deprecated
 */
class ValidationException extends LogicException implements MiniatureHappinessExceptionInterface
{
}