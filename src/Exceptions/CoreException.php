<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

use MiniatureHappiness\CoreBundle\Interfaces\MiniatureHappinessExceptionInterface;
use RuntimeException;

/**
 * @deprecated
 */
class CoreException extends RuntimeException implements MiniatureHappinessExceptionInterface
{
}