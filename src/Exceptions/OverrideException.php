<?php

namespace MiniatureHappiness\LegacyBundle\Exceptions;

use MiniatureHappiness\CoreBundle\Interfaces\MiniatureHappinessExceptionInterface;
use RuntimeException;

/**
 * @deprecated
 */
class OverrideException extends RuntimeException implements MiniatureHappinessExceptionInterface
{
}
