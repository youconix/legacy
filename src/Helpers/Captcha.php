<?php

namespace MiniatureHappiness\LegacyBundle\Helpers;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use MiniatureHappiness\LegacyBundle\Services\Random;

/**
 * Capcha generator
 *
 * @deprecated
 */
class Captcha extends Helper
{
    private SessionInterface $session;
    private Random $random;
    private int $length = 4;

    public function __construct(RequestStack $requestStack, Random $random)
    {
        $this->session = $requestStack->getSession();
        $this->random = $random;
    }

    /**
     * Generates the capcha image
     *
     * @deprecated
     */
    public function generateCapcha(): void
    {
        $s_code = $this->generateCode();

        $this->session->set('capcha', $s_code);
        $s_image = imagecreatetruecolor((40 * $this->length), 40);

        $background = imagecolorallocate($s_image, 255, 255, 255);

        $a_fonts = array(NIV . "fonts/route_3.ttf");

        /* Text size 5 */
        $size = 5;

        /* Text color red */
        $color = imagecolorallocate($s_image, 255, 0, 0);

        /* Build image */
        $s_white = imagecolorallocate($s_image, 255, 255, 255);
        $s_black = imagecolorallocate($s_image, 0, 0, 0);
        $i_left = -25;
        for ($i = 0; $i < $this->length; $i++) {
            $i_text1 = mt_rand(1, 255);       // RGB
            $i_text2 = mt_rand(0, 255);       // RGB
            $i_text3 = mt_rand(0, 255);       // RGB
            $s_text = imagecolorallocate($s_image, $i_text1, $i_text2, $i_text3);

            $i_size = mt_rand(18, 22);      // Font-size?
            $i_angle = mt_rand(0, 45); //angle
            if (mt_rand(0, 1) == 1) {
                $i_angle *= -1;
            }

            $i_up1 = mt_rand(25, 35);    // How much pixels from up?
            $i_up2 = $i_up1 - 1;     // Schade
            $i_up3 = $i_up1 + 2;     // Schade

            $i_left = $i_left + 34;        // Letters zijn nu eenmaal breed...
            $i_left1 = $i_left;              // Hoeveel pixels van links?
            $i_left2 = $i_left1 - 3;    // Schaduw
            $i_left3 = $i_left1 + 3;    // Schaduw

            $i_random_font = array_rand($a_fonts);

            imagettftext($s_image, $i_size, $i_angle, $i_left3, $i_up2, $s_white, $a_fonts[$i_random_font],
                $s_code[$i]);
            imagettftext($s_image, $i_size, $i_angle, $i_left2, $i_up3, $s_black, $a_fonts[$i_random_font],
                $s_code[$i]);
            imagettftext($s_image, $i_size, $i_angle, $i_left1, $i_up1, $s_text, $a_fonts[$i_random_font], $s_code[$i]);
        }

        /* Write image */
        header("Expires: Mon, 26 Jul 1990 05:00:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-type: image/png');
        ImagePng($s_image);

        ImageDestroy($s_image);
    }

    /**
     * Generates the capcha code
     */
    private function generateCode(): string
    {
        return $this->random->numberLetterCaptcha($this->length);
    }

    /**
     * Checks if the given case-insensitive code is correct
     *
     * @deprecated
     */
    public function checkCaptcha(string $code): bool
    {
        $code = strtolower($code);

        if (!$this->session->has('capcha')) {
            return false;
        }

        if ($this->session->get('capcha') !== $code) {
            $this->session->remove('capcha');
            return false;
        }

        $this->session->remove('capcha');
        return true;
    }
}