<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Helpers;

use InvalidArgumentException;
use MiniatureHappiness\CoreBundle\Html\HTML as Base;
use MiniatureHappiness\CoreBundle\Html\HtmlItemInterface;
use MiniatureHappiness\CoreBundle\Html\Container;
use MiniatureHappiness\CoreBundle\Html\Form;
use MiniatureHappiness\CoreBundle\Html\Lists;
use MiniatureHappiness\CoreBundle\Html\Table;
use MiniatureHappiness\CoreBundle\Html\Multimedia;
use MiniatureHappiness\CoreBundle\Html\Reference;
use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * (X)HTML-code generator
 *
 * @deprecated Use MiniatureHappiness\CoreBundle\Html\HTML instead
 */
class HTML extends Helper
{
    use DeprecationTrait;

    private Base $base;

    public function __construct(Base $base)
    {
        $this->base = $base;
    }

    /**
     * Sets the HTML type for the helper
     *
     * @param string $type The HTML type (html|xhtml)
     * @deprecated
     */
    public function setHtmlType(string $type): void
    {
        $this->triggerDeprecationError();
    }

    /**
     * Sets the html type to html 5
     * @deprecated
     */
    public function setHTML5(): void
    {
        $this->triggerDeprecationError();
    }

    /**
     * Generates a div
     *
     * @deprecated
     */
    public function div(string|HtmlItemInterface $content = ''): Container\Div
    {
        $this->triggerBaseDeprecationError('div');

        return $this->base->div($content);
    }

    /**
     * Generates a paragraph
     *
     * @deprecated
     */
    public function paragraph(string|HtmlItemInterface $content): Container\Paragraph
    {
        $this->triggerBaseDeprecationError('paragraph');

        return $this->base->paragraph($content);
    }

    /**
     * Generates a multiply row text input field
     */
    public function textarea(string $name, string|HtmlItemInterface $value = ''): Form\Textarea
    {
        $this->triggerBaseDeprecationError('textarea');

        return $this->base->textarea($name, $value);
    }

    /**
     * Generates a list
     *
     * @deprecated
     */
    public function unList(bool $isNumberedList = false): Lists\ListInterface
    {
        if ($isNumberedList)
        {
            $this->triggerBaseDeprecationError('numberedList');
            return $this->base->numberedList();
        }

        $this->triggerBaseDeprecationError('unNumberedList');
        return $this->base->unNumberedList();
    }

    /**
     * Generates a list item
     *
     * @deprecated
     */
    public function listItem(string|HtmlItemInterface $content): Lists\ListItem
    {
        $this->triggerBaseDeprecationError('listItem');
        return $this->base->listItem($content);
    }

    /**
     * Generates a form
     *
     * @deprecated
     */
    public function form(string $link, string $method, bool $multidataForm = false): Form\Form
    {
        $this->triggerBaseDeprecationError('form');
        return $this->base->form($link, $method, $multidataForm);
    }

    /**
     * Generates a table
     *
     * @deprecated
     */
    public function table(): Table\Table
    {
        $this->triggerBaseDeprecationError('table');
        return $this->base->table();
    }

    /**
     * Generates a table row
     *
     * @deprecated
     */
    public function tableRow(): Table\TableRow
    {
        $this->triggerBaseDeprecationError('tableRow');
        return $this->base->tableRow();
    }

    /**
     * Generates a table cel
     *
     * @deprecated
     */
    public function tableCell(string|HtmlItemInterface $value = ''): Table\TableCell
    {
        $this->triggerBaseDeprecationError('tableCell');
        return $this->base->tableCell($value);
    }

    /**
     * Generates a text input field
     *
     * @param string $type The type of the field (text|password|hidden|email)
     * @deprecated
     */
    public function input(string $name, string $type, string $value = ''): Form\Input
    {
        switch ($type) {
            case 'password':
                $this->triggerBaseDeprecationError('password');
                return $this->base->password($name);

            case 'hidden':
                $this->triggerBaseDeprecationError('hidden');
                return $this->base->hidden($name, $value);

            case 'email':
                $this->triggerBaseDeprecationError('email');
                return $this->base->email($name, $value);

            default:
                $this->triggerBaseDeprecationError('text');
                return $this->base->text($name, $value);
        }
    }

    /**
     * Generates a button
     *
     * @param string $type The type of the button (button|reset|submit)
     * @deprecated
     */
    public function button(string $value, string $name, string $type): Form\Button
    {
        switch ($type) {
            case 'submit':
                $this->triggerBaseDeprecationError('submit');
                return $this->base->submit($name, $value);

            case 'reset':
                $this->triggerBaseDeprecationError('reset');
                return $this->base->reset($name, $value);

            default:
                $this->triggerBaseDeprecationError('button');
                return $this->base->button($name, $value);
        }
    }

    /**
     * Generates a link for linking to other pages
     *
     * @deprecated
     */
    public function link(string $url, string $value = ''): Reference\Link
    {
        $this->triggerBaseDeprecationError('link');
        return $this->base->link($url, $value);
    }

    /**
     * Generates a image
     *
     * @deprecated
     */
    public function image(string $url): Multimedia\Image
    {
        $this->triggerDeprecationError('image');
        return $this->base->image($url);
    }

    /**
     * Generates a header
     *
     * @param int $level The type of header (1|2|3|4|5)
     * @throws InvalidArgumentException
     * @deprecated Use h1, h2, h3, h4 or h5 instead.
     */
    public function header(int $level, string|HtmlItemInterface $content): Container\InlineHeader
    {
        $this->triggerDeprecationError();
        switch ($level) {
            case 1:
                $this->triggerBaseDeprecationError('h1');
                return $this->base->h1($content);

            case 2:
                $this->triggerBaseDeprecationError('h2');
                return $this->base->h2($content);

            case 3:
                $this->triggerBaseDeprecationError('h3');
                return $this->base->h3($content);

            case 4:
                $this->triggerBaseDeprecationError('h4');
                return $this->base->h4($content);

            case 5:
                $this->triggerBaseDeprecationError('h5');
                return $this->base->h5($content);

            default:
                throw new InvalidArgumentException(sprintf('Unknown header type %d', $level));
        }
    }

    /**
     * Generates a radio button
     *
     * @deprecated
     */
    public function radio(string $name = '', string $value = ''): Form\Radio
    {
        $this->triggerBaseDeprecationError('radio');
        return $this->base->radio($name, $value);
    }

    /**
     * Generates a checkbox
     *
     * @deprecated
     */
    public function checkbox(string $name = '', string $value = ''): Form\Checkbox
    {
        $this->triggerBaseDeprecationError('checkbox');
        return $this->base->checkbox($name, $value);
    }

    /**
     * Generates a select list
     *
     * @deprecated
     */
    public function select(string $name): Form\Select
    {
        $this->triggerBaseDeprecationError('select');

        return new Form\Select($name);
    }

    /**
     * Generates a link to a external stylesheet
     *
     * @param string $media The media where the stylesheet is for, optional (screen|print|mobile)
     * @deprecated
     */
    public function stylesheetLink(string $link, string $media = 'screen'): Reference\StylesheetLink
    {
        $this->triggerBaseDeprecationError('stylesheetLink');
        return $this->base->stylesheetLink($link, $media);
    }

    /**
     * Generates the stylesheet tags for in page CSS
     *
     * @deprecated
     */
    public function stylesheet(string $css): Reference\StyleSheet
    {
        $this->triggerBaseDeprecationError('stylesheet');
        return $this->base->stylesheet($css);
    }

    /**
     * Generates a link to a external javascript file
     *
     * @deprecated
     */
    public function javascriptLink(string $link): Reference\JavascriptLink
    {
        $this->triggerBaseDeprecationError('javascriptLink');
        return $this->base->javascriptLink($link);
    }

    /**
     * Generates the javascript tags for in page javascript
     *
     * @deprecated
     */
    public function javascript(string $javascript): Reference\Javascript
    {
        $this->triggerBaseDeprecationError('javascript');
        return $this->base->javascript($javascript);
    }

    /**
     * Generates a meta tag
     *
     * @deprecated
     */
    public function metatag(string $name, string $content, string $scheme = ''): Reference\Metatag
    {
        $this->triggerBaseDeprecationError('metatag');
        return $this->base->metatag($name, $content, $scheme);
    }

    /**
     * @deprecated
     */
    public function span(string|HtmlItemInterface $content): Container\Span
    {
        $this->triggerBaseDeprecationError('span');
        return $this->base->span($content);
    }

    /**
     * Generates an audio object
     *
     * @deprecated
     */
    public function audio(string $url, string $type = 'ogg'): Multimedia\Audio
    {
        $this->triggerBaseDeprecationError('audio');
        return $this->base->audio($url, $type);
    }

    /**
     * Generates a video object
     *
     * @deprecated
     */
    public function video(string $url, string $type = 'WebM'): Multimedia\Video
    {
        $this->triggerBaseDeprecationError('video');
        return $this->base->video($url, $type);
    }

    /**
     * Generates a canvas object
     *
     * @deprecated
     */
    public function canvas(): Multimedia\Canvas
    {
        $this->triggerBaseDeprecationError('canvas');

        return $this->base->canvas();
    }

    /**
     * Generates a header object
     *
     * @deprecated
     */
    public function pageHeader(string|HtmlItemInterface $content = ''): Container\Header
    {
        $this->triggerBaseDeprecationError('pageHeader');
        return $this->base->pageHeader($content);
    }

    /**
     * @deprecated
     */
    public function pageFooter(string|HtmlItemInterface $content = ''): Container\Footer
    {
        $this->triggerBaseDeprecationError('pageFooter');
        return $this->base->pageFooter($content);
    }

    /**
     * @deprecated
     */
    public function navigation(string|HtmlItemInterface $content = ''): Container\Navigation
    {
        $this->triggerBaseDeprecationError('navigation');
        return $this->base->navigation($content);
    }

    private function triggerBaseDeprecationError(string $alternative = ''): void
    {
        $this->triggerDeprecationError(
            sprintf('%s:%s', Base::class, $alternative)
        );
    }
}
