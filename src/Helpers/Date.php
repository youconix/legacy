<?php

namespace MiniatureHappiness\LegacyBundle\Helpers;

/**
 * Date manipulation helper
 *
 * @deprecated
 */
class Date extends Helper {
    /**
     * Calculates the timestamp from the given parameters
     *
     * @deprecated
     */
    public function getTime(int $hours = -1, int $minutes = -1, int $seconds = -1, int $day = -1, int $month = -1, int $year = -1): int
    {
        if( $hours === -1 )    $hours = date("H");
        if( $minutes === -1 )  $minutes = date("i");
        if( $seconds === -1 )  $seconds = date("s");
        if( $month === -1 )    $month = date("n");
        if( $day === -1 )      $day = date("j");
        if( $year === -1 )     $year = date("Y");

        return mktime($hours, $minutes, $seconds, $month, $day, $year);
    }

    /**
     * Returns the timestamp in the future
     *
     * @deprecated
     */
    public function getFutureTime(int $days = 0, int $months = 0, int $years = 0 , int $hours = 0, int $minutes = 0, int $seconds = 0): int
    {
        return mktime(
            date("H") + $hours,
            date("i") + $minutes,
            date("s") + $seconds,
            date("n") + $months,
            date("j") + $days,
            date("Y") + $years
        );
    }

    /**
     * Calculates the timestamp from now added with the given seconds
     *
     * @deprecated
     */
    public function getTimeFrom(int $seconds): int
    {
        return $this->now() + $seconds;
    }

    /**
     * Returns the number of seconds in a day
     *
     * @deprecated
     */
    public function getDaySeconds(): int
    {
        return 86400;
    }

    /**
     * Returns the number of seconds in a week
     *
     * @deprecated
     */
    public function getWeekSeconds(): int
    {
        return $this->getDaySeconds()*7;
    }

    /**
     * Returns the current timestamp
     *
     * @deprecated
     */
    public function now(): int
    {
        return time();
    }

    /**
     * Validates the date and time
     *
     * @deprecated
     */
    public function validateDateTime(int $day, int $month, int $year, int $hours, int $minutes, int $seconds): bool
    {
        if (!$this->validateTime($hours, $minutes, $seconds) || !$this->validateDate($day, $month, $year)) {
            return false;
        }

        return true;
    }

    /**
     * Validates time
     */
    public function validateTime(int $hours, int $minutes, int $seconds): bool
    {
        if ($hours < 0 || $hours > 23 || $minutes < 0 || $minutes > 59 || $seconds < 0 || $seconds > 59) {
            return false;
        }

        return true;
    }

    /**
     * Validates the date
     *
     * @deprecated
     */
    public function validateDate(int $day, int $month, $year): bool
    {
        if ($month < 1 || $month > 12) {
            return false;
        }

        $days = $this->getDaysMonth($month, $year);

        if ($day < 1 || $day > $days) {
            return false;
        }

        return true;
    }

    /**
     * Returns the number of days from the given month
     *
     * @deprecated
     */
    public function getDaysMonth(int $month, int $year): int
    {
        switch ($month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12 :
                return 31;

            case 4:
            case 6:
            case 9:
            case 11:
                return 30;

            case 2 :
                if ($year % 400 === 0) {
                    return 29;
                }
                if ($year % 100 === 0) {
                    return 28;
                }
                if ($year % 4 === 0) {
                    return 29;
                }
                return 28;
        }
    }
}