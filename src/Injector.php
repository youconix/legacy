<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle;

use Symfony\Component\DependencyInjection\Container;

/**
 * @deprecated
 */
class Injector
{
    private static Container $container;

    public static function injectContainer(Container $container): void
    {
        self::$container = $container;
    }

    /**
     * @return mixed The loaded class
     */
    public static function load(string $className)
    {
        $parentClassName = self::getParentClassName($className);
    }

    private static function getParentClassName(string $className): string
    {
        return get_parent_class($className);
    }
}