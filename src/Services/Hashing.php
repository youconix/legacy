<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services;

use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * Hashing service
 *
 * This file is part of Miniature-happiness
 *
 * @copyright Youconix
 * @author Rachelle Scheijen
 * @version 1.0
 * @since 2.0
 * @deprecated
 */
class Hashing extends AbstractService
{
    use DeprecationTrait;

    protected Random $random;

    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    /**
     * @inheritDoc
     */
    public static function isSingleton(): bool
    {
        return true;
    }

    /**
     * Creates a hash
     *
     * @deprecated
     */
    public function hash(string $text): string
    {
        $this->triggerDeprecationError('Symfony hashing');

        return password_hash($text, PASSWORD_BCRYPT);
    }

    /**
     * Verifies the text against the hash
     *
     * @deprecated
     */
    public function verify(string $text, string $stored_text): bool
    {
        $this->triggerDeprecationError('Symfony hashing');

        return password_verify($text, $stored_text);
    }

    /**
     * Creates a salt
     *
     * @deprecated
     */
    public function createSalt(): string
    {
        $this->triggerDeprecationError('Symfony hashing');

        if (function_exists('openssl_random_pseudo_bytes')) {
            return bin2hex(openssl_random_pseudo_bytes(30));
        }

        return $this->random->randomAll(30);
    }

    /**
     * Creates a random string with the given length
     *
     * @deprecated
     */
    public function createRandom(int $length = 15): string
    {
        $this->triggerDeprecationError();

        return $this->random->randomAll($length);
    }
}