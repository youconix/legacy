<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services;

use MiniatureHappiness\LegacyBundle\Interfaces\SettingsInterface;
use RuntimeException;

/**
 * Legacy settings class
 *
 * @deprecated
 */
class XmlSettings extends Xml implements SettingsInterface
{
    private string $settingsDirectory;

    public function __construct(string $settingsDirectory)
    {
        $this->settingsDirectory  = $settingsDirectory;

        if( file_exists($this->settingsDirectory.'/settings.xml') )
            $this->load($this->settingsDirectory.'/settings.xml');
        else {
            throw new RuntimeException("Missing settings file");
        }

        $this->startTag = 'settings';
    }

    /**
     * Saves the settings file
     */
    public function save(string $file = ''): void
    {
        parent::save($this->settingsDirectory.'/settings.xml');
    }
}