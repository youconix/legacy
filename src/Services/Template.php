<?php

namespace MiniatureHappiness\LegacyBundle\Services;

use Exception;
use Memory;
use MiniatureHappiness\LegacyBundle\Interfaces\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use MiniatureHappiness\CoreBundle\Exceptions\IOException;
use MiniatureHappiness\CoreBundle\Html\HtmlItemInterface;
use MiniatureHappiness\LegacyBundle\Exceptions\TemplateException;
use MiniatureHappiness\LegacyBundle\MemoryLoader;
use MiniatureHappiness\LegacyBundle\Services\Templating\TemplateParser;
use MiniatureHappiness\LegacyBundle\Services\Templating\TemplateSettings;
use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * Template parser for handling .tpl files
 *
 * @deprecated
 */
class Template extends AbstractService implements OutputInterface
{
    use DeprecationTrait;

    private TemplateParser $templateParser;
    private TemplateSettings $templateSettings;
    private MemoryLoader $memory;
    private string $cacheDirectory;
    private string $rootDirectory;
    private array $styleBranch;
    private string $layout;
    private string $template;
    private string $templateDir;
    private bool $compression = false;
    private bool $preventOutput = false;

    public function __construct(
        TemplateParser $templateParser,
        TemplateSettings $templateSettings,
        MemoryLoader $memory,
        string $cacheDirectory,
        string $rootDirectory
    ) {
        $this->templateParser = $templateParser;
        $this->templateSettings = $templateSettings;
        $this->memory = $memory;
        $this->cacheDirectory = $cacheDirectory;
        $this->rootDirectory = $rootDirectory . '/';

        $this->init();
    }

    /**
     * Inits the template parser
     *
     * @throws TemplateException if the layout does not exist
     * @throws IOException if the layout is not readable
     */
    private function init(): void
    {
        $this->templateDir = $this->templateSettings->getTemplateDirectory();
        $this->layout = $this->templateSettings->getLayout();

        $this->styleBranch = $this->getStyleBranches($this->templateDir);

        $this->loadView();
        $this->compression();
    }

    /**
     * Checks if gzip compression is available and enables it
     */
    private function compression(): void
    {
        /* Check encoding */
        if (empty($_SERVER['HTTP_ACCEPT_ENCODING'])) {
            return;
        }

        /* Check server wide compression */
        if ((ini_get('zlib.output_compression') == 'On' || ini_get('zlib.output_compression_level') > 0) || ini_get('output_handler') == 'ob_gzhandler') {
            return;
        }

        if (extension_loaded('zlib') && (stripos($_SERVER['HTTP_ACCEPT_ENCODING'],
                    'gzip') !== false) && !defined('DEBUG')) {
            ob_start('ob_gzhandler');
            $this->compression = true;
        }
    }

    /**
     * Get the routing cache dir
     *
     * @deprecated
     */
    public function getCacheDir(): string
    {
        return $this->cacheDirectory . 'twig/';
    }

    /**
     * @deprecated
     */
    public function getLocationDir(): string
    {
        return $this->templateDir;
    }

    /**
     * Returns the loaded template directory
     *
     * @throws TemplateException
     * @deprecated
     */
    public function getStylesDir($styleDirName = null): string
    {
        $styleDirName = $styleDirName ?: $this->templateDir;

        if (!in_array($styleDirName, $this->templateSettings->getStylesDirs())) {
            throw new TemplateException("Invalid styles dir " . $styleDirName . ". ");
        }

        return $this->rootDirectory . 'styles/' . $styleDirName . '/';
    }

    /**
     * Returns all available styles directories
     *
     * @deprecated
     */
    public function getStylesDirs(): array
    {
        return $this->templateSettings->getStylesDirs();
    }

    /**
     * Return whether a file exists in one of the branch directories
     *
     * @deprecated
     */
    public function fileExists($filePath): bool
    {
        foreach ($this->styleBranch as $styleDir => $a_style) {
            if (file_exists($this->getStylesDir($styleDir) . 'templates/' . $filePath)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Prevents the Template class from outputting any text
     *
     * @deprecated
     */
    public function preventOutput(): void
    {
        $this->preventOutput = true;
    }

    /**
     * @inheritDoc
     */
    public function loadView($view = ''): void
    {
        /* Check view */
        if ($view === '') {
            if (isset($_GET['command'])) {
                $view = $_GET['command'];
            } elseif (isset($_POST['command'])) {
                $view = $_POST['command'];
            } else {
                $view = 'index';
            }
        }

        if (substr($view, -4) !== '.tpl' && substr($view, -5) !== '.twig') {
            $view .= '.tpl';
        }

        if ($view[0] === '/') {
            $view = substr($view, 1);
        } else {
            $view = str_replace('.php', '', $this->memory->getPage()) . '/' . $view;
        }

        if ($view == 'entry/index.tpl') {
            return; // View is set manually
        }

        if (!$this->fileExists($view)) {
            /* View not found */
            throw new TemplateException('Can not load view ' . $view . '.');
        }

        $this->set('VIEW', $view);
    }

    /**
     * Sets the link to the page-header
     *
     * @deprecated
     */
    public function headerLink(string|HtmlItemInterface $link): void
    {
        if ($link instanceof HtmlItemInterface) {
            $link = $link->generateItem();
        }

        $link = trim($link);

        if (str_starts_with($link, '<link')) {
            $this->templateParser->addCssLink($link);
        } elseif (str_starts_with($link, '<script')) {
            if (stripos($link, 'src=') !== false) {
                $this->templateParser->addJavascriptLink($link);
            } else {
                $this->templateParser->addJavascript($link);
            }
        } elseif (str_starts_with($link, '<meta')) {
            $this->templateParser->addMetaTag($link);
        } elseif (str_starts_with($link, '<style')) {
            $this->templateParser->addCss($link);
        }
    }

    /**
     * @deprecated
     */
    public function setLayout($name): void
    {
        $this->layout = 'layouts/' . $name . '.tpl';
    }

    /**
     * @inheritDoc
     */
    public function set(string|array $key, $value = null): void
    {
        if (is_array($key)) {
            $this->templateParser->addContentSet($key);
            return;
        }

        if (func_num_args() != 2) {
            throw new Exception("Missing argument.");
        }

        $this->templateParser->addContent($key, $value);
    }

    /**
     * @inheritDoc
     */
    public function append(string $key, mixed $value): void
    {
        $this->templateParser->append($key, $value);
    }

    /**
     * @inheritDoc
     */
    public function buildPage(): string
    {
        $this->template = $this->templateParser->build($this->layout, $this->templateDir);

        return $this->template;
    }

    /**
     * Prints the page to the screen and pushes it to the visitor
     *
     * @deprecated
     */
    public function printToScreen(): void
    {
        if ($this->preventOutput) {
            return;
        }

        $this->buildPage();
        header('Content-Type: text/html; charset=' . ini_get('default_charset'));
        echo($this->template);
        $this->preventOutput = true;
    }

    private function getStyleBranches(string $styleDir): array
    {
        $styleBranches = [];
        do {
            $stylePath = $this->getStylesDir($styleDir);

            $styleConfig = Yaml::parse(file_get_contents($stylePath . 'style.yml'));
            $styleBranches[$styleDir] = [
                'config' => $styleConfig,
            ];

            $styleDir = $styleConfig['extends'] ?? null;
        } while (!empty($styleDir));

        return $styleBranches;
    }
}
