<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services;

use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * Headers generating service
 *
 * @author Rachelle Scheijen
 * @version 1.0
 * @since 2.0
 * @deprecated
 */
class Headers
{
    use DeprecationTrait;

    protected RouterInterface $router;
    protected HeaderBag $headers;

    public function __construct(RouterInterface $router, RequestStack $requestStack)
    {
        $this->router = $router;
        $this->headers = $requestStack->getMainRequest()->headers;
    }

    /**
     * @deprecated
     */
    public function clear(): self
    {
        $this->triggerDeprecationError('Symfony Request -> headers');

        foreach ($this->headers->keys() as $key) {
            $this->headers->remove($key);
        }

        $this->headers->set('Content-Type', 'text/html');

        return $this;
    }

    /**
     * Sets the given content type
     *
     * @deprecated
     */
    public function contentType(string $contentType): self
    {
        $this->triggerDeprecationError('Symfony Request -> headers -> set');

        $this->headers->set('Content-Type', $contentType);

        return $this;
    }

    /**
     * Sets the javascript content type
     *
     * @deprecated
     */
    public function setJavascript(): self
    {
        $this->triggerDeprecationError('Symfony Request -> headers -> set');

        $this->headers->set('Content-Type', 'application/javascript');

        return $this;
    }

    /**
     * Sets the CSS content type
     *
     * @deprecated
     */
    public function setCSS(): self
    {
        $this->triggerDeprecationError('Symfony Request -> headers -> set');

        $this->headers->set('Content-Type', 'text/css');

        return $this;
    }

    /**
     * Sets the XML content type
     *
     * @deprecated
     */
    public function setXML(): self
    {
        $this->triggerDeprecationError('Symfony Request -> headers -> set');

        $this->headers->set('Content-Type', 'application/xml');

        return $this;
    }

    /**
     * Sets the last modified header
     *
     * @deprecated
     */
    public function modified(int $modified): self
    {
        $this->triggerDeprecationError('Symfony Request -> headers -> set');

        $this->headers->set('Last-Modified', gmdate('D, d M Y H:i:s', $modified) . ' GMT');

        return $this;
    }

    /**
     * Sets the cache time, -1 for no cache
     *
     * @deprecated
     */
    public function cache(int $cache): self
    {
        $this->triggerDeprecationError('Symfony Request -> headers -> set');

        if ($cache === -1) {
            $this->headers->set('Expires', 'Thu, 01-Jan-70 00:00:01 GMT');
            $this->headers->set('Last-Modified', gmdate('D, d M Y H:i:s') . ' GMT');
            $this->headers->set('Cache-Control', 'no-store, no-cache, must-revalidate');
            $this->headers->set('Cache-Control', 'post-check=0, pre-check=0');
            $this->headers->set('Pragma', 'no-cache');
        } else {
            $this->headers->set('Expires', gmdate('D, d M Y H:i:s', (time() + $cache)) . ' GMT');
        }

        return $this;
    }

    /**
     * Sets the content length in bytes
     *
     * @deprecated
     */
    public function contentLength(int $length): self
    {
        $this->triggerDeprecationError('Symfony Request -> headers -> set');

        $this->headers->set('Content-Length', (string)$length);

        return $this;
    }

    /**
     * Force downloads a file
     * Program will halt
     *
     * @deprecated
     */
    public function forceDownloadFile(string $file, string $contentType): void
    {
        $this->triggerDeprecationError('Symfony BinaryResponse');

        $size = filesize($file);
        $filename = basename($file);

        $this->forceDownload($size, $filename, $contentType);

        readfile($file);
        exit();
    }

    /**
     * Force downloads the given content
     * Program will halt
     *
     * @deprecated
     */
    public function forceDownloadContent(string $content, string $contentType, string $filename): void
    {
        $this->triggerDeprecationError('Symfony BinaryResponse');

        $size = strlen($content);

        $this->forceDownload($size, $filename, $contentType);

        echo($content);
        exit();
    }

    /**
     * Sets a header
     *
     * @deprecated
     */
    public function setHeader(string $key, string $content, string $extra = null): self
    {
        $this->triggerDeprecationError('Symfony Request -> headers -> set');

        if (stripos($key, 'http') !== false) {
            $key = strtoupper($key);
        }

        $values = $extra === null ? $content : [$content, $extra];

        $this->headers->set($key, $values);

        return $this;
    }

    /**
     * Sends the 301 redirect header
     * Program will halt
     *
     * @deprecated
     */
    public function redirect(string $location): void
    {
        $this->triggerDeprecationError('Symfony controller -> redirect');

        if (stripos($location, 'http') === false && stripos($location, 'ftp') === false) {
            if (substr($location, 0, 1) !== '/') {
                $location = '/' . $location;
            }
        }

        header('Location', $location);
        exit();
    }

    /**
     * Sends the 301 redirect header for the given path
     * Program will halt
     *
     * @deprecated
     */
    public function redirectPath(string $path, array $parameters = []): void
    {
        $this->triggerDeprecationError('Symfony controller -> redirectToRoute');

        $route = $this->router->generate($path, $parameters);

        header('Location: ' . $route);
        exit();
    }

    /**
     * Returns if a force download was executed
     *
     * @deprecated
     */
    public function isForceDownload(): bool
    {
        $this->triggerDeprecationError();

        return false;
    }

    /**
     * Returns if a redirect was executed
     *
     * @deprecated
     */
    public function isRedirect(): bool
    {
        $this->triggerDeprecationError();

        return $this->headers->has('Location');
    }

    /**
     * Returns the headers
     *
     * @deprecated
     */
    public function getHeaders(): array
    {
        $this->triggerDeprecationError('Symfony Request -> headers -> all');

        return $this->headers->all();
    }

    /**
     * Returns if the template should be skipped
     *
     * @deprecated
     */
    public function skipTemplate(): bool
    {
        $this->triggerDeprecationError();

        return false;
    }

    /**
     * Imports the given headers
     *
     * @deprecated
     */
    public function importHeaders(array $headers): self
    {
        $this->triggerDeprecationError('Symfony Request -> headers -> set');

        foreach ($headers as $key => $header) {
            $this->headers->set($key, $header);
        }

        return $this;
    }

    /**
     * Sends the cached headers to the client
     *
     * @deprecated
     */
    public function printHeaders(): self
    {
        $this->triggerDeprecationError();

        return $this;
    }

    private function forceDownload(int $size, string $filename, string $contentType): void
    {
        header('Content-Type: ' . $contentType);
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Length: ' . $size);
        header('Expires: Thu, 01-Jan-70 00:00:01 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache');
    }
}
