<?php

namespace MiniatureHappiness\LegacyBundle\Services;

use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * Generator for random numbers and letters
 *
 * @deprecated
 */
class Random extends AbstractService
{
    use DeprecationTrait;

    /**
     * Generates a random code of letters
     *
     * @deprecated
     */
    public function letter(int $length, bool $includeUppercase = false): string
    {
        $this->triggerDeprecationError();

        $codeString = 'abcdefghijklmnopqrstuvwxyz';
        if ($includeUppercase) {
            $codeString = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ';
        }

        return $this->createRandomValue($length, $codeString);
    }

    /**
     * Generates a random code of numbers
     *
     * @deprecated
     */
    public function number(int $length): string
    {
        $this->triggerDeprecationError();

        $codeString = '1234567890';

        return $this->createRandomValue($length, $codeString);
    }

    /**
     * Generates a random code of numbers and letters
     *
     * @deprecated
     */
    public function numberLetter(int $length, bool $includeUppercase): string
    {
        $this->triggerDeprecationError();

        $codeString = 'abcdefghijklmnopqrstuvwxyz1234567890';
        if ($includeUppercase) {
            $codeString = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ1234567890';
        }

        return $this->createRandomValue($length, $codeString);
    }

    /**
     * Generates a random code of numbers and letters for a captcha
     *
     * @deprecated
     */
    public function numberLetterCaptcha($length): string
    {
        $this->triggerDeprecationError();

        $codeString = 'abcdefhjkmnpqrstuvwxyz23456789';

        return $this->createRandomValue($length, $codeString);
    }

    /** Generates a random code of all signs
     *
     * @deprecated
     */
    public function randomAll($length): string
    {
        $this->triggerDeprecationError();

        $codeString = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ1234567890`~!@#$%^&*()-_+={[}];:\|<,>.?/';

        return $this->createRandomValue($length, $codeString);
    }

    private function createRandomValue(int $length, string $codeString): string
    {
        $size = strlen($codeString);
        $code = '';
        for ($i = 1; $i <= $length; $i++) {
            $position = rand(0, $size);

            $code .= $codeString[$position];
        }

        return $code;
    }
}