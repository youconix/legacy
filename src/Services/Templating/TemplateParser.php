<?php

namespace MiniatureHappiness\LegacyBundle\Services\Templating;

use Exception;
use Twig\Environment;
use MiniatureHappiness\CoreBundle\Html\Html;
use MiniatureHappiness\LegacyBundle\MemoryLoader;

/**
 * Template parser for handling .tpl file content
 *
 * @deprecated
 */
class TemplateParser
{
    private Environment $twig;
    private Html $html;
    private array $css = [];
    private array $cssLinks = [];
    private array $javascript = [];
    private array $javascriptLinks = [];
    private array $metaTags = [];
    private array $content = [];
    private string $view;
    private string $rootDirectory;

    public function __construct(Environment $twig, Html $html, string $rootDirectory)
    {
        $this->twig = $twig;
        $this->html = $html;
        $this->rootDirectory = $rootDirectory;
    }

    /**
     * @deprecated
     */
    public function setView(string $view): void
    {
        $this->view = $view;
    }

    /**
     * @deprecated
     */
    public function addCss(string $css): void
    {
        $this->css[] = $css;
    }

    /**
     * @deprecated
     */
    public function addCssLink(string $link): void
    {
        $this->cssLinks[] = $link;
    }

    /**
     * @deprecated
     */
    public function addMetaTag(string $metaTag): void
    {
        $this->metaTags[] = $metaTag;
    }

    /**
     * @deprecated
     */
    public function addJavascript(string $javascript): void
    {
        $javascript = str_replace(['<!--', '//-->'], ['', ''], $javascript);

        $this->javascript[] = trim($javascript);
    }

    /**
     * @deprecated
     */
    public function addJavascriptLink(string $javascriptLink): void
    {
        $this->javascriptLinks[] = $javascriptLink;
    }

    /**
     * @throws Exception if $s_value is not a string and not a subclass of CoreHtmlItem
     * @deprecated
     */
    public function addContent(string $key, $value): void
    {
        if (is_object($value) && is_subclass_of($value, 'CoreHtmlItem')) {
            $value = $value->generateItem();
        }

        $this->content[$key] = $value;
    }

    /**
     * @deprecated
     */
    public function addContentSet(array $contentSet): void
    {
        foreach ($contentSet as $key => $value) {
            $this->addContent($key, $value);
        }
    }

    /**
     * @throws Exception
     * @deprecated
     */
    public function append(string $key, $value): void
    {
        if (!isset($this->content[$key])) {
            $this->content[$key] = [$value];

            return;
        }

        if (!is_array($this->content[$key])) {
            throw new Exception("Can only append to arrays.");
        }

        $this->content[$key][] = $value;
    }

    public function build(string $layout, string $templateDir): string
    {
        if (!isset($this->content['NIV'])) {
            $this->addContent('NIV', $this->rootDirectory);
        }

        $this->addContent('style_dir', $this->rootDirectory . 'styles/' . $templateDir . '/');

        if (!MemoryLoader::isAjax()) {
            /* Write header-blok to template */
            $headBlock = $this->buildHeadBlock();

            $this->addContent('headblock', $headBlock);
        }

        return $this->twig->render($layout, $this->content);
    }

    private function buildHeadBlock(): string
    {
        $headBlock = '';

        foreach ($this->css as $item) {
            $headBlock .= $item . PHP_EOL;
        }
        foreach ($this->cssLinks as $item) {
            $headBlock .= $item . PHP_EOL;
        }
        foreach ($this->metaTags as $item) {
            $headBlock .= $item . PHP_EOL;
        }
        foreach ($this->javascriptLinks as $item) {
            $headBlock .= $item . PHP_EOL;
        }
        foreach ($this->javascriptLinks as $item) {
            $javascript = $this->html->javascript($item);

            $headBlock .= $javascript->generateItem() . PHP_EOL;
        }

        return $headBlock;
    }
}