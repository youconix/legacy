<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services\Templating;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use MiniatureHappiness\LegacyBundle\Exceptions\TemplateException;
use MiniatureHappiness\LegacyBundle\MemoryLoader;
use Symfony\Component\Routing\RouterInterface;

/**
 * Settings for the .tpl template parser
 *
 * @deprecated
 */
class TemplateSettings
{
    private SessionInterface $session;
    private ParameterBag $cookies;
    private RouterInterface $router;
    private string $templateDirectory;
    private string $rootDirectory;
    private ?array $styleDirs;

    public function __construct(RequestStack $requestStack, RouterInterface $router, string $templateDirectory, string $rootDirectory)
    {
        $request = $requestStack->getMainRequest();

        $this->session = $requestStack->getSession();
        $this->cookies = $request->cookies;
        $this->router = $router;
        $this->templateDirectory = $templateDirectory;
        $this->rootDirectory = $rootDirectory . '/';
    }

    /**
     * @deprecated
     */
    public function getTemplateDirectory(): string
    {
        $templateDir = $this->templateDirectory;

        if (isset($_GET['private_style_dir'])) {
            $styleDir = $this->clearLocation($_GET['private_style_dir']);
            if (file_exists($this->rootDirectory . 'styles/' . $styleDir . '/templates/layouts')) {
                $templateDir = $styleDir;
                $this->cookies->set('private_style_dir', $templateDir);
            }
            $this->cookies->remove('private_style_dir');
        } elseif ($this->cookies->has('private_style_dir')) {
            $styleDir = $this->clearLocation($this->cookies->get('private_style_dir'));
            if ($this->file->exists($this->rootDirectory . 'styles/' . $styleDir . '/templates/layouts')) {
                $templateDir = $styleDir;
                $this->cookies->set('private_style_dir', $templateDir);
            } else {
                $this->cookies->remove('private_style_dir');
            }
        }

        $validStyleDirectories = $this->getStylesDirs();

        if (isset($_GET['_useTemplate']) && ($_GET['_useTemplate'] == '_reset' || in_array($_GET['_useTemplate'],
                    $validStyleDirectories, true))) {
            if ($_GET['_useTemplate'] == '_reset') {
                if ($this->session->has('useTemplate')) {
                    $this->session->remove('useTemplate');
                }
            } else {
                $this->session->set('useTemplate', $_GET['_useTemplate']);
                $templateDir = $_GET['_useTemplate'];
            }

            $this->router->redirect(
                $this->router->getRoute(),
                $this->router->getArgs()
            );
        } elseif ($this->session->has('useTemplate')) {
            if (!in_array($this->session->get('useTemplate'), $validStyleDirectories, true)) {
                $this->session->remove('useTemplate');
            } else {
                $templateDir = $this->session->get('useTemplate');
            }
        }

        return $templateDir;
    }

    /**
     * @throws TemplateException
     * @deprecated
     */
    public function getLayout(): string
    {
        if (MemoryLoader::isAjax()) {
            return 'layouts/default_AJAXVIEW.tpl';
        }

        $layout = $this->detectLayout();

        if (!file_exists($layout)) {
            throw new TemplateException('Can not load layout ' . $layout);
        }

        return $layout;
    }

    /**
     * @deprecated
     */
    public function getStylesDirs(): array
    {
        if ($this->styleDirs === null) {
            $stylesRoot = $this->rootDirectory . 'styles/';

            $styleDirs = scandir($stylesRoot);

            $styleDirs = array_diff($styleDirs, ['.', '..']);

            $this->styleDirs = array_filter($styleDirs, function ($styleDir) use ($stylesRoot) {
                return file_exists($stylesRoot . $styleDir . '/style.yml');
            });
        }
        return $this->styleDirs;
    }

    private function detectLayout(): string
    {
        if (defined('LAYOUT')) {
            return 'layouts/' . LAYOUT . '.tpl';
        }

        if (MemoryLoader::models('User')->get()->isPersonnel()) {
            return 'layouts/default_matches.tpl';
        }

        if (in_array(MemoryLoader::getPage(), ['index.php', 'home.php', 'meditation.php'])) {
            return 'layouts/default_index.tpl';
        }

        return 'layouts/default.tpl';
    }

    /**
     * Clears the location path from evil input
     */
    private function clearLocation($location): string
    {
        while ((strpos($location, './') !== false) || (strpos($location, '../') !== false)) {
            $location = str_replace(array('./', '../'), array('', ''), $location);
        }

        return $location;
    }
}