<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services;

use Exception;
use MiniatureHappiness\CoreBundle\Common\File\DefaultMimetypes;
use MiniatureHappiness\LegacyBundle\Interfaces\OutputInterface;
use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * File uploader
 *
 * @deprecated
 */
class Upload extends AbstractService
{
    use DeprecationTrait;

    protected DefaultMimeTypes $defaultMimeTypes;
    protected FileData $fileData;

    public function __construct(DefaultMimeTypes $defaultMimeTypes, FileData $fileData)
    {
        $this->defaultMimeTypes = $defaultMimeTypes;
        $this->fileData = $fileData;
    }

    /**
     * Checks if the file is correct uploaded
     *
     * @deprecated
     */
    public function isUploaded(string $name): bool
    {
        $this->triggerDeprecationError();

        return (array_key_exists($name, $_FILES) && $_FILES[$name]['error'] === 0);
    }

    /**
     * Checks if the file is valid
     *
     * @example isValid('avatar', ['jpeg' => 'image/jpeg'], 10000)
     * @deprecated
     */
    public function isValid(string $name, array $extensions, int $maxSizeInBytes = -1): bool
    {
        $this->triggerDeprecationError();

        $mimetype = $this->fileData->getMimeType($_FILES[$name]['tmp_name']);

        $data = explode('/', $mimetype);
        $fileExtensions = explode('.', $_FILES[$name]['name']);
        $extension = strtolower(end($fileExtensions));

        if (!array_key_exists($extension, $extensions) ||
            (
                $extensions[$extension] !== $mimetype &&
                (is_array($extensions[$extension]) && !in_array($mimetype, $extensions[$extension], true))
            )
        ) {
            unlink($_FILES[$name]['tmp_name']);
            return false;
        }

        if ($maxSizeInBytes !== -1 && $_FILES[$name]['size'] > $maxSizeInBytes) {
            unlink($_FILES[$name]['tmp_name']);
            return false;
        }

        return true;
    }

    /**
     * Moves the uploaded file to the target directory
     * Does NOT overwrite files with the same name.
     *
     * @deprecated
     */
    public function moveFile(string $name, string $targetDir, string $targetName = ''): string
    {
        $this->triggerDeprecationError();

        if (empty($targetName)) {
            $targetName = $_FILES[$name]['name'];
        } else {
            $fileExtensions = explode('.', $_FILES[$name]['name']);
            $extension = '.' . strtolower(end($fileExtensions));

            $targetName .= $extension;
        }

        if (file_exists($targetDir . '/' . $targetName)) {
            $fileExtensions = explode('.', $_FILES[$name]['name']);
            $extension = '.' . strtolower(end($fileExtensions));

            $i = 1;
            $testname = $targetName;
            while (file_exists($targetDir . '/' . str_replace($extension, '__' . $i . $extension, $testname))) {
                $i++;
            }

            $targetName = str_replace($extension, '__' . $i . $extension, $testname);
        }

        move_uploaded_file($_FILES[$name]['tmp_name'], $targetDir . '/' . $targetName);

        if (!file_exists($targetDir . '/' . $targetName)) {
            return '';
        }

        return $targetName;
    }

    /**
     * @deprecated
     * @throws Exception
     */
    public function addHead(OutputInterface $output): void
    {
        $this->triggerDeprecationError();

        $output->append(
            'head',
            '<script src="/js/widgets/fileupload.js" type="text/javascript"></script>'
        );
    }

    public function getDefaultMimeTypes(): DefaultMimeTypes
    {
        $this->triggerDeprecationError();

        return $this->defaultMimeTypes;
    }

    public function getExtension($filename): string
    {
        $this->triggerDeprecationError();

        return pathinfo($filename, PATHINFO_EXTENSION);
    }
}
