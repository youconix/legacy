<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services;

use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * Class for securing user input
 *
 * @deprecated
 */
class Security extends AbstractService
{
    use DeprecationTrait;

    /**
     * @inheritDoc
     */
    public static function isSingleton(): bool
    {
        return true;
    }

    /**
     * Checks for correct boolean value
     *
     * @deprecated
     */
    public function secureBoolean($input): bool
    {
        $this->triggerDeprecationError();

        if ($input === false || $input === true) {
            return $input;
        }
        if ($input === 0 || $input === 1 || $input === '0' || $input === '1') {
            return (bool)$input;
        }

        return false;
    }

    /**
     * Checks for correct int value
     *
     * @deprecated
     */
    public function secureInt($input, bool $positive = false): int
    {
        $this->triggerDeprecationError();

        if (!is_numeric($input) || !preg_match("#^-?[0-9]+$#si", (string)$input)) {
            return 0;
        }

        if ($positive && $input < 0) {
            return 0;
        }

        return (int)$input;
    }

    /**
     * Checks for correct float value
     *
     * @deprecated
     */
    public function secureFloat($input, bool $positive = false): float
    {
        $this->triggerDeprecationError();

        if (!is_numeric($input) || !preg_match("#^-?[0-9]+(,|\.)?[0-9]*$#si", (string)$input)) {
            return 0.00;
        }

        if ($positive && $input < 0) {
            return 0.00;
        }

        return (float)$input;
    }

    /**
     * Disables code in the given string
     *
     * @deprecated
     */
    public function secureString(string $input): string
    {
        $this->triggerDeprecationError();

        $input = trim($input);
        $input = stripslashes($input);
        $input = htmlentities($input, ENT_QUOTES);

        /* Disable JavaScript */
        $input = preg_replace("#javascript:+[a-zA-Z0-9_]+\(+[a-zA-Z0-9_\-'\",:/\.]*\)+#si", "", $input);
        $input = str_replace("%3C", "&gt;", $input);
        $input = str_replace("%3E", "&gt;", $input);
        $input = preg_replace("#%3C/script%3E#si", "", $input);

        /* Disbable Javascript-events */
        $input = preg_replace(
            '#on+[a-zA-Z]+[[:space:]]*=+[[:space:]]*(\"| \')*[a-zA-Z0-9_\-\.\'\"]+(\"| \')*#si',
            "",
            $input
        );

        return $input;
    }

    /**
     * Disables code in the given string for DB input
     *
     * @deprecated
     */
    public function secureStringDB(string $input): string
    {
        $this->triggerDeprecationError();

        $input = $this->secureString($input);
        return str_replace(
            [
                '\r\n',
                '\r',
                '\n'
            ],
            [
                "\n",
                "\n",
                "\n"
            ],
            $input
        );
    }

    /**
     * Validates the given email address
     *
     * @deprecated
     */
    public function checkEmail(string $email): bool
    {
        $this->triggerDeprecationError();

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        return false;
    }

    /**
     * Validates the given URI
     *
     * @deprecated
     */
    public function checkURI(string $uri): bool
    {
        $this->triggerDeprecationError();

        if (preg_match("#^(ftp://|ftps://|file://)+#i", $uri)) {
            if (filter_var($uri, FILTER_VALIDATE_URL)) {
                return true;
            }

            return false;
        }

        if (str_starts_with($uri, 'http://') || str_starts_with($uri, 'https://')) {
            $domain = parse_url($uri, PHP_URL_HOST);

            if (preg_match("#^([a-z\d\-_\+]+\.)?([a-z\d\-_\+]+\.)([a-z\d\-_\+]+)$#i", $domain)) {
                return true;
            }

            return false;
        }

        if (preg_match("#^[a-z0-9\-\.]{2,255}\.+[a-z0-9\-]{2,63}#i", $uri)) {
            return true;
        }

        return false;
    }

    /**
     * Validates the given Dutch postal address
     *
     * @deprecated
     */
    public function checkPostalNL(string $value): bool
    {
        $this->triggerDeprecationError();

        if (trim($value) === "") {
            return false;
        }

        if (preg_match("/^\d{4}\s*[a-z]{2}$/i", $value)) {
            return true;
        }

        return false;
    }

    /**
     * Validates the given belgium postal address
     *
     * @deprecated
     */
    public function checkPostalBE($value): bool
    {
        $this->triggerDeprecationError();

        if (trim($value) === "") {
            return false;
        }
        if (!preg_match("/^\d{4}$/", $value)) {
            return false;
        }

        if ($value < 1000 || $value > 9999) {
            return false;
        }

        return true;
    }

    /**
     * @example secureInput('GET', ['username' => 'string|required'])
     *
     * @deprecated
     */
    public function secureInput(string $requestType, array $declared): array
    {
        $this->triggerDeprecationError();

        $source = null;
        if ($requestType === 'GET') {
            $source = $_GET;
        } elseif ($requestType === 'POST') {
            $source = $_POST;
        } elseif ($requestType === 'REQUEST') {
            $source = $_REQUEST;
        }

        if (is_null($source)) {
            return [];
        }

        return $this->secureArray($source, $declared);
    }

    /**
     * @deprecated
     */
    public function secureArray(array $values, array $declared): array
    {
        $this->triggerDeprecationError();

        if (!array_key_exists('command', $declared)) {
            $declared['command'] = 'string';
        }
        if (!array_key_exists('AJAX', $declared)) {
            $declared['AJAX'] = 'string';
        }

        $init = array_keys($declared);
        $keys = array_keys($values);
        $result = [];

        foreach ($keys as $key) {
            if (!in_array($key, $init)) {
                unset($values[$key]);
                continue;
            }

            switch ($declared[$key]) {
                case 'boolean':
                    $result[$key] = $this->secureBoolean($values[$key]);
                    unset($values[$key]);
                    break;

                case 'int':
                    $result[$key] = $this->secureInt($values[$key]);
                    unset($values[$key]);
                    break;

                case 'float':
                    $result[$key] = $this->secureFloat($values[$key]);
                    unset($values[$key]);
                    break;

                case 'ignore':
                    $result[$key] = $values[$key];
                    unset($values[$key]);
                    break;

                case 'ignore-keep':
                    $result[$key] = $values[$key];
                    break;

                case 'string':
                    $result[$key] = $this->secureString($values[$key]);
                    unset($values[$key]);
                    break;

                case 'string-JS':
                    $values[$key] = $this->prepareJsDecoding($values[$key]);
                    $result[$key] = $this->secureString($values[$key]);
                    $result[$key] = $this->fixDecodeBug($result[$key]);
                    unset($values[$key]);
                    break;

                case 'string-DB':
                    $result[$key] = $this->secureStringDB($values[$key]);
                    unset($values[$key]);
                    break;

                case 'string-DB-JS':
                    $values[$key] = $this->prepareJsDecoding($values[$key]);
                    $result[$key] = $this->secureStringDB($values[$key]);
                    $result[$key] = $this->fixDecodeBug($result[$key]);
                    unset($values[$key]);
                    break;

                case 'string-DB-array':
                    $result[$key] = $this->parseArray('string-DB-array', $values[$key]);
                    unset($values[$key]);
                    break;

                case 'string-DB-JS-array':
                    $result[$key] = $this->parseArray('string-DB-JS-array', $values[$key]);
                    unset($values[$key]);
                    break;

                case 'string-array':
                    $result[$key] = $this->parseArray('string-array', $values[$key]);
                    unset($values[$key]);
                    break;

                case 'string-JS-array':
                    $result[$key] = $this->parseArray('string-JS-array', $values[$key]);
                    unset($values[$key]);
                    break;

                case 'int-array':
                    $result[$key] = $this->parseArray('int-array', $values[$key]);
                    unset($values[$key]);
                    break;

                case 'float-array':
                    $result[$key] = $this->parseArray('float-array', $values[$key]);
                    unset($values[$key]);
                    break;

                default:
                    unset($values[$key]);
                    break;
            }
        }
        return $result;
    }

    /**
     * Parses a sub input array
     */
    private function parseArray(string $type, array $source): array
    {
        $result = array();
        $keys = array_keys($source);

        foreach ($keys as $key) {
            if (is_array($source[$key])) {
                $result[$key] = $this->parseArray($type, $source[$key]);
            } elseif ($type === 'string-DB-array') {
                $result[$key] = $this->secureStringDB($source[$key]);
            } elseif ($type === 'string-DB-JS-array') {
                $result[$key] = $this->prepareJsDecoding($source[$key]);
                $result[$key] = $this->secureStringDB($result[$key]);
                $result[$key] = $this->fixDecodeBug($result[$key]);
            } elseif ($type === 'string-array') {
                $result[$key] = $this->secureString($source[$key]);
            } elseif ($type === 'string-JS-array') {
                $result[$key] = $this->prepareJsDecoding($source[$key]);
                $result[$key] = $this->secureString($result[$key]);
                $result[$key] = $this->fixDecodeBug($result[$key]);
            } elseif ($type === 'int-array') {
                $result[$key] = $this->secureInt($source[$key]);
            } elseif ($type === 'float-array') {
                $result[$key] = $this->secureFloat($source[$key]);
            }
        }

        return $result;
    }

    /**
     * Prepares the decoding from AJAX requests
     *
     * @deprecated
     */
    public function prepareJsDecoding(string $value): string
    {
        $this->triggerDeprecationError();

        return str_replace(
            [
                '‘',
                '’'
            ],
            [
                '&lsquo;',
                '&rsquo;'
            ],
            urldecode($value)
        );
    }

    /**
     * Fixes the decodeUrl->htmlentities bug
     *
     * @deprecated
     */
    public function fixDecodeBug(string $text): string
    {
        $this->triggerDeprecationError();

        $text = preg_replace('/&acirc;{1}.+&not;{1}/', '&euro;', $text);

        return str_replace(
            [
                '&Atilde;&laquo;',
                '&Atilde;&curren;',
                '&Atilde;&yen;',
                '&Atilde;&sect;',
                '&Atilde;&uml;',
                '&Atilde;&copy;',
                '&Atilde;&macr;',
                '&Atilde;&sup2;',
                '&Atilde;&sup3;',
                '&Atilde;&para;',
                '&Acirc;&copy;',
                '&amp;rsquo;',
                '&amp;lsquo;'
            ],
            [
                '&euml;',
                '&auml;',
                '&aring;',
                '&ccedil;',
                '&egrave;',
                '&eacute;',
                '&iuml;',
                '&ograve;',
                '&oacute;',
                '&ouml;',
                '&copy;',
                '&rsquo;',
                '&lsquo;'
            ],
            $text
        );
    }
}