<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services;

use DOMDocument;
use DOMXPath;
use Exception;
use MiniatureHappiness\CoreBundle\Exceptions\IOException;
use MiniatureHappiness\CoreBundle\Exceptions\XmlException;

/**
 * Xml parser for handling XML files through Xpath
 *
 * @deprecated
 */
class Xml
{
    protected ?DOMDocument $domDocument = null;
    protected ?DOMXPath $document = null;
    protected ?string $startTag = null;

    /**
     * Creates a new XML-file
     *
     * @deprecated
     */
    public function create(string $rootElement): void
    {
        $this->domDocument = new DOMDocument('1.0', 'UTF-8');

        // We don't want to bother with white spaces
        $this->domDocument->preserveWhiteSpace = false;
        $this->domDocument->resolveExternals = true; // for character entities
        $this->domDocument->formatOutput = true;   // keep output alignment

        $this->domDocument->appendChild(
            $this->domDocument->createElement($rootElement)
        );

        $this->document = new DOMXPath($this->domDocument);
    }

    /**
     * Loads the requested XML-file
     *
     * @throws IOException when the file does not exist
     * @deprecated
     */
    public function load(string $filePath): void
    {
        $this->domDocument = new DOMDocument('1.0', 'UTF-8');

        // We don't want to bother with white spaces
        $this->domDocument->preserveWhiteSpace = false;

        $this->domDocument->resolveExternals = true; // for character entities

        $this->domDocument->formatOutput = true;   // keep output alignment

        if (!$this->domDocument->Load($filePath)) {
            throw new IOException("Can not load XML-file " . $filePath . ". Check the address");
        }

        $this->document = new DOMXPath($this->domDocument);
    }

    /**
     * @deprecated
     */
    public function setStartTag(string $startTag): void
    {
        $this->startTag = $startTag;
    }

    /**
     * Gives the asked part of the loaded file
     *
     * @throws  XMLException when the path does not exist
     * @deprecated
     */
    public function get(string $path): string
    {
        $path = $this->getRealPath($path);

        $query = $this->document->query("//" . $path);

        if ($query->length < 1) {
            /* Part not found */
            throw new XMLException("Can not find " . $path);
        }

        foreach ($query as $entry) {
            $result = $entry->textContent;
        }

        return $result;
    }

    /**
     * Gives the asked part of the loaded file
     *
     * @throws  XMLException when the path does not exist
     * @deprecated
     */
    public function getRecursive(string $path): array
    {
        $path = $this->getRealPath($path);

        $query = $this->document->query("//" . $path);

        if ($query->length < 1) {
            /* Part not found */
            throw new XMLException("Can not find " . $path);
        }

        $result = [];
        foreach ($query as $entry) {
            if ($entry->hasChildNodes()) {
                foreach ($entry->childNodes as $child) {
                    $result = array_merge($result, $this->getRecursive(ltrim($child->getNodePath(), '/')));
                }
            } else {
                $key = substr($entry->getNodePath(), 10, -7); // Remove /language/ and /text()
                $key = preg_replace('/\[\d+\]/', '', $key); // Remove [1], [2], etc..
                $result[$key] = $entry->textContent;
            }
        }

        return $result;
    }

    /**
     * Saves the value at the given place
     *
     * @throws XMLException when the path does not exist
     * @deprecated
     */
    public function set(string $path, string $content): void
    {
        $path = $this->getRealPath($path);

        $data = explode('/', $path);
        $name = end($data);
        $query = $this->document->query("//" . $path);

        if ($query->length < 1) {
            /* Part not found */
            throw new XMLException("Can not find " . $path);
        }
        $oldnode = $query->item(0);

        $newNode = $this->domDocument->createElement($name);
        $newNode->appendChild($this->domDocument->createCDataSection($content));
        $oldnode->parentNode->replaceChild($newNode, $oldnode);
    }

    /**
     * Saves a new node at the given place
     *
     * @throws  XMLException when the path (up till the parent) does not exist
     * @deprecated
     */
    public function add(string $path, ?string $content): void
    {
        $path = $this->getRealPath($path);

        // Check existence
        $query = $this->document->query("//" . $path);
        if ($query->length > 0) {
            throw new XMLException("Path already exists " . $path);
        }

        // Check parent existence
        $data = explode('/', $path);
        $name = array_pop($data);
        $parentPath = implode('/', $data);
        $query = $this->document->query("//" . $parentPath);

        if ($query->length < 1) {
            throw new XMLException("Can not find " . $parentPath);
        }
        $parentNode = $query->item(0);

        // Create new node
        $newNode = $this->domDocument->createElement($name);
        if ($content !== null) {
            $newNode->appendChild($this->domDocument->createCDataSection($content));
        }
        $parentNode->appendChild($newNode);
    }

    /**
     * Delete a node at the given place
     *
     * @throws  XMLException when the path does not exist
     * @deprecated
     */
    public function delete(string $path): void
    {
        $path = $this->getRealPath($path);

        $query = $this->document->query("//" . $path);

        if ($query->length < 1) {
            /* Part not found */
            throw new XMLException("Can not find " . $path);
        }
        $delNode = $query->item(0);

        $delNode->parentNode->removeChild($delNode);
    }

    /**
     * Saves the XML file loaded to the given file
     *
     * @throws    Exception    If the directory is not writable
     * @deprecated
     */
    public function save(string $file): void
    {
        $dir = dirname($file);

        if (!is_writable($dir)) {
            throw new Exception("Can not write directory " . $dir . '.');
        }

        $this->domDocument->save($file);
    }

    /**
     * Checks of the given part of the loaded file exists
     *
     * @deprecated
     */
    public function exists(string $path): bool
    {
        $path = $this->getRealPath($path);

        $query = $this->document->query("//" . $path);

        if ($query->length < 1) {
            /* Part not found */
            return false;
        }

        return true;
    }

    /**
     * Replaces the given keys in the given text with the given values
     *
     * @throws Exception when the path does not exist
     * @deprecated
     */
    public function insert(string $path, string|array $keys, string|array $values): string
    {
        $text = $this->get($path);

        if (is_array($keys)) {
            foreach ($keys as $id => $key) {
                $text = str_replace('[' . $key . ']', $values[$id], $text);
            }
        } else {
            $text = str_replace('[' . $keys . ']', $values, $text);
        }

        return $text;
    }

    /**
     * Checks the path and adds the default start tag
     *
     * @throws XMLException If the path is invalid
     */
    protected function getRealPath(string $path): string
    {
        if (substr($path, -1) === '/') {
            throw new XMLException('Invalid XML query : ' . $path);
        }

        if (empty($this->startTag)) {
            return $path;
        }

        if (strpos($path, $this->startTag) !== 0) {
            $path = $this->startTag . '/' . $path;
        }

        return $path;
    }
}