<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services;

use Exception;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RequestStack;
use MiniatureHappiness\LegacyBundle\Interfaces\CookieInterface;
use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * Service class for handling and manipulating cookies
 *
 * @copyright Youconix
 * @author Rachelle Scheijen
 * @version 1.0
 * @since 1.0
 * @deprecated
 */
class Cookie extends AbstractService implements CookieInterface
{
    use DeprecationTrait;

    private ?ParameterBag $cookies;

    public function __construct(RequestStack $requestStack)
    {
        $this->cookies = $requestStack->getMainRequest()->cookies;
    }

    public function __destruct()
    {
        $this->cookies = null;
    }

    /**
     * Returns if the object should be treated as singleton
     * @deprecated
     */
    public static function isSingleton(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     * @deprecated
     */
    public function delete(string $cookieName, string $domain): void
    {
        $this->triggerDeprecationError('Symfony Request -> cookies -> remove');

        if (!$this->cookies->has($cookieName)) {
            throw new Exception("Cookie " . $cookieName . " does not exist.");
        }

        $this->cookies->remove($cookieName);
    }

    /**
     * @inheritDoc
     */
    public function set(string $cookieName, string $cookieData, string $domain, string $url = "", int $secure = 0): static
    {
        $this->triggerDeprecationError('Symfony Request -> cookies -> set');

        $this->cookies->set($cookieName, $cookieData);

        return $this;
    }

    /**
     * @inheritDoc
     * @deprecated
     */
    public function get(string $cookieName): string
    {
        $this->triggerDeprecationError('Symfony Request -> cookies -> get');

        if (!$this->cookies->has($cookieName))
        {
            throw new Exception("Cookie " . $cookieName . " does not exist.");
        }

        return $this->cookies->get($cookieName);
    }

    /**
     * @inheritDoc
     * @deprecated
     */
    public function exists(string $cookieName): bool
    {
        $this->triggerDeprecationError('Symfony Request -> cookies -> has');

        return $this->cookies->has($cookieName);
    }
}