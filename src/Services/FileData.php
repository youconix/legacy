<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services;

use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * File-data handler for collecting file specific data
 *
 * This file is part of Miniature-happiness
 *
 * @copyright Youconix
 * @author Rachelle Scheijen
 * @version 1.0
 * @since 1.0
 * @deprecated
 */
class FileData extends AbstractService
{
    use DeprecationTrait;

    /**
     * @inheritDoc
     */
    public static function isSingleton(): bool
    {
        return true;
    }

    /**
     * Returns the mine-type from the given file.
     * Needs mime_content_type() of finfo_open() on the server to work.
     * @deprecated
     */
    public function getMimeType(string $file, bool $removeDetails = true): string
    {
        $this->triggerDeprecationError('Symfony mime');

        if (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimeType = finfo_file($finfo, $file);
            finfo_close($finfo);
        } elseif (function_exists('mime_content_type')) {
            $mimeType = mime_content_type($file);
        } else {
            $mimeType = "unknown/unknown";
        }

        if ($removeDetails && ($pos = strpos($mimeType, ';')) !== false) {
            $mimeType = substr($mimeType, 0, $pos);
        }

        return $mimeType;
    }

    /**
     * Return the size from the given file.
     * Needs file_size() or stat() on the server to work.
     *
     * returns -1 if the size could not be collected
     * @deprecated
     */
    public function getFileSize(string $file): int
    {
        $this->triggerDeprecationError('Symfony File');

        if (function_exists('file_size')) {
            return file_size($file);
        }
        if (function_exists('stat')) {
            $stat = stat($file);

            return (int)$stat[7];
        }
        return -1;
    }

    /**
     * Returns the last date that the given file was accessed.
     * Needs stat() on the server to work.
     *
     * Returns -1 if the date could not be collected
     * @deprecated
     */
    public function getLastAccess(string $file): int
    {
        $this->triggerDeprecationError('Symfony File');

        if (function_exists('stat')) {
            $stats = stat($file);

            return (int)$stats[8];
        }

        return -1;
    }

    /**
     * Returns the last data that the given file was modified.
     * Needs stat() on the server to work.
     *
     * Return -1 if the date could not be collected
     * @deprecated
     */
    public function getLastModified(string $file): int
    {
        $this->triggerDeprecationError('Symfony File');

        if (function_exists('stat')) {
            $stats = stat($file);

            return (int)$stats[9];
        }

        return -1;
    }
}