<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services;

use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;
use MiniatureHappiness\CoreBundle\Exceptions\XMLException;
use MiniatureHappiness\LegacyBundle\Interfaces\LanguageInterface;
use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * Language parser for parsing the old XML language files
 *
 * @deprecated
 */
class Language implements LanguageInterface
{
    use DeprecationTrait;

    protected string $encoding;
    private string $language;
    private TranslatorInterface $translator;

    public function __construct(RequestStack $requestStack, TranslatorInterface $translator)
    {
        $this->language = $requestStack->getMainRequest()->getLocale();
        $this->encoding = ini_get('default_charset');

        $this->translator = $translator;
    }

    /**
     * @inheritDoc
     */
    public function getLanguages(): array
    {
        $this->triggerDeprecationError();

        return [];
    }

    /**
     * @inheritDoc
     */
    public function setLanguage(string $language): void
    {
        $this->triggerDeprecationError('Use Symfony language system');
    }

    /**
     * @inheritDoc
     */
    public function getLanguage(): string
    {
        $this->triggerDeprecationError();

        return $this->language;
    }

    /**
     * @inheritDoc
     */
    public function ensure(string $newLang): void
    {
        $this->triggerDeprecationError();
    }

    /**
     * @inheritDoc
     */
    public function endEnsure(): void
    {
        $this->triggerDeprecationError();
    }

    /**
     * @inheritDoc
     */
    public function getEncoding(): string
    {
        $this->triggerDeprecationError();

        return $this->encoding;
    }

    /**
     * @inheritDoc
     */
    public function getEncoded(string $path, array $variables = null): string
    {
        $this->triggerDeprecationError();

        $text = trim($this->get($path));

        if (is_array($variables)) {
            $text = $this->insert($text, array_keys($variables), array_values($variables));
        }

        return $text;
    }

    /**
     * @inheritDoc
     */
    public function getDecoded(string $path, array $variables = null): string
    {
        $this->triggerDeprecationError();

        return $this->get($path, $variables);
    }

    /**
     * @inheritDoc
     * @throws XMLException when the path is not part of the standard texts of the framework
     */
    public function get(string $path, array $variables = null): string
    {
        $this->triggerDeprecationError();

        if ($variables === null)
        {
            $variables = [];
        }

        $id = sprintf(
            'youconix.core.%s',
            str_replace('/', '.', $path)
        );

        $text = $this->translator->trans($id, $variables, 'youconix_core');

        if ($text === $id)
        {
            throw new XMLException(sprintf('Path %s is not a default framework text. Use Symfony translation for your own translations.', $path));
        }

        return html_entity_decode(trim($text), ENT_QUOTES | ENT_XML1, 'UTF-8');
    }

    /**
     * @inheritDoc
     */
    public function getEnsured(string $language, string $path, array $variables = null): string
    {
        $this->triggerDeprecationError();

        $this->ensure($language);

        $s_text = $this->get($path, $variables);

        $this->endEnsure();

        return $s_text;
    }

    /**
     * @inheritDoc
     */
    public function insert(string $text, array $fields, $values): string
    {
        $this->triggerDeprecationError();

        $amount = count($fields);
        for ($i = 0; $i < $amount; $i++) {
            $text = str_replace('%' . $fields[$i] . '%', $values[$i], $text);
        }

        return $text;
    }

    /**
     * @inheritDoc
     */
    public function getSub(string $sub, string $path, string $lang = ''): string
    {
        $this->triggerDeprecationError();

        return trim($this->sub($sub, $lang)->get($path));
    }

    /**
     * @inheritDoc
     */
    public function sub(string $sub, string $lang = '')
    {
        $this->triggerDeprecationError();
    }

    /**
     * @inheritDoc
     */
    public function date(string $format, int|DateTime $datetime): string
    {
        $this->triggerDeprecationError();

        if ($datetime instanceof DateTime) {
            $date = $datetime->format($format);
        } elseif (is_numeric($datetime)) {
            $date = date($format, $datetime);
        } else {
            throw new Exception("Invalid date.");
        }

        if (!isset($this->dateReplacement[$this->language])) {
            $this->dateReplacement[$this->language] = [
                'Monday' => $this->get('days/day2'),
                'Tuesday' => $this->get('days/day3'),
                'Wednesday' => $this->get('days/day4'),
                'Thursday' => $this->get('days/day5'),
                'Friday' => $this->get('days/day6'),
                'Saturday' => $this->get('days/day7'),
                'Sunday' => $this->get('days/day1'),

                'Mon' => $this->get('daysShort/day2'),
                'Tue' => $this->get('daysShort/day3'),
                'Wed' => $this->get('daysShort/day4'),
                'Thu' => $this->get('daysShort/day5'),
                'Fri' => $this->get('daysShort/day6'),
                'Sat' => $this->get('daysShort/day7'),
                'Sun' => $this->get('daysShort/day1'),

                'January' => $this->get('months/month1'),
                'February' => $this->get('months/month2'),
                'March' => $this->get('months/month3'),
                'April' => $this->get('months/month4'),
                'May' => $this->get('months/month5'),
                'June' => $this->get('months/month6'),
                'July' => $this->get('months/month7'),
                'August' => $this->get('months/month8'),
                'September' => $this->get('months/month9'),
                'October' => $this->get('months/month10'),
                'November' => $this->get('months/month11'),
                'December' => $this->get('months/month12'),

                'Jan' => $this->get('monthsShort/month1'),
                'Feb' => $this->get('monthsShort/month2'),
                'Mar' => $this->get('monthsShort/month3'),
                'Apr' => $this->get('monthsShort/month4'),
                'May' => $this->get('monthsShort/month5'),
                'Jun' => $this->get('monthsShort/month6'),
                'Jul' => $this->get('monthsShort/month7'),
                'Aug' => $this->get('monthsShort/month8'),
                'Sep' => $this->get('monthsShort/month9'),
                'Oct' => $this->get('monthsShort/month10'),
                'Nov' => $this->get('monthsShort/month11'),
                'Dec' => $this->get('monthsShort/month12'),
            ];
        }

        // Translate result
        $date = str_replace(
            array_keys($this->dateReplacement[$this->language]),
            array_values($this->dateReplacement[$this->language]),
            $date
        );

        // Return
        return strtolower($date);
    }

    /**
     * @inheritDoc
     */
    public function insertPath(string $path, string|array $keys, string|array $variables = null): string
    {
        if (!is_array($keys)) {
            $keys = [$keys];
        }

        if (!is_array($variables)) {
            $variables = [$variables];
        }

        array_walk($keys, static function (&$key): string {
            $key = '%' . $key . '%';
        });

        $text = $this->get($path);

        return str_replace($keys, $variables, $text);
    }
}