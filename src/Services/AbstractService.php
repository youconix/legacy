<?php

namespace MiniatureHappiness\LegacyBundle\Services;

use MiniatureHappiness\LegacyBundle\AbstractObject;
use MiniatureHappiness\LegacyBundle\Interfaces\DeprecatedInterface;

/**
 * @deprecated
 */
abstract class AbstractService extends AbstractObject implements DeprecatedInterface
{
    /**
     * @deprecated
     */
    public function cloneService(): AbstractService
    {
        return clone $this;
    }
}