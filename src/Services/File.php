<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services;

use MiniatureHappiness\CoreBundle\Exceptions\IOException;
use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * File handler
 *
 * @deprecated
 */
class File extends AbstractService
{
    use DeprecationTrait;

    /**
     * Returns if the object should be treated as singleton
     */
    public static function isSingleton(): bool
    {
        return true;
    }

    /**
     * Generates a new file with the given rights
     *
     * @throws IOException when the given directory is not writable
     * @deprecated
     */
    public function newFile(string $fileName, $filePermissions, bool $binary = false): void
    {
        $this->triggerDeprecationError('Symfony Finder');

        $dir = dirname($fileName);

        if (!is_writable($dir)) {
            throw new IOException('Can not make file ' . $fileName . ' in directory ' . $dir . '. Check the permissions');
        }

        ($binary) ? $mode = 'wb+' : $mode = 'w+';
        $file = fopen($fileName, $mode);
        fwrite($file, "");
        fclose($file);

        $this->rights($fileName, $filePermissions);
    }

    /**
     * Reads the content from the given file
     *
     * @throws IOException when the file does not exist or is not readable
     * @deprecated
     */
    public function readFile(string $fileName, bool $binary = false): string
    {
        $this->triggerDeprecationError('Symfony Finder');

        if (preg_match("#^(http://|ftp://)#si", $fileName)) {
            return $this->readExtenalFile($fileName, $binary);
        }

        if (!$this->exists($fileName)) {
            throw new IOException('File ' . $fileName . ' does not exist!');
        }

        /* Check file */
        if (!is_readable($fileName)) {
            throw new IOException('Can not read ' . $fileName . '. Check the permissions.');
        }

        filesize($fileName) != 0 ? $size = filesize($fileName) : $size = 1;
        ($binary) ? $mode = 'rb' : $mode = 'r';
        $file = fopen($fileName, $mode);
        $content = fread($file, $size);
        fclose($file);

        return $content;
    }

    /**
     * Reads the content from the given file on a different server
     */
    protected function readExtenalFile(string $s_file, bool $bo_binary = false): string
    {
        $this->triggerDeprecationError('Symfony Finder');

        $i_size = 50000;
        ($bo_binary) ? $s_mode = 'rb' : $s_mode = 'r';
        $s_file = @fopen($s_file, $s_mode);
        $s_content = @fread($s_file, $i_size);
        @fclose($s_file);

        return $s_content;
    }

    /**
     * Overwrites the given file or generates if it does not exist
     *
     * @throws IOException When the file is not readable or writable
     * @deprecated
     */
    public function writeFile(
        string $fileName,
        string $content,
        $filePermissions = 0644,
        bool $binary = false
    ): void {
        $this->triggerDeprecationError('Symfony Finder');

        $fileExists = $this->mustBeWritable($fileName);

        ($binary) ? $s_mode = 'wb+' : $s_mode = 'w+';
        $file = fopen($fileName, $s_mode);
        fwrite($file, $content);
        fclose($file);

        if (!$fileExists) {
            $this->rights($fileName, $filePermissions);
        }
    }

    /**
     * Writes at the end of the given file or generates if it does not exist
     *
     * @throws IOException When the file is not readable or writable
     */
    public function writeLastFile(
        string $fileName,
        string $content,
        $filePermissions = 0644,
        bool $binary = false
    ): void {
        $this->triggerDeprecationError('Symfony Finder');

        $fileExists = $this->mustBeWritable($fileName);

        ($binary) ? $mode = 'ab' : $mode = 'a';
        $file = fopen($fileName, $mode);
        fwrite($file, $content);
        fclose($file);

        if (!$fileExists) {
            $this->rights($fileName, $filePermissions);
        }
    }

    /**
     * Writes at the start of the given file or generates if it does not exist
     *
     * @throws IOException When the file is not readable or writable
     * @deprecated
     */
    public function writeFirstFile(
        string $fileName,
        string $content,
        $filePermissions = 0644,
        bool $binary = false
    ): void {
        $this->triggerDeprecationError('Symfony Finder');

        $fileExists = $this->mustBeWritable($fileName);

        $contentOld = file_get_contents($fileName);

        ($binary) ? $mode = 'wb' : $mode = 'w';
        $file = fopen($fileName, $mode);
        fwrite($file, $content);
        fwrite($file, $contentOld);
        fclose($file);

        if (!$fileExists) {
            $this->rights($fileName, $filePermissions);
        }
    }

    /**
     * Renames the given file
     *
     * @throws IOException when the file does not exist or is not writable (needed for renaming)
     * @deprecated
     */
    public function renameFile(string $oldName, string $newName): void
    {
        $this->triggerDeprecationError('Symfony Finder');

        /* Check file */
        if (!$this->exists($oldName)) {
            throw new IOException('File ' . $oldName . ' does not exist.');
        }

        if (!rename($oldName, $newName)) {
            throw new IOException('File ' . $oldName . ' can not be renamed.');
        }
    }

    /**
     * Copy's the given file to the given directory
     *
     * @throws IOException when the file is not readable or the target directory is not writable
     * @deprecated
     */
    public function copyFile(string $fileName, string $targetDirectory): void
    {
        $this->triggerDeprecationError('Symfony Finder');

        /* Check file */
        if (!$this->exists($fileName)) {
            throw new IOException('Can not read file ' . $fileName . '.');
        }

        /* Check target */
        if (!is_writable($targetDirectory)) {
            throw new IOException('Can not write in directory ' . $targetDirectory . '.');
        }

        $parts = explode('/', $fileName);
        $newFilename = end($parts);

        copy($fileName, $targetDirectory . '/' . $newFilename);
    }

    /**
     * Moves the given file to the given directory
     *
     * @throws IOException when the target directory is not writable (needed for moving)
     * @deprecated
     */
    public function moveFile(string $file, string $targetDirectory): void
    {
        $this->triggerDeprecationError('Symfony Finder');

        /* Check file and target-directory */
        if (!$this->exists($file)) {
            throw new IOException('File ' . $file . ' does not exist');
        }

        if (!is_writable($file)) {
            throw new IOException('File ' . $file . ' is not writable, needed for deleting.');
        }

        if (!is_writable($targetDirectory)) {
            throw new IOException('Directory ' . $targetDirectory . ' is not writable');
        }

        /* Copy old file */
        $this->copyFile($file, $targetDirectory);

        /* Delete old file */
        $this->deleteFile($file);
    }

    /**
     * Deletes the given file
     *
     * @throws IOException when the file does not exist or is not writable
     * @deprecated
     */
    public function deleteFile(string $fileName): void
    {
        $this->triggerDeprecationError('Symfony Finder');

        /* Check file */
        if (!$this->exists($fileName)) {
            throw new IOException('File ' . $fileName . ' does not exist.');
        }

        if (!is_writable($fileName)) {
            throw new IOException('File ' . $fileName . ' is not writable.');
        }

        unlink($fileName);
    }

    /**
     * Reads the given directory
     *
     * @throws IOException
     * @deprecated
     */
    public function readDirectory(string $directory, bool $recursive = false): array
    {
        $this->triggerDeprecationError('Symfony Finder');

        /* Check Directory */
        if (!is_readable($directory)) {
            throw new IOException('Can not read directory ' . $directory . '.');
        }

        /* read directory */
        $handle = opendir($directory);
        if ($handle === false) {
            throw new IOException('Can not open directory ' . $directory . '.');
        }

        $files = [];

        /* read all files */
        while (false !== ($file = readdir($handle))) {
            if (($file == '.') || ($file == '..') || (substr($file, -1) == "~")) {
                continue;
            }

            /* check of $file is a directory */
            if (is_dir($directory . '/' . $file) && $recursive) {
                /* read subdirectory */
                $files = array_merge($files, $this->readDirectory($directory . '/' . $file, $recursive));
            } else {
                if ($recursive) {
                    $files[] = $directory . '/' . $file;
                } else {
                    $files[] = $file;
                }
            }
        }

        /* close directory */
        closedir($handle);

        /* Sort arrays and merge arrays */
        sort($files);

        return $files;
    }

    /**
     * Generates a new directory with the given name and rights
     *
     * @throws IOException when the target directory is not writable
     * @deprecated
     */
    public function newDirectory(string $name, $filePermissions = 0755): void
    {
        $this->triggerDeprecationError('Symfony Finder');

        $dir = $name;
        if (substr($dir, -1) == '/') {
            $dir = substr($dir, 0, -1);
        }
        $pos = strrpos($dir, '/');
        if ($pos === false) {
            throw new IOException("Invalid directory " . $name . ".");
        }

        $dir = substr($dir, 0, $pos);

        if (!is_writable($dir)) {
            throw new IOException('Directory ' . $name . ' is not writable.');
        }

        if (!is_dir($name)) {
            mkdir($name, $filePermissions);
        }

        $this->rights($name, $filePermissions);
    }

    /**
     * Renames the given Directory
     *
     * @throws IOException if the directory is not writable
     * @deprecated
     */
    public function renameDirectory(string $directory, string $nameNew): void
    {
        $this->triggerDeprecationError('Symfony Finder');

        /* Check or the directory exists */
        if (!$this->exists($directory)) {
            throw new IOException('Directory ' . $directory . ' does not exist.');
        }

        $target = dirname($directory);
        if (!rename($directory, $target . DIRECTORY_SEPARATOR . $nameNew)) {
            throw new IOException('Can not write new position for directory ' . $directory . '.');
        }
    }

    /**
     * Moves a directory to the given address
     *
     * @throws IOException
     * @deprecated
     */
    public function moveDirectory(string $directory, string $target): bool
    {
        $this->triggerDeprecationError('Symfony Finder');

        $name = substr($directory, strrpos($directory, '/') + 1);

        return rename($directory,  $target . '/' . $name);
    }

    /**
     * Copy's a directory to a new location
     *
     * @throws IOException if the copy fails
     * @deprecated
     */
    public function copyDirectory(string $directory, string $target): void
    {
        $this->triggerDeprecationError('Symfony Finder');

        /* Check or new map exists */
        if (!$this->exists($target)) {
            $this->newDirectory($target);
        }

        if (!is_writable($target)) {
            throw new IOException('Can not write in directory ' . $target . '.');
        }

        /* Open old Directory */
        if (!$handle = opendir($directory)) {
            throw new IOException('Can not open directory ' . $directory . '.');
        }

        /* Read old map */
        while (false !== ($file = readdir($handle))) {
            if (($file == '.') || ($file == '..') || (substr($file, -1) == "~")) {
                continue;
            }

            /* check or $file is a map */
            if (!is_dir($file)) {
                copy($directory . '/' . $file, $target . '/' . $file);
            } else {
                /* Copy subdirectory */
                $this->copyDirectory($directory . '/' . $file, $target . '/' . $file);
            }
        }
    }

    /**
     * Deletes the given Directory
     *
     * @throws IOException When the directory is not writable
     * @deprecated
     */
    public function deleteDirectory(string $directory): bool
    {
        $this->triggerDeprecationError('Symfony Finder');

        /* Delete all present files and maps */
        if (!$handle = opendir($directory)) {
            throw new IOException('Can\'t read directory ' . $directory . '.');
        }

        while (false !== ($file = readdir($handle))) {
            if (($file != '.') && ($file != '..')) {
                /* check or $file is a map */
                if (is_dir($directory . '/' . $file)) {
                    $this->deleteDirectory($directory . '/' . $file);
                } else {
                    $this->deleteFile($directory . '/' . $file);
                }
            }
        }

        if (!rmdir($directory)) {
            throw new IOException('Can\'t delete ' . $directory . '.');
        }

        return true;
    }

    /**
     * Checks if the given file or directory exists
     *
     * @deprecated
     */
    public function exists(string $fileName): bool
    {
        $this->triggerDeprecationError('Symfony Finder');

        return file_exists($fileName);
    }

    /**
     * Sets the rights from a file or directory.
     * The rights must be in hexadecimal form (0644)
     *
     * @deprecated
     */
    public function rights(string $fileName, $filePermissions): bool
    {
        $this->triggerDeprecationError('Symfony Finder');

        if (function_exists('chmod')) {
            chmod($fileName, $filePermissions);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @throws IOException
     */
    private function mustBeWritable(string $fileName): bool
    {
        if ($this->exists($fileName)) {
            if (!is_readable($fileName)) {
                throw new IOException('Can not open file ' . $fileName . '. Check the permissions');
            }

            if (!is_writable($fileName)) {
                throw new IOException('Can not write file ' . $fileName . '. Check the permissions');
            }

            return true;
        }

        if (!is_writable(dirname($fileName))) {
            throw new IOException('Can not write file ' . $fileName . '. Check the permissions');
        }

        return false;
    }
}