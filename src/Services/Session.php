<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface as SymfonySessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use MiniatureHappiness\CoreBundle\Auth\LoginManager;
use MiniatureHappiness\CoreBundle\Exceptions\AuthenticationException;
use MiniatureHappiness\CoreBundle\Exceptions\IOException;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;
use MiniatureHappiness\LegacyBundle\Interfaces\SessionInterface;
use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * Session management
 *
 * @deprecated
 */
class Session implements SessionInterface
{
    use DeprecationTrait;

    private SymfonySessionInterface $session;
    private LoginManager $loginManager;
    private TokenStorageInterface $tokenStorage;
    private Request $request;

    public function __construct(RequestStack $requestStack, LoginManager $loginManager, TokenStorageInterface $tokenStorage)
    {
        $this->session = $requestStack->getSession();
        $this->loginManager = $loginManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @inheritDoc
     */
    public function set(string $sessionName, string $sessionData): self
    {
        $this->triggerDeprecationError('Symfony session -> set');

        $this->session->set($sessionName, $sessionData);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function delete(string $sessionName): self
    {
        $this->triggerDeprecationError('Symfony session -> remove');

        if (!$this->session->has($sessionName)) {
            throw new IOException('Session ' . $sessionName . ' does not exist');
        }

        $this->session->remove($sessionName);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function get(string $sessionName): string
    {
        $this->triggerDeprecationError('Symfony session -> get');

        if (!$this->session->has($sessionName)) {
            throw new IOException('Session ' . $sessionName . ' does not exist');
        }

        return $this->session->get($sessionName);
    }

    /**
     * @inheritDoc
     */
    public function exists(string $sessionName): bool
    {
        $this->triggerDeprecationError('Symfony session -> has');

        return $this->session->has($sessionName);
    }

    /**
     * @inheritDoc
     */
    public function destroy(): self
    {
        $this->triggerDeprecationError();

        $this->session->clear();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function renew(string $sessionName): self
    {
        $this->triggerDeprecationError();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setLogin(int $userId, int $lastLogin, bool $autoLogin = false): self
    {
        $this->triggerDeprecationError('Use LoginManager -> login');

        $this->loginManager->login(
            $this->loginManager->getUserById($userId),
            $lastLogin,
            $autoLogin
        );

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function destroyLogin(): self
    {
        $this->triggerDeprecationError('Use LoginManager -> logout');

        $this->loginManager->logout();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function checkLogin(): self
    {
        $this->triggerDeprecationError('Use Symfony login system');

        $user = $this->getUser();

        if ($user === null) {
            if (!($user = $this->loginManager->checkAutoLogin())) {
                throw new AuthenticationException();
            }
        }

        $this->loginManager->validateFingerprint($user);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function redirectToLogin($route = 'home_login'): void
    {
        $this->triggerDeprecationError('Use Symfony login system');

        throw new AuthenticationException();
    }

    /**
     * @inheritDoc
     */
    public function check2fa(bool $redirect = true): void
    {
        $this->triggerDeprecationError('LoginManager -> check2fa');

        $this->loginManager->check2fa($this->getUser(), $redirect);
    }

    /**
     * @inheritDoc
     */
    public function set2fa(): self
    {
        $this->triggerDeprecationError('LoginManager -> set2fa');

        $this->loginManager->set2fa();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setAutoLogin(): self
    {
        $this->triggerDeprecationError('LoginManager -> setAutoLogin');

        $this->loginManager->setAutoLogin();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function checkAutoLogin(): bool
    {
        $this->triggerDeprecationError('LoginManager -> checkAutoLogin');

        return $this->loginManager->checkAutoLogin() !== null;
    }

    /**
     * @inheritDoc
     */
    public function unsetAutoLogin(): self
    {
        $this->triggerDeprecationError('LoginManager -> unsetAutoLogin');

        $this->loginManager->unsetAutoLogin();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getFingerprint(bool $bindToIp = false): string
    {
        $this->triggerDeprecationError('Use LoginManager -> getFingerprint');

        return $this->loginManager->getFingerprint($bindToIp);
    }

    private function getUser(): ?UserInterface
    {
        return $this->tokenStorage->getToken()?->getUser();
    }
}
