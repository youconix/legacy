<?php

namespace MiniatureHappiness\LegacyBundle;

/**
 * @deprecated
 */
abstract class AbstractObject
{
    /**
     * Returns if the object should be treated as singleton
     * @deprecated
     */
    public static function isSingleton(): bool
    {
        return false;
    }
}