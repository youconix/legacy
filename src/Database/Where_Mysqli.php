<?php

namespace MiniatureHappiness\LegacyBundle\Database;

use MiniatureHappiness\LegacyBundle\Exceptions\DBException;
use MiniatureHappiness\LegacyBundle\Interfaces\BuilderInterface;
use MiniatureHappiness\LegacyBundle\Interfaces\WhereInterface;

/**
 * @deprecated
 */
class Where_Mysqli extends QueryConditions_Mysqli implements WhereInterface
{
    protected $builder;

    /**
     * @inheritDoc
     */
    public function reset(): self
    {
        parent::reset();
        $this->builder = null;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function startSubWhere(): self
    {
        $this->query .= '(';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function endSubWhere(): self
    {
        $this->query .= ')';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addSubQuery(BuilderInterface $builder, string $field, string $key, string $command): self
    {
        if (!array_key_exists($key, $this->keys)) {
            throw new DBException('Unknown where key ' . $key . '.');
        }

        $command = strtoupper($command);
        if (!in_array($command, [
            'OR',
            'AND'
        ])) {
            throw new DBException('Unknown where command ' . $command . '.  Only AND & OR are supported.');
        }

        $this->builder = [
            'object' => $builder,
            'field' => $field,
            'key' => $key,
            'command' => $command
        ];

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function render(): ?array
    {
        if (empty($this->query)) {
            return null;
        }

        if (!is_null($this->builder)) {
            $obj_builder = $this->builder['object']->render();
            $this->query .= $this->builder['command'] . ' ' . $this->builder['field'] . ' ' . $this->builder['key'] . ' (' . $obj_builder['query'] . ')';
            $this->values[] = $obj_builder['values'];
            $this->types[] = $obj_builder['types'];
        }

        return [
            'where' => ' WHERE ' . $this->query,
            'values' => $this->values,
            'types' => $this->types
        ];
    }
}