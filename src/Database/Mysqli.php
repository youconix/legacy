<?php

namespace MiniatureHappiness\LegacyBundle\Database;

use MiniatureHappiness\LegacyBundle\Exceptions\DBException;

/**
 * Database connection layer for MySQL
 * @since 1.0
 * @deprecated
 */
class Mysqli extends AbstractGeneralDAL
{
    /** @var array */
    private $result;

    /**
     * @inheritDoc
     */
    public static function checkLogin(string $username, string $password, string $database, string $host = self::LOCALHOST, int $port = -1): bool
    {
        if (empty($username) || empty($host) || empty($database)) {
            return false;
        }

        $reporting = error_reporting();
        error_reporting(0);

        /* connect to the database */
        if ($port == -1 || $port == '') {
            $connection = new \Mysqli($host, $username, $password, $database);
        } else {
            $connection = new \Mysqli($host, $username, $password, $database, $port);
        }

        error_reporting($reporting);
        if ($connection->connect_errno) {
            return false;
        }

        $connection->close();
        unset($connection);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function connection(string $username, string $password, string $database, string $host = self::LOCALHOST, int $port = -1): void
    {
        if ($this->isConnected) {
            return;
        }

        $this->isConnected = false;

        /* connect to the database */
        if ($port == -1 || $port == '') {
            $this->connection = new \Mysqli($host, $username, $password, $database);
        } else {
            $this->connection = new \Mysqli($host, $username, $password, $database, $port);
        }
        if ($this->connection->connect_errno) {
            /* Error connecting */
            throw new DBException("Error connection to database " . $database . '. Check the connection-settings');
        }

        $this->lastDatabase = $database;
        $this->isConnected = true;
    }

    /**
     * @inheritDoc
     */
    public function connectionEnd(): void
    {
        if ($this->isConnected) {
            @$this->connection->close();
            $this->isConnected = false;
        }
    }

    /**
     * @inheritDoc
     */
    public function dump(string $target): void
    {
        $command = 'mysqldump ' . $this->database . ' --password=' . $this->password . ' --user=' . $this->username
            . ' --host=' . $this->host . ' --port=' . $this->port . ' --single-transaction > ' . $target;
        $result = exec($command, $output);

        if ($result !== '') {
            throw new DBException('Dumping the database failed: ' . $result);
        }
    }

    /**
     * @inheritDoc
     */
    public function escape_string(string $data): string
    {
        $data = htmlentities($data, ENT_QUOTES);

        return $this->connection->real_escape_string($data);
    }

    /**
     * @inheritDoc
     */
    public function prepare(string $query): self
    {
        if (empty($query)) {
            throw new DBException("Illegal query call " . $query);
        }

        $this->reset();

        if (is_null($this->connection)) {
            throw new DBException("No connection to the database");
        }

        $this->currentQuery = null;

        $matches = null;
        if (preg_match_all('/:[a-zA-Z\-_0-9\.]+/s', $query, $matches)) {
            $i = 0;

            $keys = [];
            foreach ($matches[0] as $field) {
                $keys[] = $field;
            }

            $start = (count($keys) - 1);
            for ($i = $start; $i >= 0; $i--) {
                $this->bindedKeys[$keys[$i]] = $i;
            }
        }
        $query = preg_replace('/:[a-zA-Z\-_0-9\.]+/s', '?', $query);
        $this->query = $query;

        $this->currentQuery = $this->connection->stmt_init();

        if (!$this->currentQuery->prepare($query)) {
            throw new DBException("Query failed : " . $this->connection->error . '.\n' . $query);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function execute(): void
    {
        $this->bindParams($this->bindedTypes, $this->bindedValues);
        $res = $this->currentQuery->execute();

        if ($res === false) {
            throw new DBException("Query failed : " . $this->connection->error . '.\n' . $this->query);
        }

        $matches = null;
        preg_match('/^([a-zA-Z]+)\s/', $this->query, $matches);
        $command = strtoupper($matches[1]);
        if ($command == 'SELECT' || $command == 'SHOW' || $command == 'ANALYZE' || $command == 'OPTIMIZE' || $command == 'REPAIR') {
            $this->result = null; // force cleaning

            $this->currentQuery->store_result();

            $obj_meta = $this->currentQuery->result_metadata();
            $params = [];
            while ($field = $obj_meta->fetch_field()) {
                $params[] = &$this->result[$field->name];
            }

            call_user_func_array([
                $this->currentQuery,
                'bind_result'
            ], $params);
        } elseif ($command == 'INSERT') {
            $this->id = $this->currentQuery->insert_id;
        } elseif ($command == 'UPDATE' || $command == 'DELETE') {
            $this->affectedRows = $this->currentQuery->affected_rows;
        }
    }

    /**
     * Bind the values de query
     *
     * @throws DBException
     */
    protected function bindParams(array $types, array $values): void
    {
        $params = [
            0 => ''
        ];
        $num = count($types);

        if ($num == 0) {
            return;
        }

        for ($i = 0; $i < $num; $i++) {
            $type = $types[$i];

            $params[0] .= $type;
            $params[] = $values[$i];
        }

        $callable = [
            $this->currentQuery,
            'bind_param'
        ];
        call_user_func_array($callable, $this->refValues($params));
    }

    /**
     * Callback to bind the parameters to the query
     */
    protected function refValues(array $arguments): array
    {
        if (strnatcmp(phpversion(), '5.3') >= 0) { // Reference is required for PHP 5.3+
            $refs = [];
            foreach ($arguments as $key => $value) {
                $refs[$key] = &$arguments[$key];
            }
            return $refs;
        }
        return $arguments;
    }

    /**
     * @inheritDoc
     */
    public function num_rows(): int
    {
        if (is_null($this->currentQuery)) {
            throw new DBException("Trying to count the numbers of results on a non-SELECT-query");
        }

        return $this->currentQuery->num_rows;
    }

    /**
     * @inheritDoc
     */
    public function result(int $row, string $field): string
    {
        $this->checkSelect();

        if ($row > $this->num_rows() || $row < 0) {
            throw new DBException("Trying to get data from a not existing field");
        }

        $this->resetPointer();

        $rows = $this->num_rows();
        if ($row >= $rows) {
            throw new DBException("Unable to fetch row " . $row . " Only " . $rows . " are present");
        }

        $this->currentQuery->data_seek($row);
        $data = $this->fetch_assoc();

        if (!array_key_exists($field, $data[0])) {
            throw new DBException("Unable to fetch the unknown field " . $field);
        }

        return $data[0][$field];
    }

    /**
     * @inheritDoc
     */
    public function fetch_row(): array
    {
        $this->checkSelect();

        $this->resetPointer();

        $result = [];
        while ($this->currentQuery->fetch()) {
            $field = 0;
            $temp = [];
            foreach ($this->result as $value) {
                $temp[$field] = $value;
                $field++;
            }
            $result[] = $temp;
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetch_array(): array
    {
        $this->checkSelect();

        $this->resetPointer();

        $result = [];
        while ($this->currentQuery->fetch()) {
            $field = 0;
            $temp = [];
            foreach ($this->result as $key => $value) {
                $temp[$field] = $value;
                $temp[$key] = $value;
                $field++;
            }
            $result[] = $temp;
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetch_assoc(): array
    {
        $this->checkSelect();

        $this->resetPointer();

        $result = [];

        while ($this->currentQuery->fetch()) {
            $temp = [];
            foreach ($this->result as $key => $value) {
                $temp[$key] = $value;
            }
            $result[] = $temp;
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetch_assoc_key(string $key): array
    {
        $this->checkSelect();

        $this->resetPointer();

        $result = [];
        while ($this->currentQuery->fetch()) {
            $temp = [];
            foreach ($this->result as $fieldkey => $value) {
                $temp[$fieldkey] = $value;

                if ($fieldkey == $key) {
                    $rowKey = $value;
                }
            }
            $result[$rowKey] = $temp;
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetch_object(): array
    {
        $this->checkSelect();

        $this->resetPointer();

        $temp = [];
        while ($this->currentQuery->fetch()) {
            $object = new \stdClass();
            foreach ($this->result as $fieldkey => $value) {
                $object->$fieldkey = $value;
            }
            $temp[] = $object;
        }

        return $temp;
    }

    /**
     * @inheritDoc
     */
    public function fetch_column(int $column, int $row = 0): string
    {
        $rows = $this->fetch_row();
        if (array_key_exists($row, $rows) || !array_key_exists($column, $rows[$row])) {
            throw new DBException('Can not fetch column number ' . $column . ' from result row ' . $row);
        }

        return $rows[$row][$column];
    }

    /**
     * @inheritDoc
     */
    public function transaction(): void
    {
        if ($this->isTransaction) {
            throw new DBException("Can not start new transaction. Call commit() or rollback() first.");
        }

        $this->prepare("START TRANSACTION");
        $this->execute();
        $this->isTransaction = true;
    }

    /**
     * @inheritDoc
     */
    public function commit(): void
    {
        if (!$this->isTransaction) {
            throw new DBException("Can not commit transaction. Call transaction() first.");
        }

        $this->prepare("COMMIT");
        $this->execute();

        $this->isTransaction = false;
    }

    /**
     * @inheritDoc
     */
    public function rollback(): void
    {
        if (!$this->isTransaction) {
            throw new DBException("Can not rollback transaction. Call transaction() first.");
        }

        $this->prepare("ROLLBACK");
        $this->execute();
        $this->isTransaction = false;
    }

    /**
     * @inheritDoc
     */
    public function useDB(string $database): void
    {
        try {
            $this->prepare("USE " . $database);
            $this->execute();

            $this->lastDatabase = $database;
        } catch (\Exception $ex) {
            throw new DBException("Database-change failed .\n" . $database);
        }
    }

    /**
     * @inheritDoc
     */
    public function databaseExists(string $database): bool
    {
        $this->prepare("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" . $database . "'");
        $this->execute();

        if ($this->num_rows() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Checks if the last query was a SELECT-query
     *
     * @throws DBException when no SELECT-query was executed
     */
    protected function checkSelect(): void
    {
        if ($this->currentQuery === null) {
            throw new DBException("Database-error : trying to get data on a non-SELECT-query");
        }
    }

    /**
     * @inheritDoc
     */
    public function describe(string $table, bool $addNotExists = false, bool $dropTable = false): string
    {
        $this->prepare('SHOW CREATE TABLE ' . $table);
        $this->execute();

        if ($this->num_rows() == 0) {
            throw new DBException('Table ' . $table . ' does not exist.');
        }

        $table = $this->fetch_row();
        $description = $table[0][1];
        if ($dropTable) {
            $description = 'DROP TABLE IF EXISTS ' . $table . ";\n" . $description;
        }
        if ($addNotExists) {
            $description = str_replace('CREATE TABLE', 'CREATE TABLE IF NOT EXISTS', $description);
        }
        return $description;
    }

    /**
     * Resets the data result pointer
     */
    protected function resetPointer(): void
    {
        if (!is_null($this->currentQuery)) {
            $this->currentQuery->data_seek(0);
        }
    }

    /**
     * Clears the previous result set
     */
    protected function clearResult(): void
    {
        if (!is_null($this->currentQuery)) {
            $this->currentQuery->free_result();
            $this->currentQuery->close();

            $this->currentQuery = null;
        }
    }
}
