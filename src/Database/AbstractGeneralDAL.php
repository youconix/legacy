<?php

namespace MiniatureHappiness\LegacyBundle\Database;

use MiniatureHappiness\LegacyBundle\Exceptions\DBException;
use MiniatureHappiness\LegacyBundle\Interfaces\DALInterface;
use MiniatureHappiness\LegacyBundle\Interfaces\SettingsInterface;

/**
 * General database connection layer
 *
 * @since 1.0
 * @deprecated Use Doctrine instead
 */
abstract class AbstractGeneralDAL implements DALInterface
{
    protected string $username;
    protected string $password;
    protected string $database;
    protected string $host;
    protected int $port;

    protected $settings;
    protected $connection;

    /** @var string */
    protected $query;
    protected $currentQuery;

    /** @var bool */
    protected $isConnected;

    /** @var string */
    protected $lastDatabase;

    /** @var int */
    protected $id;

    /** @var int */
    protected $affectedRows;

    /** @var bool */
    protected $isTransaction = false;

    /** @var array */
    protected $bindedKeys = [];

    /** @var array */
    protected $bindedTypes = [];

    /** @var array */
    protected $bindedValues = [];

    public function __construct(string $username, string $password, string $database, string $host, int $port)
    {
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        if ($this->isConnected) {
            $this->connectionEnd();
        }

        $this->connection = null;
        $this->currentQuery = null;
        $this->isConnected = null;
        $this->lastDatabase = null;
        $this->id = null;
        $this->affectedRows = null;
        $this->isTransaction = null;
    }

    /**
     * @inheritDoc
     */
    public static function isSingleton(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function defaultConnect(): void
    {
        $this->isConnected = false;

        $this->connection(
            $this->username,
            $this->password,
            $this->database,
            $this->host,
            $this->port
        );

        $this->reset();
    }

    /**
     * @inheritDoc
     */
    public function reset(): void
    {
        if (is_object($this->currentQuery) && !$this->isTransaction) {
            $this->clearResult();
        }

        $this->query = '';
        $this->currentQuery = null;
        $this->id = -1;
        $this->affectedRows = -1;
        $this->bindedKeys = [];
        $this->bindedTypes = [];
        $this->bindedValues = [];
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return (int) $this->id;
    }

    /**
     * @inheritDoc
     */
    public function affected_rows(): int
    {
        return $this->affectedRows;
    }

    /**
     * @inheritDoc
     */
    public function isConnected(): bool
    {
        return $this->isConnected;
    }

    /**
     * @inheritDoc
     */
    public function query(string $query): void
    {
        $this->prepare($query);
    }

    /**
     * @inheritDoc
     */
    public function queryBinded(string $query, array $types, array $values): void
    {
        $this->prepare($query);

        $this->bindedTypes = $types;
        $this->bindedValues = $values;

        $this->exequte();
    }

    /**
     * @inheritDoc
     */
    public function bindString(string $field, string $value): self
    {
        $this->bind($field, $value, 's');

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bindInt(string $field, int $value): self
    {
        $this->bind($field, $value, 'i');

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bindFloat(string $field, float $value): self
    {
        $this->bind($field, $value, 'f');

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bindBlob(string $field, $value): self
    {
        $this->bind($field, $value, 'b');

        return $this;
    }

    private function bind(string $field, mixed $value, string $type): void
    {
        if (!array_key_exists(':' . $field, $this->bindedKeys)) {
            throw new DBException('Trying to bind to unknown key ' . $field);
        }
        $pos = $this->bindedKeys[':' . $field];
        $this->bindedTypes[$pos] = $type;
        $this->bindedValues[$pos] = $value;
    }

    /**
     * @inheritDoc
     */
    public function analyse(string $table): bool
    {
        $this->prepare("ANALYZE TABLE " . $table);
        $this->execute();

        $result = $this->fetch_assoc();
        if ($result[0]['Msg_text'] != 'OK' && $result[0]['Msg_text'] != 'Table is already up to date') {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function repair(string $table): bool
    {
        $this->prepare("REPAIR TABLE " . $table);
        $this->execute();

        $result = $this->fetch_assoc();

        if ($result[0]['Msg_text'] == "The storage engine for the table doesn't support repair") {
            return true;
        }

        if ($result[0]['Msg_text'] != 'OK') {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function optimize(string $table): bool
    {
        $this->prepare("OPTIMIZE TABLE " . $table);
        $this->execute();

        $result = $this->fetch_assoc();

        if ($result[0]['Msg_text'] == 'Table does not support optimize, doing recreate + analyze instead') {
            return true;
        }

        if ($result[0]['Msg_text'] != 'OK' && $result[0]['Msg_text'] != 'Table is already up to date') {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getDatabase(): string
    {
        return $this->lastDatabase;
    }
}
