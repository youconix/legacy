<?php

namespace MiniatureHappiness\LegacyBundle\Database;

use MiniatureHappiness\LegacyBundle\Interfaces\HavingInterface;

/**
 * @deprecated
 */
class Having_Mysqli extends QueryConditions_Mysqli implements HavingInterface
{
    /**
     *
     * @var Builder_Mysqli
     */
    protected $parent;

    /**
     * @inheritDoc
     */
    public function startSubHaving(): self
    {
        $this->query .= '(';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function endSubHaving(): self
    {
        $this->query .= ')';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function render(): ?array
    {
        if (empty($this->query)) {
            return null;
        }

        return [
            'having' => ' HAVING ' . $this->query,
            'values' => $this->values,
            'types' => $this->types
        ];
    }
}