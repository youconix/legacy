<?php

namespace MiniatureHappiness\LegacyBundle\Database;

use MiniatureHappiness\LegacyBundle\Exceptions\DBException;
use MiniatureHappiness\LegacyBundle\Interfaces\BuilderInterface;
use MiniatureHappiness\LegacyBundle\Interfaces\CreateInterface;
use MiniatureHappiness\LegacyBundle\Interfaces\DALInterface;
use MiniatureHappiness\LegacyBundle\Interfaces\HavingInterface;
use MiniatureHappiness\LegacyBundle\Interfaces\WhereInterface;

/**
 * Mysql query builder
 *
 * @since 1.0
 * @deprecated
 */
class Builder_Mysqli implements BuilderInterface
{
    /** @var DALInterface */
    protected $dal;

    /** @var string */
    protected $query;

    /** @var string */
    protected $limit;

    /** @var string */
    protected $group;

    /** @var string */
    protected $order;

    /** @var array */
    protected $joins;

    /** @var array */
    protected $fieldsPre;

    /** @var array */
    protected $fields;

    /** @var array */
    protected $values;

    /** @var array */
    protected $types;

    /** @var bool */
    protected $isCreate;

    /** @var string */
    protected $resultQuery;

    /** @var Where_Mysqli */
    protected $where;

    /** @var Create_Mysqli */
    protected $create;

    /** @var Having_Mysqli */
    protected $having;

    /** @var bool */
    protected $upsert = false;

    public function __construct(DALInterface $dal)
    {
        $this->dal = $dal;
        if (!$this->dal->isConnected()) {
            $this->dal->defaultConnect();
        }

        $this->where = new Where_Mysqli($this);
        $this->create = new Create_Mysqli($this);
        $this->having = new Having_Mysqli($this);
        $this->reset();
    }

    public function __destruct()
    {
        $this->dal = null;
        $this->where = null;
        $this->create = null;
        $this->having = null;
    }

    /**
     * Resets the builder
     */
    protected function reset(): void
    {
        $this->query = '';
        $this->limit = '';
        $this->group = '';
        $this->order = '';
        $this->joins = [];
        $this->fieldsPre = [];
        $this->fields = [];
        $this->values = [];
        $this->types = [];
        $this->isCreate = false;
        $this->resultQuery = '';
        $this->where->reset();
        $this->create->reset();
        $this->having->reset();
        $this->upsert = false;
    }

    public function __clone()
    {
        $this->where = new Where_Mysqli($this);
        $this->create = new Create_Mysqli($this);
        $this->having = new Having_Mysqli($this);
        $this->reset();
    }

    /**
     * @inheritDoc
     */
    public static function isSingleton(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function showTables(): self
    {
        $this->isCreate = false;

        $this->query = 'SHOW TABLES';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function showDatabases(): self
    {
        $this->isCreate = false;

        $this->query = 'SHOW DATABASES';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function select(string $table, string $fieldNames): self
    {
        $this->isCreate = false;

        $this->query = "SELECT " . $fieldNames . " FROM " . DB_PREFIX . $table . " ";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function insert(string $table, bool $ignoreErrors = false): self
    {
        $this->isCreate = false;

        if ($ignoreErrors) {
            $this->query = "INSERT IGNORE INTO " . DB_PREFIX . $table . " ";
        } else {
            $this->query = "INSERT INTO " . DB_PREFIX . $table . " ";
        }

        $this->fields = [];
        $this->values = [];
        $this->types = [];

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function update(string $table): self
    {
        $this->isCreate = false;

        $this->query = "UPDATE " . DB_PREFIX . $table . " ";
        $this->fields = [];
        $this->values = [];
        $this->types = [];

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function delete(string $table): self
    {
        $this->isCreate = false;

        $this->query = "DELETE FROM " . DB_PREFIX . $table . " ";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function upsert(string $table, string $unique): self
    {
        $this->isCreate = false;
        $this->upsert = true;

        return $this->insert($table);
    }

    /**
     * @inheritDoc
     */
    public function bindString(string $key, string $value): self
    {
        $this->fields[] = $key;
        $this->values[$key] = $value;
        $this->types[$key] = 's';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bindInt(string $key, int $value): self
    {
        $this->fields[] = $key;
        $this->values[$key] = $value;
        $this->types[$key] = 'i';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bindFloat(string $key, float $value): self
    {
        $this->fields[] = $key;
        $this->values[$key] = $value;
        $this->types[$key] = 'f';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bindBlob(string $key, $value): self
    {
        $this->fields[] = $key;
        $this->values[$key] = $value;
        $this->types[$key] = 'b';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bindLiteral(string $key, string $statement): self
    {
        $this->fields[] = $key;
        $this->values[$key] = $statement;
        $this->types[$key] = 'l';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCreate(string $table, bool $dropTable): CreateInterface
    {
        $this->isCreate = true;
        $this->create->setTable($table, $dropTable);

        return $this->create;
    }

    /**
     * @inheritDoc
     */
    public function innerJoin(string $table, string $field1, string $field2): self
    {
        $this->joins[] = "INNER JOIN " . DB_PREFIX . $table . " ON " . $field1 . " = " . $field2 . " ";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function outerJoin(string $table, string $field1, string $field2): self
    {
        $this->joins[] = "OUTER JOIN " . DB_PREFIX . $table . " ON " . $field1 . " = " . $field2 . " ";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function leftJoin(string $table, string $field1, string $field2): self
    {
        $this->joins[] = "LEFT JOIN " . DB_PREFIX . $table . " ON " . $field1 . " = " . $field2 . " ";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function rightJoin(string $table, string $field1, string $field2): self
    {
        $this->joins[] = "RIGHT JOIN " . DB_PREFIX . $table . " ON " . $field1 . " = " . $field2 . " ";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getWhere(): WhereInterface
    {
        return $this->where;
    }

    /**
     * @inheritDoc
     */
    public function limit(int $limit, int $offset = 0): self
    {
        $this->limit = "LIMIT " . $offset . "," . $limit . " ";

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function group(string $field): self
    {
        $this->group = 'GROUP BY ' . $field;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getHaving(): HavingInterface
    {
        return $this->having;
    }

    /**
     * @inheritDoc
     */
    public function order(string $field1, string $ordering1 = 'ASC', string $field2 = '', string $ordering2 = 'ASC'): self
    {
        $this->order = "ORDER BY " . $field1 . " " . $ordering1;
        if (empty($field2)) {
            $this->order .= " ";
        } else {
            $this->order .= "," . $field2 . " " . $ordering2 . " ";
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSum(string $field, string $alias = ''): string
    {
        return $this->getSpecialField($field, $alias, 'SUM');
    }

    /**
     * @inheritDoc
     */
    public function getMaximum(string $field, string $alias = ''): string
    {
        return $this->getSpecialField($field, $alias, 'MAX');
    }

    /**
     * @inheritDoc
     */
    public function getMinimum(string $field, string $alias = ''): string
    {
        return $this->getSpecialField($field, $alias, 'MIN');
    }

    /**
     * @inheritDoc
     */
    public function getAverage(string $field, string $alias = ''): string
    {
        return $this->getSpecialField($field, $alias, 'AVG');
    }

    /**
     * @inheritDoc
     */
    public function getCount(string $field, string $alias = ''): string
    {
        return $this->getSpecialField($field, $alias, 'COUNT');
    }

    private function getSpecialField(string $field, string $alias, string $key): string
    {
        if (!empty($alias)) {
            $alias = 'AS ' . $alias . ' ';
        }

        return $key . '(' . $field . ') ' . $alias;
    }

    /**
     * @inheritDoc
     */
    public function getResult(): DALInterface
    {
        $query = $this->render();

        $this->dal->prepare($query['query']);

        foreach ($query['types'] as $field => $type) {
            switch ($type) {
                case 's':
                    $this->dal->bindString($field, $query['values'][$field]);
                    break;

                case 'i':
                    $this->dal->bindInt($field, $query['values'][$field]);
                    break;

                case 'f':
                    $this->dal->bindFloat($field, $query['values'][$field]);
                    break;

                case 'b':
                    $this->dal->bindBlob($field, $query['values'][$field]);
                    break;
            }
        }

        $this->dal->execute();

        return $this->dal;
    }

    /**
     * @inheritDoc
     */
    public function render(): array
    {
        $this->resultQuery = $this->query;
        if (!is_array($this->fields)) {
            $this->fields = [
                $this->fields
            ];
        }

        $command = strtoupper(substr($this->query, 0,
            strpos($this->query, ' ')));

        switch ($command) {
            case 'SELECT':
                $this->addJoins();

                $this->addHaving();

                $this->addWhere();

                $this->addGroup();

                $this->addOrder();

                $this->addLimit();

                break;
            case 'UPDATE':
                $this->addJoins();

                $data = [];
                foreach ($this->fields as $field) {
                    if ($this->types[$field] != 'l') {
                        $data[] = $field . ' = :' . $field;
                    } else {
                        $data[] = $field . ' = ' . $this->values[$field];
                        unset($this->values[$field]);
                        unset($this->types[$field]);
                    }
                }

                $this->resultQuery .= ' SET ' . implode(',', $data) . ' ';

                $this->addGroup();

                $this->addHaving();

                $this->addWhere();

                $this->addLimit();

                $this->addLimit();

                break;
            case 'INSERT':
                $values = [];
                foreach ($this->fields as $field) {
                    if ($this->types[$field] != 'l') {
                        $values[] = ':' . $field;
                    } else {
                        $field .= ' = ' . $this->values[$field];
                        unset($this->values[$field]);
                        unset($this->types[$field]);
                    }
                }

                $this->resultQuery .= '(' . implode(',', $this->fields) . ') VALUES (' . implode(',',
                        $values) . ') ';

                if ($this->upsert) {
                    $updateFields = [];
                    foreach ($this->fields as $field) {
                        $updateFields[] = $field . ' = VALUES(' . $field . ')';
                    }

                    $this->resultQuery .= 'ON DUPLICATE KEY UPDATE ' . implode(',', $updateFields);
                }
                break;
            case 'DELETE':
                $this->addWhere();

                $this->addLimit();
                break;
            case 'SHOW':
                $this->addWhere();
                break;

            default:
                if ($this->isCreate) {
                    $dropTable = $this->create->getDropTable();

                    if ($dropTable != '') {
                        $this->dal->prepare($dropTable);
                    }

                    $this->resultQuery = $this->create->render();
                }
                break;
        }

        $data = [
            'query' => $this->resultQuery,
            'values' => $this->values,
            'types' => $this->types
        ];
        $this->reset();

        return $data;
    }

    /**
     * Adds the joins
     */
    private function addJoins(): void
    {
        foreach ($this->joins as $join) {
            $this->resultQuery .= $join;
        }
    }

    /**
     * Adds the group by
     */
    private function addGroup(): void
    {
        $this->resultQuery .= $this->group . " ";
    }

    /**
     * Adds the having part
     */
    private function addHaving(): void
    {
        $having = $this->having->render();
        if (is_null($having)) {
            return;
        }

        $this->values = array_merge($this->values, $having['values']);
        $this->types = array_merge($this->types, $having['types']);

        $this->resultQuery .= $having['having'] . " ";
    }

    /**
     * Adds the where part
     */
    private function addWhere(): void
    {
        $where = $this->where->render();
        if (is_null($where)) {
            return;
        }

        $this->values = array_merge($this->values, $where['values']);
        $this->types = array_merge($this->types, $where['types']);

        $this->resultQuery .= $where['where'] . " ";
    }

    /**
     * Adds the limit part
     */
    private function addLimit(): void
    {
        $this->resultQuery .= $this->limit;
    }

    /**
     * Adds the order part
     */
    private function addOrder(): void
    {
        $this->resultQuery .= $this->order;
    }

    /**
     * @inheritDoc
     */
    public function transaction(): self
    {
        $this->dal->transaction();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function commit(): self
    {
        $this->dal->commit();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function rollback(): self
    {
        $this->dal->rollback();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDatabase(): DALInterface
    {
        return $this->dal;
    }

    /**
     * @inheritDoc
     */
    public function dumpDatabase(): string
    {
        $sql = '';

        /* Remove constrains */
        $this->dal->prepare("SELECT table_name,column_name,referenced_table_name,referenced_column_name,constraint_name
            FROM  information_schema.key_column_usage WHERE
            referenced_table_name is not null
            and table_schema = '" . $this->dal->getDatabase() . "'");

        $constrains = [];
        if ($this->dal->num_rows() > 0) {
            $constrains = $this->dal->fetch_assoc();

            foreach ($constrains as $constrain) {
                $sql .= 'ALTER TABLE ' . $constrain['table_name'] . ' IF EXISTS DROP FOREIGN KEY ' . $constrain['constraint_name'] . ';';
            }
            $sql .= "\n-- --------------------------\n";
        }

        $this->dal->prepare('SHOW TABLES');
        $tables = $this->dal->fetch_row();
        foreach ($tables as $table) {
            $table = str_replace(DB_PREFIX, '', $table);

            $sql .= $this->dumpTable($table[0]);
            $sql .= "\n";
            $sql .= "-- ---------------------------\n\n";
        }

        /* Restore constrains */
        if (count($constrains) > 0) {
            foreach ($constrains as $contrain) {
                $sql .= 'ALTER TABLE ' . $contrain['table_name'] . ' ADD CONSTRAINT ' . $contrain['constraint_name'] . ' FOREIGN KEY ( ' . $contrain['column_name'] . ')
                    REFERENCES ' . $contrain['referenced_table_name'] . ' ( ' . $contrain['referenced_column_name'] . ' ) ON DELETE RESTRICT ON UPDATE RESTRICT ;' . "\n";
            }

            $sql .= "\n-- --------------------------\n";
        }

        return $sql;
    }

    /**
     * @throws \Exception
     */
    protected function dumpTable(string $table): string
    {
        /* Table structure */
        $sql = "--\n" . '-- Table structure for table ' . DB_PREFIX . $table . ".\n--\n";
        $structure = $this->dal->describe(DB_PREFIX . $table, false,
            true);

        /* Table content */
        $this->select($table, '*');
        $database = $this->getResult();

        /* Get colums */
        $sql .= "--\n" . '-- Dumping data for table ' . DB_PREFIX . $table . ".\n--\n";
        if ($database->num_rows() == 0) {
            return $sql;
        }
        $data = $database->fetch_assoc();
        $keys = array_keys($data[0]);

        $columns = [];
        foreach ($keys as $column) {
            $columns[] = $column;
        }
        $insert = 'INSERT INTO ' . $table . ' (' . implode(',', $columns) . ') VALUES (';

        foreach ($data as $item) {
            $values = [];
            foreach ($item as $key => $value) {
                if (is_numeric($value)) {
                    $values[] = $value;
                } else {
                    $values[] = "'" . str_replace("'", "\'", $value) . "'";
                }
            }

            $sql .= $insert . implode(',', $values) . ");\n";
        }

        return $sql;
    }

    /**
     * @inheritDoc
     */
    public function describe(string $table): array
    {
        $this->dal->prepare('SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = :database AND TABLE_NAME = :table');
        $this->dal->bindString('database',
            $this->dal->getDatabase());
        $this->dal->bindString('table', DB_PREFIX . $table);
        $this->dal->execute();

        return $this->dal->fetch_assoc_key('COLUMN_NAME');
    }

    /**
     * @inheritDoc
     */
    public function describeFields(string $table): array
    {
        $descriptionRaw = $this->describe($table);

        $description = [];
        foreach ($descriptionRaw as $row) {
            $description[$row['COLUMN_NAME']] = [
                'type' => $row['DATA_TYPE'],
                'null' => ($row['IS_NULLABLE'] != 'NO'),
                'primary' => ($row['COLUMN_KEY'] == 'PRI'),
                'max' => $row['CHARACTER_MAXIMUM_LENGTH'],
                'default' => $row['COLUMN_DEFAULT'],
                'set' => []
            ];

            if ($row['DATA_TYPE'] == 'enum') {
                $field = str_replace([
                    'enum(',
                    ')'
                ],
                    [
                        '',
                        ''
                    ], $row['COLUMN_TYPE']);
                $field = explode(',', $field);
                foreach ($field as $item) {
                    $a_description[$row['COLUMN_NAME']]['set'][] = str_replace([
                        "'",
                        '"'
                    ],
                        [
                            '',
                            ''
                        ], $item);
                }
            }
        }

        return $description;
    }
}