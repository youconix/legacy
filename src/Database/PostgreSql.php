<?php

namespace MiniatureHappiness\LegacyBundle\Database;

use MiniatureHappiness\LegacyBundle\Exceptions\DBException;
use stdClass;

/**
 * Database connection layer for PostgreSQL
 *
 * @author Rachelle Scheijen
 * @since 1.0
 * @deprecated
 */
class PostgreSql extends AbstractGeneralDAL
{

    /** @var stdClass */
    protected $connectionData;

    /**
     * @inheritDoc
     */
    public static function checkLogin(string $username, string $password, string $database, string $host = self::LOCALHOST, int $port = -1): bool
    {
        if ($port === -1) {
            $port = 5432;
        }

        $reporting = error_reporting();
        error_reporting(0);

        try {
            /* connect to the database */
            $res = pg_connect("host=" . $host . " port=" . $port . " dbname=" . $database . " user=" . $username . " password=" . $password);

            error_reporting($reporting);

            if (!$res) {
                return false;
            }

            pg_close($res);

            return true;
        } catch (\Exception $exception) {
            error_reporting($reporting);
            /* Error connecting */
            return false;
        }
    }

    /**
     * @inheritDoc
     */
    public function connection(string $username, string $password, string $database, string $host = self::LOCALHOST, int $port = -1): void
    {
        if ($this->isConnected) {
            return;
        }

        if ($port === -1) {
            $port = 5432;
        }

        $this->connectionData = new stdClass();
        $this->connectionData->host = $host;
        $this->connectionData->port = $port;
        $this->connectionData->username = $username;
        $this->connectionData->password = $password;

        $res = @pg_connect("host=" . $host . " port=" . $port . " dbname=" . $database . " user=" . $username . " password=" . $password);
        if ($res == false) {
            /* Error connecting */
            throw new DBException("Error connection to database " . $database . '. Check the connection-settings');
            $this->isConnected = false;
        }

        $this->lastDatabase = $database;
        $this->isConnected = true;
        $this->connection = $res;
    }

    /**
     * @inheritDoc
     */
    public function connectionEnd(): void
    {
        if ($this->isConnected) {
            pg_close($this->connection);
            $this->connection = null;
            $this->isConnected = false;
        }
    }

    /**
     * @inheritDoc
     */
    public function dump(string $target): void
    {
        $command = 'pg_dump --username ' . $this->username . ' --port ' . $this->port . ' --host=' . $this->host . ' -Fc --file=' . $target . ' ' . $this->database;
        $result = exec($command, $output);

        if ($result !== '') {
            throw new DBException('Dumping the database failed: ' . $result);
        }
    }

    /**
     * @inheritDoc
     */
    public function escape_string(string $data): string
    {
        $data = htmlentities($data, ENT_QUOTES);

        return pg_escape_string($data);
    }

    /**
     * @inheritDoc
     */
    public function prepare(string $query): self
    {
        if (empty($query)) {
            throw new DBException("Illegal query call " . $query);
        }

        $this->reset();

        if (is_null($this->connection)) {
            throw new DBException("No connection to the database");
        }

        $this->currentQuery = null;
        $this->query = $query;

        $this->currentQuery = pg_prepare($this->connection, '', $query);
        if ($this->currentQuery === false) {
            throw new DBException("Query failed : " . pg_last_error($this->connection) . '.\n' . $query);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function execute(): void
    {
        $result = pg_execute($this->connection, '', $this->bindedValues);

        if ($result === false) {
            throw new DBException("Query failed : " . pg_last_error($this->connection) . '.\n' . $this->query);
        }

        $matches = null;
        preg_match('/^([a-zA-Z]+)\s/', $this->query, $matches);
        $command = strtoupper($matches[1]);
        switch ($command) {
            case 'SELECT':
            case 'SHOW':
            case 'ANALYZE':
            case 'OPTIMIZE':
            case 'REPAIR':
                $this->currentQuery = $result;

                if ($this->query == 'SELECT lastval()') {
                    $data = $this->fetch_row();
                    $this->id = $data[0];
                    $this->currentQuery = null;
                }
                break;
            case 'INSERT':
                $this->prepare('SELECT lastval()');
                $this->exequte();
                break;
            case 'UPDATE':
            case 'DELETE':
                $this->affectedRows = pg_affected_rows($result);
                break;
        }
    }

    /**
     * @inheritDoc
     */
    public function num_rows(): int
    {
        if (is_null($this->currentQuery)) {
            throw new DBException("Trying to count the numbers of results on a non-SELECT-query");
        }

        return pg_num_rows($this->currentQuery);
    }

    /**
     * @inheritDoc
     */
    public function result(int $row, string $field): string
    {
        if (is_null($this->currentQuery)) {
            throw new DBException("Trying to get data on a non-SELECT-query");
        }

        return pg_fetch_result($this->currentQuery, $row, $field);
    }

    /**
     * @inheritDoc
     */
    public function fetch_row(): array
    {
        if (is_null($this->currentQuery)) {
            throw new DBException("Trying to get data on a non-SELECT-query");
        }

        $temp = [];
        while ($res = pg_fetch_row($this->currentQuery)) {
            $temp[] = $res;
        }

        return $temp;
    }

    /**
     * @inheritDoc
     */
    public function fetch_array(): array
    {
        if (is_null($this->currentQuery)) {
            throw new DBException("Trying to get data on a non-SELECT-query");
        }

        $ret = [];
        for ($i = 0; $arr = pg_fetch_array($this->currentQuery, $i, PGSQL_ASSOC); $i++) {
            $ret = $arr[$i];
        }

        return $ret;
    }

    /**
     * @inheritDoc
     */
    public function fetch_assoc(): array
    {
        if (is_null($this->currentQuery)) {
            throw new DBException("Trying to get data on a non-SELECT-query");
        }

        $temp = [];
        while ($res = pg_fetch_assoc($this->currentQuery)) {
            $temp[] = $res;
        }

        return $temp;
    }

    /**
     * @inheritDoc
     */
    public function fetch_assoc_key(string $key): array
    {
        if (is_null($this->currentQuery)) {
            throw new DBException("Trying to get data on a non-SELECT-query");
        }

        $temp = [];
        while ($res = pg_fetch_assoc($this->currentQuery)) {
            $temp[$res[$key]] = $res;
        }

        return $temp;
    }

    /**
     * @inheritDoc
     */
    public function fetch_object(): array
    {
        if (is_null($this->currentQuery)) {
            throw new DBException("Trying to get data on a non-SELECT-query");
        }

        $temp = [];
        while ($res = pg_fetch_object($this->currentQuery)) {
            $temp[] = $res;
        }

        return $temp;
    }

    /**
     * @inheritDoc
     */
    public function transaction(): void
    {
        if ($this->isTransaction) {
            throw new DBException("Can not start new transaction. Call commit() or rollback() first.");
        }

        $this->prepare("BEGIN");
        $this->execute();
        $this->isTransaction = true;
    }

    /**
     * @inheritDoc
     */
    public function commit(): void
    {
        if (!$this->isTransaction) {
            throw new DBException("Can not commit transaction. Call transaction() first.");
        }

        $this->prepare("COMMIT");
        $this->execute();
        $this->isTransaction = false;
    }

    /**
     * @inheritDoc
     */
    public function rollback(): void
    {
        if (!$this->isTransaction) {
            throw new DBException("Can not rollback transaction. Call transaction() first.");
        }

        $this->prepare("ROLLBACK");
        $this->execute();
        $this->isTransaction = false;
    }

    /**
     * @inheritDoc
     */
    public function useDB(string $database): void
    {
        $this->connectionEnd();
        $this->connection($this->connectionData->username,
            $this->connectionData->password, $database,
            $this->connectionData->host, $this->connectionData->port);
    }

    /**
     * @inheritDoc
     */
    public function databaseExists(string $database): bool
    {
        $this->prepare('SELECT 1 FROM pg_database WHERE datname = ":database"');
        $this->bindString('database', $database);
        $this->exequte();

        return ($this->num_rows() > 0);
    }

    /**
     * @inheritDoc
     */
    public function describe(string $table, bool $addNotExists = false, bool $dropTable = false): string
    {
        $this->prepare("SELECT f.attnum AS number, f.attname AS name, f.attnum, f.attnotnull AS notnull,  
            pg_catalog.format_type(f.atttypid,f.atttypmod) AS type,  
        CASE  
            WHEN p.contype = 'p' THEN 't'  
            ELSE 'f'  
            END AS primarykey,  
        CASE  
            WHEN p.contype = 'u' THEN 't'  
            ELSE 'f'
            END AS uniquekey,
        CASE
            WHEN p.contype = 'f' THEN g.relname
            END AS foreignkey,
        CASE
            WHEN p.contype = 'f' THEN p.confkey
            END AS foreignkey_fieldnum,
        CASE
            WHEN p.contype = 'f' THEN g.relname
            END AS foreignkey,
        CASE
            WHEN p.contype = 'f' THEN p.conkey
            END AS foreignkey_connnum,
        CASE
            WHEN f.atthasdef = 't' THEN d.adsrc
            END AS default
        FROM pg_attribute f  
            JOIN pg_class c ON c.oid = f.attrelid  
            JOIN pg_type t ON t.oid = f.atttypid  
            LEFT JOIN pg_attrdef d ON d.adrelid = c.oid AND d.adnum = f.attnum  
            LEFT JOIN pg_namespace n ON n.oid = c.relnamespace  
            LEFT JOIN pg_constraint p ON p.conrelid = c.oid AND f.attnum = ANY (p.conkey)  
            LEFT JOIN pg_class AS g ON p.confrelid = g.oid  
        WHERE c.relkind = 'r'::char  
        AND n.nspname = ':schema'  -- Replace with Schema name  
        AND c.relname = ':relname'  -- Replace with table name  
        AND f.attnum > 0 ORDER BY number");
        $this->bindString('schema', $this->getDatabase());
        $this->bindstring('relname', $table);
        $this->exequte();

        $table = $this->fetch_row();
        $description = $table[0][1];
        if ($dropTable) {
            $description = 'DROP TABLE IF EXISTS ' . $table . ";\n" . $description;
        }
        if ($addNotExists) {
            $description = str_replace('CREATE TABLE', 'CREATE TABLE IF NOT EXISTS',
                $description);
        }
        return $description;
    }

    /**
     * @deprecated
     */
    public function getVersion(): string
    {
        $this->prepare('SELECT version()');
        $this->execute();

        $data = $this->fetch_row();
        $data = explode(' ', $data[0]);
        $data = explode('.', $data[1]);

        return $data[0] . '.' . $data[1];
    }

    /**
     * @inheritDoc
     */
    public function fetch_column(int $column, int $row = 0): string
    {
        $rows = $this->fetch_row();
        if (array_key_exists($row, $rows) || !array_key_exists($column, $rows[$row])) {
            throw new DBException('Can not fetch column number ' . $column . ' from result row ' . $row);
        }

        return $rows[$row][$column];
    }
}
