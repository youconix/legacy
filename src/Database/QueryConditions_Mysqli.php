<?php

namespace MiniatureHappiness\LegacyBundle\Database;

use MiniatureHappiness\LegacyBundle\Interfaces\ConditionsInterface;
use MiniatureHappiness\LegacyBundle\Interfaces\DALInterface;

/**
 * @deprecated
 */
abstract class QueryConditions_Mysqli implements ConditionsInterface
{
    /** @var Builder_Mysqli */
    protected $parent;

    /** @var string */
    protected $query;

    /** @var array */
    protected $types;

    /** @var array */
    protected $values;

    /** @var array */
    protected $keys = [
        '=' => '=',
        '==' => '=',
        '<>' => '<>',
        '!=' => '<>',
        '<' => '<',
        '>' => '>',
        'LIKE' => 'LIKE',
        'IN' => 'IN',
        'BETWEEN' => 'BETWEEN',
        '<=' => '<=',
        '>=' => '>='
    ];

    public function __construct(Builder_Mysqli $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @inheritDoc
     */
    public function reset(): self
    {
        $this->query = '';
        $this->types = [];
        $this->values = [];
    }

    /**
     * @inheritDoc
     */
    public function bindString(string $field, string $value, string $type = 'AND', string $key = '='): self
    {
        $this->bind($field, $value, $type, $key, 's');

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bindInt(string $field, int $value, string $type = 'AND', string $key = '='): self
    {
        $this->bind($field, $value, $type, $key, 'i');

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bindFloat(string $field, float $value, string $type = 'AND', string $key = '='): self
    {
        $this->bind($field, $value, $type, $key, 'f');

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bindBlob(string $field, $value, string $type = 'AND', string $key = '='): self
    {
        $this->bind($field, $value, $type, $key, 'b');

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function bindLiteral(string $field, string $statement, string $type = 'AND', string $key = '='): self
    {
        $this->bind($field, $statement, $type, $key, 'l');

        return $this;
    }

    /**
     * Binds a field
     *
     * @param string $glue The glue type (AND|OR)
     * @param string $key (=|<>|<|>|LIKE|IN|BETWEEN)
     * @param string $type The bind type (s|i|f|b|l)
     */
    private function bind(string $field, mixed $value, string $glue, string $key, string $type): void
    {
        $glue = strtoupper($glue);
        if (!in_array($glue, [
            'AND',
            'OR'
        ])) {
            $glue = 'AND';
        }

        $key = strtoupper($key);
        if (!array_key_exists($key, $this->keys)) {
            $key = '=';
        }
        $key = $this->keys[$key];

        if ($this->query !== '') {
            $this->query .= ' ' . $glue . ' ';
        }

        if ($type === 'l') {
            $this->query .= $field . ' = ' . $value;
            return;
        }

        $counter = 0;
        while (array_key_exists($field . '_' . $counter, $this->values)) {
            $counter++;
        }

        switch ($key) {
            case 'BETWEEN' :
                $this->query .= $field . ' BETWEEN :' . $field . '_1 AND :' . $field . '_2';
                $this->values[$field . '_1'] = $value[0];
                $this->types[$field . '_1'] = $type[0];

                $this->values[$field . '_2'] = $value[1];
                $this->types[$field . '_2'] = $type[0];
                break;
            case 'IN' :
                $fields = [];
                for ($i = 0; $i < count($value); $i++) {
                    $fields[] = ':' . $field . '_' . ($counter + $i);
                    $this->types[$field . '_' . ($counter + $i)] = $type;
                    $this->values[$field . '_' . ($counter + $i)] = $value[$i];
                }

                $this->query .= $field . ' IN (' . implode(',', $fields) . ')';
                break;
            case 'LIKE' :
                $this->query .= $field . ' LIKE :' . $field;
                $this->types[$field] = $type;
                $this->values[$field] = $value;
                break;
            default:
                $this->query .= $field . ' ' . $key . ' :' . $field . '_' . $counter;
                $this->values[$field . '_' . $counter] = $value;
                $this->types[$field . '_' . $counter] = $type;
                break;
        }
    }

    /**
     * @inheritDoc
     */
    public function getResult(): DALInterface
    {
        return $this->parent->getResult();
    }
}