<?php

namespace MiniatureHappiness\LegacyBundle\Database;

use MiniatureHappiness\LegacyBundle\Interfaces\DALInterface;
use MiniatureHappiness\LegacyBundle\Interfaces\HavingInterface;

/**
 * @deprecated
 */
class Having_PostgreSql extends QueryConditions_PostgreSql implements HavingInterface
{
    /** @var Builder_PostgreSql */
    protected $parent;

    /**
     * @inheritDoc
     */
    public function startSubHaving(): self
    {
        $this->query .= '(';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function endSubHaving(): self
    {
        $this->query .= ')';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function render(): ?array
    {
        if (empty($this->query)) {
            return null;
        }

        return [
            'having' => ' HAVING ' . $this->query,
            'values' => $this->values,
            'types' => $this->types
        ];
    }

    /**
     * Returns the query result (parent)
     *
     * @return DALInterface query result as a database object
     */
    public function getResult()
    {
        return $this->parent->getResult();
    }
}