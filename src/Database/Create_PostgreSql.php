<?php

namespace MiniatureHappiness\LegacyBundle\Database;

use MiniatureHappiness\LegacyBundle\Exceptions\DBException;
use MiniatureHappiness\LegacyBundle\Interfaces\CreateInterface;
use MiniatureHappiness\LegacyBundle\Interfaces\DALInterface;

/**
 * @deprecated
 */
class Create_PostgreSql implements CreateInterface
{

    /** @var string */
    private $query;

    /** @var array */
    private $createRows;

    /** @var array */
    private $createTypes;

    /** @var string */
    private $engine;

    /** @var string */
    private $dropTable;

    /** @var string */
    private $table;

    /** @var Builder_PostgreSql */
    protected $parent;

    public function __construct(Builder_PostgreSql $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @inheritDoc
     */
    public function reset(): self
    {
        $this->query = '';
        $this->createRows = [];
        $this->createTypes = [];
        $this->engine = '';
        $this->dropTable = '';
        $this->table = '';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setTable(string $table, bool $dropTable = false): self
    {
        if ($dropTable) {
            $this->dropTable = 'DROP TABLE IF EXISTS ' . DB_PREFIX . $table;
        }

        $this->query = "CREATE TABLE " . DB_PREFIX . $table . " (";
        $this->table = $table;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addRow(string $field, string $databaseType, int $length, string $defaultValue = '', bool $signed = false, bool $null = false, bool $autoIncrement = false): self
    {
        $signed = (($signed) ? ' SIGNED ' : ' UNSIGNED ');

        $defaultValue = $this->checkDefaultValue($defaultValue, $null);
        $null = $this->checkNull($null);
        $autoIncrement = (($autoIncrement) ? ' AUTO_INCREMENT' : '');
        $databaseType = strtoupper($databaseType);

        switch ($databaseType) {
            case 'VARCHAR':
                $this->createRows[$field] = $field . ' ' . $databaseType . '(' . $length . ') ' . $defaultValue . $null;
                break;

            case 'SMALLINT':
            case 'MEDIUMINT':
            case 'INT':
            case 'BIGINT':
                $this->createRows[$field] = $field . ' ' . $databaseType . '(' . $length . ') ' . $signed . ' ' . $defaultValue . $null . $autoIncrement;
                break;

            case 'DECIMAL':
                $this->createRows[$field] = $field . ' DECIMAL(10,0) ' . $signed . ' ' . $defaultValue . $null;
                break;

            default:
                $this->createRows[$field] = $field . ' ' . $databaseType . ' ' . $defaultValue . $null;
                break;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addEnum(string $field, array $values, string $defaultValue, bool $null = false): self
    {
        $valuesPre = [];
        foreach ($values as $value) {
            $valuesPre[] = "'" . $value . "'";
        }

        $defaultValue = $this->checkDefaultValue($defaultValue, $null);
        $null = $this->checkNull($null);

        $this->createRows[$field] = $field . ' ENUM(' . implode(',', $valuesPre) . ') ' . $defaultValue . $null;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addSet(string $field, array $values, string $defaultValue, bool $null = false): self
    {
        $valuesPre = [];
        foreach ($values as $value) {
            $valuesPre[] = "'" . $value . "'";
        }

        $defaultValue = $this->checkDefaultValue($defaultValue, $null);
        $null = $this->checkNull($null);

        $this->createRows[$field] = $field . ' SET(' . implode(',', $valuesPre) . ') ' . $defaultValue . $null;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addPrimary(string $field): self
    {
        if (!array_key_exists($field, $this->createRows)) {
            throw new DBException("Can not add primary key on unknown field $field.");
        }
        if (array_key_exists('primary', $this->createTypes)) {
            throw new DBException("Only one primary key pro table is allowed.");
        }

        $this->createTypes['primary'] = 'PRIMARY KEY (' . $field . ')';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addIndex(string $field): self
    {
        if (!array_key_exists($field, $this->createRows)) {
            throw new DBException("Can not add index key on unknown field $field.");
        }

        $this->createTypes[] = 'KEY ' . $this->table . '_' . $field . ' (' . $field . ')';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addUnique(string $field): self
    {
        if (!array_key_exists($field, $this->createRows)) {
            throw new DBException("Can not add unique key on unknown field $field.");
        }

        $this->createTypes[] = 'UNIQUE KEY ' . $this->table . '_' . $field . ' (' . $field . ')';

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addFullTextSearch(string $field): self
    {
        if (!array_key_exists($field, $this->createRows)) {
            throw new DBException("Can not add full text search on unknown field $field.");
        }
        if (stripos($this->createRows[$field], 'VARCHAR') === false && stripos($this->createRows[$field], 'TEXT') === false) {
            throw new DBException("Full text search can only be added on VARCHAR or TEXT fields.");
        }

        $this->createTypes[] = 'FULLTEXT KEY ' . $this->table . '_' . $field . ' (' . $field . ')';

        $this->engine = 'ENGINE=MyISAM';

        return $this;
    }

    private function checkDefaultValue(string $defaultValue, bool $null): string
    {
        if ($null && $defaultValue == "") {
            return ' DEFAULT NULL ';
        }
        if (!empty($defaultValue)) {
            return " DEFAULT '" . $defaultValue . "' ";
        }
        return '';
    }

    /**
     * Parses the null setting
     */
    private function checkNull(bool $null): string
    {
        if ($null) {
            return ' NULL ';
        }

        return ' NOT NULL ';
    }

    /**
     * @inheritDoc
     */
    public function getDropTable(): string
    {
        return $this->dropTable;
    }

    /**
     * @inheritDoc
     */
    public function render(): string
    {
        $this->query .= "\n" . implode(",\n", $this->createRows);
        if (count($this->createTypes) > 0) {
            $this->query .= ",\n";
            $this->query .= implode(",\n", $this->createTypes);
        }
        $this->query .= "\n)" . $this->engine;

        return $this->query;
    }

    /**
     * @inheritDoc
     */
    public function getResult(): DALInterface
    {
        return $this->parent->getResult();
    }
}