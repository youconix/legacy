<?php

namespace MiniatureHappiness\LegacyBundle\Traits;

use Psr\Log\LoggerInterface;

trait DeprecationTrait
{
    protected LoggerInterface $deprecatedLogger;

    public function setDeprecationLogger(LoggerInterface $logger): void
    {
        $this->deprecatedLogger = $logger;
    }

    private function triggerDeprecationError(string $alternative = ''): void
    {
        if (isset($_ENV['SYMFONY_DEPRECATIONS_HELPER']))
        {
            return;
        }

        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
        $caller = $backtrace[1]['function'] ?? '?';
        $callerClass = $backtrace[1]['class'] ?? '?';

        if ($alternative === '')
        {
            $message = sprintf('Function %s of class %s is deprecated.', $caller, $callerClass);
        }
        else {
            $message = sprintf('Function %s of class %s is deprecated. Use %s instead.', $caller, $callerClass, $alternative);
        }

        $this->deprecatedLogger->error($message);
    }
}