<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Listeners;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use MiniatureHappiness\LegacyBundle\LegacyMapper\LegacyMapRetriever;
use MiniatureHappiness\LegacyBundle\LegacyMapper\ServiceMapper;
use MiniatureHappiness\LegacyBundle\MemoryLoader;

/**
 * Hooks Symfony dependency injection into the old Scripthulp Framework
 */
class BridgeHookListener
{
    private LegacyMapRetriever $retriever;
    private ServiceMapper $legacyLocator;
    private MemoryLoader $memory;
    private string $cacheDirectory;

    public function __construct(
        LegacyMapRetriever $retriever,
        ServiceMapper $legacyLocator,
        MemoryLoader $memory,
        string $cacheDirectory
    ) {
        $this->retriever = $retriever;
        $this->legacyLocator = $legacyLocator;
        $this->memory = $memory;
        $this->cacheDirectory = $cacheDirectory;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $legacyMap = $this->retriever->get();
        $legacyMap->setServiceLocator($this->legacyLocator);
        $legacyMap->loadMap($this->cacheDirectory);

        \Memory::getInstance($this->memory);
    }
}