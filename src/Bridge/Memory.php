<?php

use MiniatureHappiness\CoreBundle\Helpers\AbstractHelper;
use MiniatureHappiness\LegacyBundle\Exceptions\MemoryException;
use MiniatureHappiness\LegacyBundle\Exceptions\NullPointerException;
use MiniatureHappiness\LegacyBundle\Exceptions\TypeException;
use MiniatureHappiness\LegacyBundle\MemoryLoader as BaseMemory;
use MiniatureHappiness\LegacyBundle\Services\AbstractService;
use Psr\Container\ContainerExceptionInterface;

/**
 * @deprecated
 */
class Memory
{
    private static BaseMemory $base;
    private static ?Memory $instance = null;

    /**
     * @throws MemoryException
     * @deprecated
     */
    public static function getInstance(BaseMemory $base = null): self
    {
        if (self::$instance === null) {
            if ($base === null) {
                throw new MemoryException('Need the legacy Memory for Memory startup');
            }

            self::$instance = new Memory();
            self::$base = $base;
        }

        return self::$instance;
    }

    /**
     * @deprecated
     */
    public function __destruct()
    {
        self::$base->reset();
    }

    /**
     * Starts the framework in testing mode.
     * DO NOT USE THIS IN PRODUCTION
     *
     * @deprecated
     */
    public static function setTesting(): void
    {
        self::$base->setTesting();
    }

    /**
     * @deprecated
     */
    public static function isDebug(): bool
    {
        return self::$base->isDebug();
    }

    /**
     * @throws RuntimeException
     * @deprecated
     */
    public static function startUp(): void
    {
        throw new RuntimeException('Can not call start up directly');
    }

    /**
     * Returns the used protocol
     *
     * @deprecated
     */
    public static function getProtocol(): string
    {
        return self::$base->getProtocol();
    }

    /**
     * Returns the current page
     *
     * @deprecated
     */
    public static function getPage(): string
    {
        return self::$base->getPage();
    }

    /**
     * Returns the current URL
     *
     * @deprecated
     */
    public static function getURL(): string
    {
        return self::$base->getURL();
    }

    /**
     * Checks if ajax-mode is active
     *
     * @deprecated
     */
    public static function isAjax(): bool
    {
        return self::$base->isAjax();
    }

    /**
     * Sets the framework in ajax-mode
     *
     * @deprecated
     */
    public static function setAjax(bool $value = true): void
    {
        self::$base->setAjax($value);
    }

    /**
     * Sets whether the Memory class should automatically render the page
     *
     * @deprecated
     */
    public static function autoRender(bool $value): void
    {
        self::$base->autoRender($value);
    }

    /**
     * Checks if testing-mode is active
     *
     * @deprecated
     */
    public static function isTesting(): bool
    {
        return self::$base->isTesting();
    }

    /**
     * Returns the base directory
     *
     * @deprecated
     */
    public static function getBase(): string
    {
        return self::$base->getBase();
    }

    /**
     * Ensures that the given class is loaded
     *
     * @throws MemoryException        If the class does not exist in include/class/
     * @deprecated
     */
    public static function ensureClass(string $className): void
    {
        self::$base->ensureClass($className);
    }

    /**
     * Ensures that the given interface is loaded
     *
     * @throws MemoryException            If the interface does not exist in include/interface/
     * @deprecated
     */
    public static function ensureInterface(string $interface): void
    {
        self::$base->ensureInterface($interface);
    }

    /**
     * API for checking or a helper exists
     *
     * @deprecated
     */
    public static function isHelper(string $className): bool
    {
        return self::$base->isHelper($className);
    }

    /**
     * Loads the requested helper
     *
     * @return AbstractHelper|object
     * @throws  Exception|MemoryException If the requested helper does not exist
     * @deprecated
     */
    public static function helpers(string $className): mixed
    {
        return self::$base->helpers($className);
    }

    /**
     * API for checking or a service exists
     *
     * @deprecated
     */
    public static function isService(string $className): bool
    {
        return self::$base->isService($className);
    }

    /**
     * Loads the requested service
     *
     * @return AbstractService|object
     * @throws  MemoryException|ContainerExceptionInterface If the requested service does not exist
     * @deprecated
     */
    public static function services(string $className): mixed
    {
        return self::$base->services($className);
    }

    /**
     * API for checking or a model exists
     *
     * @deprecated
     */
    public static function isModel(string $className): bool
    {
        return self::$base->isModel($className);
    }

    /**
     * Loads the requested model
     *
     * @return object
     * @throws  Exception If the requested model does not exist
     * @deprecated
     */
    public static function models(string $className): mixed
    {
        return self::$base->models($className);
    }

    /**
     * Checks if a helper, service or model is loaded
     *
     * @deprecated
     */
    public static function isLoaded(string $type, string $name): bool
    {
        return self::$base->isLoaded($type, $name);
    }

    /**
     * API for checking the type of the given value.
     * Kills the program when the type of the variable is not the requested type
     *
     * @throws  NullPointerException if $value is null and $s_type is not 'null'.
     * @throws  TypeException if $value has the wrong type.
     * @deprecated
     */
    public static function type(string $type, mixed $value): void
    {
        self::$base->type($type, $value);
    }

    /**
     * Removes a value from the global memory
     *
     * @param string $type The type (Service|Model|Helper)
     * @deprecated
     */
    public static function delete(string $type, string $name): void
    {
        self::$base->delete($type, $name);
    }

    /**
     * Stops the framework and writes all the content to the screen
     *
     * @deprecated
     */
    public static function endProgram(): void
    {
        self::$base->endProgram();
    }

    /**
     * @deprecated
     */
    public static function reset(): void
    {
        self::$base->reset();
    }

    /**
     * Transforms a relative url into a absolute url (site domain only)
     *
     * @deprecated
     */
    public static function generateUrl(string $url): string
    {
        return self::$base->generateUrl($url);
    }
}