<?php

use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * @deprecated
 */
class Service extends \MiniatureHappiness\LegacyBundle\Services\AbstractService
{
    use DeprecationTrait;
}