<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle;

use MiniatureHappiness\LegacyBundle\LegacyMapper\LegacyMap;
use MiniatureHappiness\LegacyBundle\LegacyMapper\LegacyMapRetriever;
use MiniatureHappiness\LegacyBundle\Services\AbstractService;
use MiniatureHappiness\LegacyBundle\Exceptions\MemoryException;
use MiniatureHappiness\LegacyBundle\Exceptions\NullPointerException;
use MiniatureHappiness\LegacyBundle\Exceptions\TypeException;
use Psr\Container\ContainerExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @deprecated
 */
class MemoryLoader
{
    private LegacyMap $legacyMap;
    private Filesystem $fileHandler;

    private bool $ajax;
    private ?array $memory = null;
    private bool $autoRender = true;
    private bool $isDebug;
    private string $base;
    private string $page;
    private string $protocol;
    private bool $testing = false;

    private string $servicePath;
    private string $modelPath;
    private string $helperPath;
    private string $classPath;
    private string $interfacePath;

    public function __construct(
        LegacyMapRetriever $retriever,
        Filesystem $fileHandler,
        string $rootDirectory,
        string $sqlPrefix,
        string $protocol,
        bool $overrideSSL,
        bool $isDebug
    ) {
        $this->legacyMap = $retriever->get();
        $this->fileHandler = $fileHandler;

        $this->protocol = $protocol;
        $this->isDebug = $isDebug;

        if ($overrideSSL) {
            $this->protocol = 'https';
        }

        $this->startUp($rootDirectory, $sqlPrefix);
    }

    /**
     * @deprecated
     */
    public function __destruct()
    {
        $this->reset();
    }

    /**
     * Starts the framework in testing mode.
     * DO NOT USE THIS IN PRODUCTION
     *
     * @deprecated
     */
    public function setTesting(): void
    {
        if (!defined('DEBUG')) {
            define('DEBUG', 'true');
        }

        if (!defined('PROCESS')) {
            define('PROCESS', 1);
        }

        $this->testing = true;
    }

    /**
     * @deprecated
     */
    public function isDebug(): bool
    {
        return $this->isDebug;
    }

    /**
     * Starts the framework
     */
    private function startUp(string $rootDirectory, string $sqlPrefix): void {
        if (!is_null($this->memory)) {
            return;
        }

        if (!defined('DATA_DIR')) {
            if ($this->testing) {
                define('DATA_DIR', $rootDirectory . '/admin/data/tests/');
            } else {
                define('DATA_DIR', $rootDirectory . '/admin/data/');
            }
        }

        /* Prepare cache */
        $this->memory = [
            'service' => [],
            'model' => [],
            'helper' => [],
        ];
        $this->ajax = false;

        if (!defined('NIV')) {
            define('NIV', $rootDirectory);
        }

        $this->loadPaths($rootDirectory);

        if (!defined('DB_PREFIX')) {
            define('DB_PREFIX', $sqlPrefix);
        }

        $base = $rootDirectory;
        if (!str_starts_with($base, '/')) {
            $this->base = '/' . $base;
        } else {
            $this->base = $base;
        }

        if (!defined('BASE')) {
            define('BASE', NIV);
        }
        if (!defined('ROOT')) {
            define('ROOT', $rootDirectory);
        }

        /* Get page */
        $page = $_SERVER['SCRIPT_NAME'];

        while (str_starts_with($page, '/')) {
            $page = substr($page, 1);
        }
        $this->page = $page;

        if ($base == '/') {
            $this->page = substr($this->page, 1);
        } elseif (stripos($page, $base) !== false) {
            $this->page = substr($this->page, strlen($base));
        }

        $this->protocol = sprintf('%s://', $this->protocol);

        if (!defined('DEBUG')) {
            define('DEBUG', $this->isDebug);
        }

        if (defined('DEBUG')) {
            ini_set('display_errors', 'on');
        } else {
            ini_set('display_errors', 'off');
        }
    }

    private function loadPaths(string $rootDirectory): void
    {
        $this->servicePath = $rootDirectory . 'include/services';
        $this->modelPath = $rootDirectory . 'include/models';
        $this->helperPath = $rootDirectory . 'include/helpers';
        $this->classPath = $rootDirectory . 'include/class';
        $this->interfacePath = $rootDirectory . 'include/interface';

        if (file_exists($rootDirectory . 'include/functions.php')) {
            require_once($rootDirectory . 'include/functions.php');
        }
    }

    /**
     * Returns the used protocol
     *
     * @deprecated
     */
    public function getProtocol(): string
    {
        return $this->protocol;
    }

    /**
     * Returns the current page
     *
     * @deprecated
     */
    public function getPage(): string
    {
        return $this->page;
    }

    /**
     * Returns the current URL
     *
     * @deprecated
     */
    public function getURL(): string
    {
        $url = $_SERVER['REQUEST_URI'];
        if (str_starts_with($url, $this->getBase())) {
            $url = substr($url, strlen($this->getBase()));
        }
        return ltrim($url, '/');
    }

    /**
     * Checks if ajax-mode is active
     *
     * @deprecated
     */
    public function isAjax(): bool
    {
        return $this->ajax;
    }

    /**
     * Sets the framework in ajax-mode
     *
     * @deprecated
     */
    public function setAjax(bool $value = true): void
    {
        $this->ajax = $value;
    }

    /**
     * Sets whether the Memory class should automatically render the page
     *
     * @deprecated
     */
    public function autoRender(bool $value): void
    {
        $this->autoRender = $value;
    }

    /**
     * Checks if testing-mode is active
     *
     * @deprecated
     */
    public function isTesting(): bool
    {
        return $this->testing;
    }

    /**
     * Returns the base directory
     *
     * @deprecated
     */
    public function getBase(): string
    {
        return $this->base;
    }

    /**
     * Ensures that the given class is loaded
     *
     * @throws MemoryException        If the class does not exist in include/class/
     * @deprecated
     */
    public function ensureClass(string $className): void
    {
        $fileName = sprintf('%s/%s.inc.php', $this->classPath, $className);

        if (!class_exists($className)) {
            if (!$this->fileHandler->exists($fileName)) {
                throw new MemoryException(sprintf('Can not find class %s.', $className));
            }

            require_once($fileName);
        }
    }

    /**
     * Ensures that the given interface is loaded
     *
     * @throws MemoryException            If the interface does not exist in include/interface/
     * @deprecated
     */
    public function ensureInterface(string $interface): void
    {
        $fileName = sprintf('%s/%s.inc.php', $this->interfacePath, $interface);

        if (!interface_exists($interface)) {
            if (!$this->fileHandler->exists($fileName)) {
                throw new MemoryException(sprintf('Can not find interface %s.', $interface));
            }

            require_once($fileName);
        }
    }

    /**
     * API for checking or a helper exists
     *
     * @deprecated
     */
    public function isHelper(string $name): bool
    {
        return $this->classExist('helper', 'Helper_', $this->helperPath, $name);
    }

    /**
     * Loads the requested helper
     *
     * @throws  MemoryException|ContainerExceptionInterface If the requested helper does not exist
     * @deprecated
     */
    public function helpers(string $name): object
    {
        return $this->loadClass(
            'helper',
            'Helper_',
            $this->helperPath,
            $name,
            'Can not find helper %s in include/helpers.'
        );
    }

    /**
     * API for checking or a service exists
     *
     * @deprecated
     */
    public function isService(string $name): bool
    {
        return $this->classExist('service', 'Service_', $this->servicePath, $name);
    }

    /**
     * Loads the requested service
     *
     * @return AbstractService|object
     * @throws  MemoryException|ContainerExceptionInterface If the requested service does not exist
     * @deprecated
     */
    public function services(string $name): object
    {
        return $this->loadClass(
            'service',
            'Service_',
            $this->servicePath,
            $name,
            'Can not find service %s in include/services.'
        );
    }

    /**
     * API for checking or a model exists
     *
     * @deprecated
     */
    public function isModel(string $name): bool
    {
        return $this->classExist('model', 'Model_', $this->modelPath, $name);
    }

    /**
     * Loads the requested model
     *
     * @throws  MemoryException|ContainerExceptionInterface If the requested model does not exist
     * @deprecated
     */
    public function models(string $model): object
    {
        return $this->loadClass(
            'model',
            'Model_',
            $this->modelPath,
            $model,
            'Can not find model %s in include/models'
        );
    }

    /**
     * Checks if a helper, service or model is loaded
     *
     * @deprecated
     */
    public function isLoaded(string $type, string $name): bool
    {
        $type = strtolower($type);
        $name = ucfirst($name);

        if (array_key_exists($name, $this->memory[$type])) {
            return true;
        }

        return false;
    }

    /**
     * API for checking the type of the given value.
     * Kills the program when the type of the variable is not the requested type
     *
     * @throws  NullPointerException if $value is null and $type is not 'null'.
     * @throws  TypeException if $value has the wrong type.
     * @deprecated
     */
    public static function type(string $type, mixed $value): void
    {
        $oke = true;

        if ($type != 'null' && is_null($value)) {
            throw new NullPointerException('Null found when expected ' . $type . '.');
        }

        switch ($type) {
            case 'bool':
                if (!is_bool($value)) {
                    $oke = false;
                }
                break;

            case 'int':
                if (!is_int($value)) {
                    $oke = false;
                }
                break;

            case 'float':
                if (!is_float($value)) {
                    $oke = false;
                }
                break;

            case 'string':
                if (!is_string($value)) {
                    $oke = false;
                }
                break;

            case 'object':
                if (!is_object($value)) {
                    $oke = false;
                }
                break;

            case 'array':
                if (!is_array($value)) {
                    $oke = false;
                }
                break;

            case 'null' :
                if (!is_null($value)) {
                    $oke = false;
                }
                break;
        }

        if (!$oke) {
            throw new TypeException(
                'Wrong datatype found. Expected ' . $type . ' but found ' . gettype($value) . '.'
            );
        }
    }

    /**
     * Removes a value from the global memory
     *
     * @deprecated
     */
    public function delete(string $type, string $name): void
    {
        $type = strtolower($type);
        $name = ucfirst(strtolower($name));

        if ($this->isLoaded($type, $name)) {
            unset($this->memory[$type][$name]);
        } else {
            throw new MemoryException("Trying to delete " . $type . " " . $name . " that does not exist");
        }
    }

    /**
     * Stops the framework and writes all the content to the screen
     *
     * @deprecated
     */
    public function endProgram(): void
    {
        if (!$this->autoRender) {
            return;
        }

        // Patch: when calling endProgram() from a destructor, the cwd will change to / (with Apache anyways).
        // Sadly we indeed call it from a destructor, so change it back here.
        // http://php.net/manual/en/language.oop5.decon.php
        if ((getcwd() == '/' || getcwd() == '/usr/local/bin') && isset($_SERVER['DOCUMENT_ROOT'])) {
            chdir($_SERVER['DOCUMENT_ROOT']);
        }

        if ($this->isLoadedInMemory('service', 'Template')) {
            $template = $this->getFromMemory('service', 'Template');
            $template->printToScreen();
        }
    }

    /**
     * @deprecated
     */
    public function reset(): void
    {
        $this->ajax = false;
        $this->memory = null;
        $this->base = '';
        $this->page = '';
        $this->protocol = '';
        $this->testing = false;

        $this->servicePath = '';
        $this->modelPath = '';
        $this->helperPath = '';
        $this->classPath = '';
        $this->interfacePath = '';
    }

    /**
     * Transforms a relative url into a absolute url (site domain only)
     *
     * @deprecated
     */
    public function generateUrl(string $url): string
    {
        $url = str_replace('../', '', $url);
        $url = str_replace('./', '', $url);
        return $this->base . $url;
    }

    /**
     * Checks or the given service or model already is loaded
     */
    private function isLoadedInMemory(string $type, string $name): bool
    {
        if (array_key_exists($name, $this->memory[$type])) {
            return true;
        }

        return false;
    }

    /**
     * Returns the requested service or model from the memory
     */
    private function getFromMemory(string $type, string $name): object
    {
        return $this->memory[$type][$name];
    }

    /**
     * Saves the given service or model in the memory
     */
    private function setInMemory(string $type, string $name, object $value): void
    {
        $this->memory[$type][$name] = $value;
    }

    private function classExist(string $type, string $prefix, string $path, string $name): bool
    {
        $name = ucfirst($name);
        $className = str_starts_with($name, $prefix) ? $name : sprintf('%s%s', $prefix, $name);

        /* Check or class is in the global memory */
        if ($this->isLoadedInMemory($type, $name) || $this->canResolve($className)) {
            return true;
        }

        /* Check or the file exists */
        $fileName = sprintf('%s/%s.inc.php', $path, $className);
        return $this->fileHandler->exists($fileName);
    }

    /**
     * @throws MemoryException if the class does not exist
     */
    private function loadClass(string $type, string $prefix, string $path, string $name, string $errorMessage): object
    {
        $name = ucfirst($name);
        $className = str_starts_with($name, $prefix) ? $name : sprintf('%s%s', $prefix, $name);

        if ($this->isLoadedInMemory($type, $name)) {
            return $this->getFromMemory($type, $name);
        }

        if ($this->canResolve($className)) {
            $class = $this->resolve($className);
        } else {
            $fileName = sprintf('%s/%s.inc.php', $path, $name);

            if (!file_exists($fileName)) {
                throw new MemoryException(sprintf($errorMessage, $name));
            }

            require_once($fileName);
            $class = new $className();
        }

        $this->setInMemory($type, $name, $class);

        return $class;
    }

    private function canResolve(string $name): bool
    {
        return $this->legacyMap->find($name) !== null;
    }

    /**
     * @throws MemoryException
     */
    private function resolve(string $name): object
    {
        return $this->legacyMap->loadByClassName($name);
    }
}
