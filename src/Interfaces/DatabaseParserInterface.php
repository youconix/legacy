<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

/**
 * @deprecated
 */
interface DatabaseParserInterface
{
  public function createAddTables($document);

  public function updateTables($document);
}