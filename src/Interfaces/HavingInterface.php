<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

/**
 * @deprecated
 */
interface HavingInterface extends ConditionsInterface
{
  /**
   * Starts a sub having part
   *
   * @deprecated
   */
  public function startSubHaving(): self;

  /**
   * Ends a sub having part
   *
   * @deprecated
   */
  public function endSubHaving(): self;

  /**
   * Renders the having
   *
   * @deprecated
   */
  public function render(): ?array;
}