<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

use DateTime;
use Exception;
use MiniatureHappiness\CoreBundle\Exceptions\IOException;
use MiniatureHappiness\CoreBundle\Exceptions\XMLException;

/**
 * @deprecated
 */
interface LanguageInterface
{
    /**
     * Collects the installed languages
     *
     * @deprecated
     */
    public function getLanguages(): array;

    /**
     * Sets the language
     *
     * @throws IOException when the language code does not exist
     * @deprecated
     */
    public function setLanguage(string $language): void;

    /**
     * Returns the set language
     *
     * @deprecated
     */
    public function getLanguage(): string;

    /**
     * Set the given language, keeping track of the current language to switch back later with endEnsure().
     *
     * @throws IOException when the language code does not exist
     * @deprecated
     */
    public function ensure(string $newLang): void;

    /**
     * Switch back to the language that was switch from by the corresponding ensure() call.
     *
     * @throws Exception when the ensure stack is empty
     * @deprecated
     */
    public function endEnsure(): void;

    /**
     * Returns the set encoding
     *
     * @deprecated
     */
    public function getEncoding(): string;

    /**
     * Gives the asked part of the loaded file
     *
     * @throws  XMLException when the path does not exist
     * @deprecated
     */
    public function getEncoded(string $path, array $variables = null): string;

    /**
     * Gives the asked part of the loaded file, with html entities decoded
     * @see get()
     *
     * @deprecated
     */
    public function getDecoded(string $path, array $variables = null): string;

    /**
     * @throws XMLException when the path does not exist
     * @deprecated
     */
    public function get(string $path, array $variables = null): string;

    /**
     * Get the specified entry from the specified language file
     * @param string $language The language to use
     * @see get()
     *
     * @deprecated
     */
    public function getEnsured(string $language, string $path, array $variables = null): string;

    /**
     * Changes the language-values with the given values
     *
     * @param array $values
     * @deprecated
     */
    public function insert(string $text, array $fields, $values): string;

    /**
     * Gives the asked part of a language sub-file
     *
     * @throws  XMLException when the path does not exist
     * @deprecated
     */
    public function getSub(string $sub, string $path, string $lang = ''): string;

    /**
     * Return a language sub-file
     *
     * @return  object      The XML object of the requested sub-lang
     * @throws  XMLException when the path does not exist
     * @deprecated
     */
    public function sub(string $sub, string $lang = '');

    /**
     * Gives the asked part of a language sub-file
     *
     * @deprecated
     */
    public function date(string $format, int|DateTime $datetime): string;

    /**
     * @throws XMLException when the path does not exist
     * @deprecated
     */
    public function insertPath(string $path, string|array $keys, string|array $variables = null): string;
}