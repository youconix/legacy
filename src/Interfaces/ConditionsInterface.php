<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

/**
 * @deprecated
 */
interface ConditionsInterface
{
    /**
     * Resets the class
     *
     * @deprecated
     */
    public function reset(): self;

    /**
     * Binds a string value
     *
     * @param string $type The type (AND|OR)
     * @param string $key (=|<>|<|>|LIKE|IN|BETWEEN).
     * @deprecated
     */
    public function bindString(string $field, string $value, string $type = 'AND', string $key = '='): self;

    /**
     * Binds an integer value
     *
     * @param string $type The type (AND|OR)
     * @param string $key (=|<>|<|>|LIKE|IN|BETWEEN)
     * @deprecated
     */
    public function bindInt(string $field, int $value, string $type = 'AND', string $key = '='): self;

    /**
     * Binds a float value
     *
     * @param string $type The type (AND|OR)
     * @param string $key (=|<>|<|>|LIKE|IN|BETWEEN)
     * @deprecated
     */
    public function bindFloat(string $field, float $value, string $type = 'AND', string $key = '='): self;


    /**
     * Binds a binary value
     *
     * @param binary $value The value
     * @param string $type The type (AND|OR)
     * @param string $key (=|<>|<|>|LIKE|IN|BETWEEN)
     * @deprecated
     */
    public function bindBlob(string $field, $value, string $type = 'AND', string $key = '='): self;

    /**
     * Adds a literal statement
     *
     * @param string $type The type (AND|OR)
     * @param string $key (=|<>|<|>|LIKE|IN|BETWEEN)
     * @deprecated
     */
    public function bindLiteral(string $field, string $statement, string $type = 'AND', string $key = '='): self;

    /**
     * Returns the query result (parent)
     *
     * @deprecated
     */
    public function getResult(): DALInterface;
}