<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

/**
 * @deprecated
 *
 * Use the Symfony request (headers) instead
 */
interface HeadersInterface
{
    /**
     * Clears the headers
     */
    public function clear(): self;

    /**
     * Sets the given content type
     */
    public function contentType(string $contentType): self;

    /**
     * Sets the JavaScript content type
     */
    public function setJavascript(): self;

    /**
     * Sets the CSS content type
     */
    public function setCSS(): self;

    /**
     * Sets the XML content type
     */
    public function setXML(): self;

    /**
     * Sets the last modified header
     *
     * Time as timestamp
     */
    public function modified(int $modified): self;

    /**
     * Sets the cache time in seconds, -1 for no cache
     */
    public function cache(int $cache): self;

    /**
     * Sets the content length in bytes
     */
    public function contentLength(int $length): self;

    /**
     * Force downloads a file
     * Program wil halt
     */
    public function forceDownloadFile(string $file, string $contentType): void;

    /**
     * Force downloads the given content
     * Program wil halt
     */
    public function forceDownloadContent(string $content, string $contentType, string $name): void;

    /**
     * Sets a header
     */
    public function setHeader(string $key, string $content): self;

    /**
     * Sends the 301 redirect header
     * Program will halt
     */
    public function redirect(string $location): void;

    /**
     * Sends the 301 redirect header for the given path
     * Program will halt
     */
    public function redirectPath(string $path, array $parameters = []): void;

    /**
     * Returns if a force download was executed
     */
    public function isForceDownload(): bool;

    /**
     * Returns if a redirect was executed
     */
    public function isRedirect(): bool;

    /**
     * Returns the headers
     */
    public function getHeaders(): array;

    /**
     * Returns if the template should be skipped
     */
    public function skipTemplate(): bool;

    /**
     * Imports the given headers
     */
    public function importHeaders(array $headers): self;

    /**
     * Sends the cached headers to the client
     */
    public function printHeaders(): self;
}
