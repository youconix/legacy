<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

use MiniatureHappiness\LegacyBundle\Exceptions\DBException;

/**
 * @deprecated
 */
interface CreateInterface
{
    /**
     * @deprecated
     */
    public function reset(): self;

    /**
     * Creates a table
     *
     * @deprecated
     */
    public function setTable(string $table, bool $dropTable = false): self;

    /**
     * Adds a field
     *
     * @deprecated
     */
    public function addRow(string $field, string $databaseType, int $length, string $defaultValue = '', bool $signed = false, bool $null = false, bool $autoIncrement = false): self;

    /**
     * Adds an enum field
     *
     * @deprecated
     */
    public function addEnum(string $field, array $values, string $defaultValue, bool $null = false): self;

    /**
     * Adds a set field
     *
     * @deprecated
     */
    public function addSet(string $field, array $values, string $defaultValue, bool $null = false): self;

    /**
     * Adds a primary key to the given field
     *
     * @throws DBException If the field is unknown or if the primary key is already set
     * @deprecated
     */
    public function addPrimary(string $field): self;

    /**
     * Adds a index to the given field
     *
     * @throws DBException If the field is unknown
     * @deprecated
     */
    public function addIndex(string $field): self;

    /**
     * Sets the given fields as unique
     *
     * @throws DBException If the field is unknown
     * @deprecated
     */
    public function addUnique(string $field): self;

    /**
     * Sets full text search on the given field
     *
     * @throws DBException If the field is unknown
     * @throws DBException If the field type is not VARCHAR and not TEXT.
     * @deprecated
     */
    public function addFullTextSearch(string $field): self;

    /**
     * Returns the drop table setting
     *
     * @deprecated
     */
    public function getDropTable(): string;

    /**
     * Creates the query
     *
     * @deprecated
     */
    public function render(): string;

    /**
     * Returns the query result (parent)
     *
     * @deprecated
     */
    public function getResult(): DALInterface;
}