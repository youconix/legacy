<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

use MiniatureHappiness\LegacyBundle\Exceptions\ConfigException;

/**
 * @deprecated
 */
interface SettingsInterface
{

  const SSL_DISABLED = 0;

  const SSL_LOGIN = 1;

  const SSL_ALL = 2;

  const REMOTE = 'https://framework.youconix.nl/2/';

  const MAJOR = 2;

  /**
   * Checks if the config value exists
   *
   * @param string $path
   * @return bool
   */
  public function exists(string $path): bool;

  /**
   * Returns the config value
   *
   * @param string $path
   * @return string
   * @throws ConfigException
   */
  public function get(string $path): string;
}