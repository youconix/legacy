<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

use Psr\Log\LoggerInterface;

interface DeprecatedInterface {
    public function setDeprecationLogger(LoggerInterface $logger): void;
}