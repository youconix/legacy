<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Interfaces;

use MiniatureHappiness\CoreBundle\Exceptions\AuthenticationException;
use MiniatureHappiness\CoreBundle\Exceptions\IOException;

/**
 * @deprecated
 */
interface SessionInterface
{
    public const FORBIDDEN = -1;
    // Stil here for backwards compatibility
    public const ANONYMOUS = -1;
    public const USER = 0;
    public const MODERATOR = 1;
    public const ADMIN = 2;
    public const FORBIDDEN_COLOR = 'grey';
    // Stil here for backwards compatibility
    public const ANONYMOUS_COLOR = 'grey';
    public const USER_COLOR = 'black';
    public const MODERATOR_COLOR = 'green';
    public const ADMIN_COLOR = 'red';

    /**
     * Sets the session with the given name and content
     *
     * @deprecated
     */
    public function set(string $sessionName, string $sessionData): self;

    /**
     * Deletes the session with the given name
     *
     * @throws IOException if the session does not exist
     * @deprecated
     */
    public function delete(string $sessionName): self;

    /**
     * Collects the content of the given session
     *
     * @throws IOException if the session does not exist
     * @deprecated
     */
    public function get(string $sessionName) : string;

    /**
     * Checks or the given session exists
     *
     * @deprecated
     */
    public function exists(string $sessionName): bool;

    /**
     * Destroys all sessions currently set
     *
     * @deprecated
     */
    public function destroy();

    /**
     * @deprecated
     */
    public function renew(string $sessionName): self;

    /**
     * Logs in the user and sets the login-session
     *
     * @deprecated
     */
    public function setLogin(int $userId, int $lastLogin, bool $autoLogin = false): self;

    /**
     * @deprecated
     */
    public function destroyLogin(): self;

    /**
     * @deprecated
     * @throws AuthenticationException if the user is not logged in
     */
    public function checkLogin(): self;

    /**
     * @deprecated
     */
    public function redirectToLogin(string $route): void;

    /**
     * @deprecated
     */
    public function check2fa(bool $redirect = true): void;

    /**
     * Register a two factor authentication login
     *
     * @deprecated
     */
    public function set2fa(): self;

    /**
     * Enable auto-login
     *
     * @deprecated
     */
    public function setAutoLogin(): self;

    /**
     * Check whether auto-login is enabled. If so, perform auto-login.
     *
     * @return bool True if a valid autologin was found and applied, false otherwise
     * @deprecated
     */
    public function checkAutoLogin(): bool;

    /**
     * Disable auto-login
     *
     * @deprecated
     */
    public function unsetAutoLogin(): self;

    /**
     * Returns the visitors browser fingerprint
     *
     * @deprecated
     */
    public function getFingerprint(bool $bindToIp = false): string;
}
