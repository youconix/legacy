<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

/**
 * @deprecated
 */
interface LoggerInterface extends \Psr\Log\LoggerInterface
{
}
