<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Interfaces;

use Exception;
use MiniatureHappiness\CoreBundle\Exceptions\IOException;
use MiniatureHappiness\LegacyBundle\Exceptions\TemplateException;

/**
 * @deprecated
 */
interface OutputInterface
{
    /**
     * Loads the given view into the parser
     *
     * @param string $view
     *            The view relative to the template-directory if it starts with a slash (/),
     *            or relative to the current page template directory if it does not start with a slash.
     * @throws TemplateException if the view does not exist
     * @throws IOException if the view is not readable
     * @deprecated
     */
    public function loadView(string $view): void;

    /**
     * Sets the given value in the template on the given key
     *
     * This function has two distinct interfaces:
     * Single value:
     * @param string $key The key in template
     * @param string/CoreHtmlItem $s_value The value to write in the template
     *
     * Multiple values:
     * @param array array(key=>value)
     *
     * @throws TemplateException if no template is loaded yet
     * @throws Exception if $s_value is not a string and not a subclass of CoreHtmlItem
     *
     * @deprecated
     */
    public function set(string $key, string $value): void;

    /**
     * Append a value to an already present array.
     * Useful for e.g. displaying multiple rows.
     *
     * @throws Exception
     *
     * @deprecated
     */
    public function append(string $key, mixed $value): void;

    /**
     * Builds the page
     *
     * @deprecated
     */
    public function buildPage(): string;
}