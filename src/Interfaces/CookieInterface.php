<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

use Exception;

/**
 * @deprecated
 */
interface CookieInterface
{
  /**
   * Deletes the cookie with the given name and domain
   *
   * @throws Exception if the cookie does not exist
   * @deprecated
   */
  public function delete(string $cookieName, string $domain): void;

  /**
   * Sets the cookie with the given name and data
   *
   * @return $this
   * @deprecated
   */
  public function set(string $cookieName, string $cookieData, string $domain, string $url = "", int $secure = 0): static;

  /**
   * Receives the content from the cookie with the given name
   *
   * @throws Exception if the cookie does not exist
   * @deprecated
   */
  public function get(string $cookieName): string;

  /**
   * Checks if the given cookie exists
   * @deprecated
   */
  public function exists(string $cookieName): bool;
}