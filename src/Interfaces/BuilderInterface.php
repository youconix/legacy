<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

use Exception;
use MiniatureHappiness\LegacyBundle\Exceptions\DBException;

/**
 * @deprecated
 */
interface BuilderInterface
{
  public function __construct(DALInterface $dal);

  /**
   * Returns if the object should be treated as singleton
   *
   * @deprecated
   */
  public static function isSingleton(): bool;

  /**
   * Shows the tables in the current database
   *
   * @deprecated
   */
  public function showTables(): self;

  /**
   * Shows the databases that the user has access to
   *
   * @deprecated
   */
  public function showDatabases(): self;

  /**
   * Creates a select statement
   * Field names are separated with a comma
   *
   * @deprecated
   */
  public function select(string $table, string $fieldNames): self;

  /**
   * Creates a insert statement
   *
   * @deprecated
   */
  public function insert(string $table, bool $ignoreErrors = false): self;

  /**
   * Creates a update statement
   *
   * @deprecated
   */
  public function update(string $table): self;

  /**
   * Creates a delete statement
   *
   * @deprecated
   */
  public function delete(string $table): self;

  /**
   * Updates the given record with the unique field or inserts a new one if it does not exist
   *
   * @deprecated
   */
  public function upsert(string $table, string $unique): self;

  /**
   * Binds a string value
   *
   * @deprecated
   */
  public function bindString(string $key, string $value): self;

  /**
   * Binds an integer value
   *
   * @deprecated
   */
  public function bindInt(string $key, int $value): self;

  /**
   * Binds a float value
   *
   * @deprecated
   */
  public function bindFloat(string $key, float $value): self;

  /**
   * Binds a binary value
   *
   * @param binary $value
   * @deprecated
   */
  public function bindBlob(string $key, $value): self;

  /**
   * Adds a literal statement
   *
   * @deprecated
   */
  public function bindLiteral(string $key, string $statement): self;

  /**
   * Returns the create table generation class
   *
   * @deprecated
   */
  public function getCreate(string $table, bool $dropTable): CreateInterface;

  /**
   * Adds a inner join between 2 tables
   *
   * @deprecated
   */
  public function innerJoin(string $table, string $field1, string $field2): self;

  /**
   * Adds an outer join between 2 tables
   *
   * @deprecated
   */
  public function outerJoin(string $table, string $field1, string $field2): self;

  /**
   * Adds a left join between 2 tables
   *
   * @deprecated
   */
  public function leftJoin(string $table, string $field1, string $field2): self;

  /**
   * Adds a right join between 2 tables
   *
   * @deprecated
   */
  public function rightJoin(string $table, string $field1, string $field2): self;

  /**
   * Returns the where generation class
   *
   * @deprecated
   */
  public function getWhere(): WhereInterface;

  /**
   * Adds a limitation to the query statement
   * Only works on select, update and delete statements
   *
   * @deprecated
   */
  public function limit(int $limit, int $offset = 0): self;

  /**
   * Groups the results by the given field
   *
   * @deprecated
   */
  public function group(string $field): self;

  /**
   * Returns the having generation class
   *
   * @deprecated
   */
  public function getHaving(): HavingInterface;

  /**
   * Orders the records in the given order
   *
   * Ordering: ASC|DESC
   *
   * @deprecated
   */
  public function order(string $field1, string $ordering1 = 'ASC', string $field2 = '', string $ordering2 = 'ASC'): self;

  /**
   * Return the total amount statement for the given field
   *
   * @deprecated
   */
  public function getSum(string $field, string $alias = ''): string;

  /**
   * Return the maximum value statement for the given field
   *
   * @deprecated
   */
  public function getMaximum(string $field, string $alias = ''): string;

  /**
   * Return the minimum value statement for the given field
   *
   * @deprecated
   */
  public function getMinimum(string $field, string $alias = ''): string;

  /**
   * Return the average value statement for the given field
   *
   * @deprecated
   */
  public function getAverage(string $field, string $alias = ''): string;

  /**
   * Return statement for counting the number of records on the given field
   *
   * @deprecated
   */
  public function getCount(string $field, string $alias = ''): string;

  /**
   * Returns the query result
   *
   * @deprecated
   */
  public function getResult(): DALInterface;

  /**
   * @deprecated
   */
  public function render(): array;

  /**
   * Starts a new transaction
   *
   * @throws DBException a transaction is already active
   * @deprecated
   */
  public function transaction(): self;

  /**
   * Commits the current transaction
   *
   * @throws DBException no transaction is active
   * @deprecated
   */
  public function commit(): self;

  /**
   * Rolls the current transaction back
   *
   * @throws DBException no transaction is active
   * @deprecated
   */
  public function rollback(): self;

  /**
   * Returns the DAL
   *
   * @deprecated
   */
  public function getDatabase(): DALInterface;

  /**
   * Dumps the current active database to a file
   *
   * @throws Exception
   * @deprecated
   */
  public function dumpDatabase(): string;

  /**
   * Returns the table description
   *
   * @deprecated
   */
  public function describe(string $table): array;

  /**
   * Returns the fields description
   *
   * @deprecated
   */
  public function describeFields(string $table): array;
}