<?php

namespace MiniatureHappiness\LegacyBundle\Interfaces;

use MiniatureHappiness\LegacyBundle\Exceptions\DBException;

/**
 * @deprecated
 */
interface WhereInterface extends ConditionsInterface
{
  /**
   * Starts a sub where part
   *
   * @deprecated
   */
  public function startSubWhere(): self;

  /**
   * Ends a sub where part
   *
   * @deprecated
   */
  public function endSubWhere(): self;

  /**
   * @param string $key (=|<>|LIKE|IN|BETWEEN)
   * @param string $command command (AND|OR)
   * @throws DBException the key is invalid
   * @throws DBException the command is invalid
   * @deprecated
   */
  public function addSubQuery(BuilderInterface $builder, string $field, string $key, string $command): self;

  /**
   * Renders the where
   *
   * @deprecated
   */
  public function render(): ?array;

  /**
   * Returns the query result (parent)
   *
   * @deprecated
   */
  public function getResult(): DALInterface;
}
