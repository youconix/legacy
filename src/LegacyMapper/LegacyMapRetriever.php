<?php

namespace MiniatureHappiness\LegacyBundle\LegacyMapper;

/**
 * Wrapper around the static legacy map
 */
class LegacyMapRetriever
{
    public function get(): LegacyMap
    {
        return LegacyMap::getInstance();
    }
}