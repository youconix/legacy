<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\LegacyMapper;

use Psr\Container\ContainerExceptionInterface;
use Symfony\Component\DependencyInjection\ServiceLocator;
use MiniatureHappiness\LegacyBundle\Exceptions\MemoryException;

/**
 * Maps the old names from Scripthulp Framework to Symfony
 *
 * @deprecated
 */
class ServiceMapper
{
    private ServiceLocator $locator;

    public function __construct(ServiceLocator $locator)
    {
        $this->locator = $locator;
    }

    /**
     * @deprecated
     */
    public function has(string $id): bool
    {
        return $this->locator->has($id);
    }

    /**
     * @throws MemoryException|ContainerExceptionInterface
     * @deprecated
     */
    public function get(string $id)
    {
        if (!$this->has($id)) {
            throw new MemoryException(
                sprintf('Unable to find bridged service with id %s. Is it tagged with miniature_happiness.legacy?', $id)
            );
        }

        return $this->locator->get($id);
    }
}