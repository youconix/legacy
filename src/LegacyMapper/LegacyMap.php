<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\LegacyMapper;

use MiniatureHappiness\LegacyBundle\Exceptions\MemoryException;

/**
 * Data class for mapping the old names from Scripthulp Framework to Symfony
 */
class LegacyMap
{
    private array $map = [];
    private static ?LegacyMap $instance = null;
    private ServiceMapper $legacyLocator;

    public static function getInstance(): LegacyMap
    {
        if (self::$instance === null) {
            self::$instance = new LegacyMap();
        }

        return self::$instance;
    }

    public function setServiceLocator(ServiceMapper $legacyLocator): void
    {
        $this->legacyLocator = $legacyLocator;
    }

    public function loadMap(string $cacheDir): void
    {
        $fileName = sprintf('%s/%s', $cacheDir, 'legacyMap.php');

        $this->map = unserialize(
            file_get_contents($fileName)
        );
    }

    public function find(string $className): ?string
    {
        return $this->has($className) ? $this->map[$className]['id'] : null;
    }

    /**
     * @throws MemoryException
     */
    public function loadById(string $id)
    {
        return $this->legacyLocator->get($id);
    }

    /**
     * @throws MemoryException
     */
    public function loadByClassName($className)
    {
        $id = $this->find($className);
        if ($id === null)
        {
            throw new MemoryException(sprintf('Class %s not found in legacy map.', $className));
        }

        return $this->loadById($id);
    }

    public function getFileName(string $className): ?string
    {
        return $this->has($className) ? $this->map[$className]['filename'] : null;
    }

    protected function has(string $className): bool
    {
        return array_key_exists($className, $this->map);
    }
}