<?php

namespace MiniatureHappiness\LegacyBundle\CompilerPass;

use DOMDocument;
use DOMNode;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Filesystem\Filesystem;

class LegacyCompilerPass implements CompilerPassInterface
{
    private Filesystem $fileSystem;
    private string $fileName;
    private array $mapping;

    public function __construct(Filesystem $fileSystem, string $cacheDir)
    {
        $this->fileSystem = $fileSystem;
        $this->fileName = sprintf('%s/%s', $cacheDir, 'legacyMap.php');
    }

    public function process(ContainerBuilder $container)
    {
        $this->mapping = [];

        $reader = new DOMDocument();
        $reader->load(__DIR__ . '/../Resources/legacy_map.xml');

        $items = $reader->getElementsByTagName('legacyClass');

        /** @var DOMNode $item */
        foreach($items as $item) {
            $this->parseLegacyItem($item);
        }

        $this->fileSystem->dumpFile($this->fileName, serialize($this->mapping));
    }

    private function parseLegacyItem(DOMNode $node): void
    {
        $data = [];

        /** @var DOMNode $attribute */
        foreach($node->attributes as $attribute)
        {
            $data[$attribute->nodeName] = (string) $attribute->nodeValue;
        }

        /** @var DOMNode $child */
        foreach($node->childNodes as $child)
        {
            $data[$child->nodeName] = (string) $child->nodeValue;
        }

        $filename = sprintf('%s/%s', $data['prefix'], $data['filename']);
        $fqn = sprintf('\\%s\\%s', $data['namespace'], $data['fqn']);

        $this->mapping[$data['key']] = [
            'id' => $data['id'],
            'filename' => $filename,
            'fqn' => $fqn,
        ];
    }
}