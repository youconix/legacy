<?php

namespace MiniatureHappiness\LegacyBundle\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ConfigurationCompilerPass implements CompilerPassInterface
{
    private ContainerBuilder $container;

    public function process(ContainerBuilder $container)
    {
        $this->container = $container;

        $this->twig();
        $this->router();
    }

    private function twig(): void
    {
    }

    private function router(): void
    {
        $router = $this->container->getExtensionConfig('framework');
    }
}