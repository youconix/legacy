<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Classes;

use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * Queue class.
 * This collection works with the principal first in, first out
 *
 * @author Rachelle Scheijen
 * @since 1.0
 * @deprecated
 *
 * @see http://php.net/manual/en/class.splqueue.php
 */
class Queue
{
    use DeprecationTrait;

    /**
     * @var array
     */
    private $content;

    /**
     * @var int
     */
    private $start;

    /**
     * @var int
     */
    private $counter;

    public function __construct(array $content = [])
    {
        $this->clear();

        $this->addArray($content);
    }

    /**
     * Merges the given queue with this one
     */
    public function addQueue(Queue $queue): self
    {
        $this->triggerDeprecationError();

        while (!$queue->isEmpty()) {
            $this->push($queue->pop());
        }

        return $this;
    }

    /**
     * Adds the array to the queue
     */
    public function addArray(array $content): self
    {
        $this->triggerDeprecationError();

        foreach ($content as $item) {
            $this->push($item);
        }

        return $this;
    }

    /**
     * Pushes the item at the end of the queue
     *
     * @param mixed $item
     */
    public function push($item): self
    {
        $this->triggerDeprecationError();

        $this->content[] = $item;
        $this->counter++;

        return $this;
    }

    /**
     * Retrieves and removes the head of this queue, or null if this queue is empty.
     *
     * @return mixed The first element of the queue.
     */
    public function pop()
    {
        $this->triggerDeprecationError();

        if ($this->isEmpty()) {
            return null;
        }

        $s_content = $this->content[$this->start];
        $this->content[$this->start] = null;
        $this->start++;

        return $s_content;
    }

    /**
     * Retrieves the head of this queue, or null if this queue is empty
     *
     * @return mixed The first element of the queue.
     */
    public function peek()
    {
        $this->triggerDeprecationError();

        if ($this->isEmpty()) {
            return null;
        }

        return $this->content[$this->start];
    }

    /**
     * Searches if the queue contains the given item
     *
     * @param Object $search
     */
    public function search($search): bool
    {
        $this->triggerDeprecationError();

        if ($this->isEmpty()) {
            return false;
        }

        for ($i = $this->start; $i <= $this->counter; $i++) {
            if ($this->content[$i] === $search) {
                return true;
            }
        }

        return false;
    }

    public function isEmpty(): bool
    {
        $this->triggerDeprecationError();

        return ($this->start === $this->counter);
    }

    public function clear(): self
    {
        $this->triggerDeprecationError();

        $this->content = [];
        $this->counter = 0;
        $this->start = 0;

        return $this;
    }
}