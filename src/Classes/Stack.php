<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Classes;

use MiniatureHappiness\LegacyBundle\Exceptions\StackException;
use MiniatureHappiness\LegacyBundle\Traits\DeprecationTrait;

/**
 * Stack class.
 * This collection works with the principal first in, last out
 *
 * @author Rachelle Scheijen
 * @since 1.0
 * @deprecated
 *
 * @deprecated
 * @see http://php.net/manual/en/class.splstack.php
 */
class Stack
{
    use DeprecationTrait;

    /**
     * @var array
     */
    private $content;

    /**
     * @var int
     */
    private $counter;

    public function __construct(array $content = [])
    {
        $this->clear();

        $this->addArray($content);
    }

    /**
     * Merges the given stack with this one
     */
    public function addStack(Stack $stack): self
    {
        $this->triggerDeprecationError();

        while (!$stack->isEmpty()) {
            $this->push($stack->pop());
        }

        return $this;
    }

    /**
     * Adds the array to the stack
     */
    public function addArray(array $content): self
    {
        $this->triggerDeprecationError();

        foreach ($content as $item) {
            $this->push($item);
        }

        return $this;
    }

    /**
     * Pushes the item at the end of the stack
     *
     * @param mixed $item
     */
    public function push($item): self
    {
        $this->triggerDeprecationError();

        $this->content[] = $item;
        $this->counter++;

        return $this;
    }

    /**
     * Retrieves and removes the end of this stack
     *
     * @return mixed The last element of the stack.
     * @throws StackException the stack is empty
     */
    public function pop()
    {
        $this->triggerDeprecationError();

        if ($this->isEmpty()) {
            throw new StackException("Can not pop from empty stack");
        }

        $s_content = $this->content[$this->counter];
        $this->content[$this->counter] = null;
        $this->counter--;

        return $s_content;
    }

    /**
     * Retrieves end of this stack without removing it
     *
     * @return mixed The last element of the stack.
     * @throws StackException the stack is empty
     */
    public function peek()
    {
        $this->triggerDeprecationError();

        if ($this->isEmpty()) {
            throw new StackException("Can not peek from empty stack");
        }

        return $this->content[$this->counter];
    }

    /**
     * Searches if the stack contains the given item
     *
     * @param Object $search
     */
    public function search($search): bool
    {
        $this->triggerDeprecationError();

        for ($i = 0; $i <= $this->counter; $i++) {
            if ($this->content[$i] === $search) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the stack is empty
     */
    public function isEmpty(): bool
    {
        $this->triggerDeprecationError();

        return ($this->counter === -1);
    }

    public function clear(): self
    {
        $this->triggerDeprecationError();

        $this->content = [];
        $this->counter = -1;

        return $this;
    }
}