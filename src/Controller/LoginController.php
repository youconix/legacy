<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends AbstractController
{
    public function indexAction(): Response
    {
        $service = \Memory::services('Service_Session');

        return new JsonResponse(['className' => get_class($service)]);
    }

    public function loginAction(): Response
    {

    }
}