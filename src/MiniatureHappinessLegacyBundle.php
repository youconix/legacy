<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\Filesystem\Filesystem;
use MiniatureHappiness\LegacyBundle\CompilerPass\ConfigurationCompilerPass;
use MiniatureHappiness\LegacyBundle\CompilerPass\LegacyCompilerPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MiniatureHappinessLegacyBundle extends Bundle implements PrependExtensionInterface
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $cacheDirectory = $container->getParameter('kernel.cache_dir');
        $fileSystem = new Filesystem();

        $container->addCompilerPass(new LegacyCompilerPass($fileSystem, $cacheDirectory));
        $container->addCompilerPass(new ConfigurationCompilerPass());
    }

    public function prepend(ContainerBuilder $container)
    {
        // TODO: Implement prepend() method.
    }
}