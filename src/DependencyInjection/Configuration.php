<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('miniature_happiness.legacy');

        $rootNode = $treeBuilder->getRootNode();
        $rootNode->children()
            ->arrayNode('database')
                ->addDefaultsIfNotSet()
                ->children()
                    ->node('username','string')
                        ->defaultValue('')
                        ->end()
                    ->node('password','string')
                        ->defaultValue('')
                        ->end()
                    ->node('host','string')
                        ->defaultValue('localhost')
                        ->end()
                    ->enumNode('type')
                        ->values(['mysql', 'postgresql'])
                        ->defaultValue('mysql')
                        ->end()
                    ->integerNode('port')
                        ->defaultValue(3306)
                        ->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}