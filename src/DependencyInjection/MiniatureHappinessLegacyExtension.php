<?php

declare(strict_types=1);

namespace MiniatureHappiness\LegacyBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class MiniatureHappinessLegacyExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new XmlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );

        $loader->load('packages/parameters.xml');
        $loader->load('packages/classes.xml');
        $loader->load('packages/controllers.xml');
        $loader->load('packages/database.xml');
        $loader->load('packages/helpers.xml');
        $loader->load('packages/interfaces.xml');
        $loader->load('packages/listeners.xml');
        $loader->load('packages/service_locator.xml');
        $loader->load('packages/services.xml');
    }
}